(function ($) {
    "use strict";


    jQuery(document).ready(function ($) {


        /*---------------------------------------------*
        * Carousel
        ---------------------------------------------*/
        $('#Carousel').carousel({
                interval: 5000,
                item: 2
            })
            /*------------------------*/

    });
/*------------------------------*/
/* Clients Carousel
/*------------------------------*/


$('.clients-carousel').owlCarousel({
    autoPlay:3000,
    slideSpeed: 200,
     items : 6,
     itemsDesktop : [1199,4],
     itemsDesktopSmall : [979,3],
     stopOnHover:true,
     pagination:false,
   });
    /*---------------------------------------------*
        * STICKY scroll
    ---------------------------------------------*/

    $.localScroll();

    /**************************/


    jQuery(window).load(function () {


    });



}(jQuery));
