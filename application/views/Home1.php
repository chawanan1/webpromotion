<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">

    <!-- Site Metas -->
    <title>NT Conn|X</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="/assets/images/favicon-96x96.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="/assets/images/apple-touch-icon.png">

    <!-- Bootstrap CSS -->

    <link id="changeable-colors" rel="stylesheet" href="/assets/css/orange.css" />

    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <!-- Site CSS -->
    <link rel="stylesheet" href="/assets/css/style_card.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <!-- Responsive CSS -->

    <link rel="stylesheet" href="/assets/css/responsive.css">
    <!-- color -->

    <link  rel="stylesheet" href="/assets/fonts/font.css" />

    <link  rel="stylesheet" href="/assets/fonts/fontnt.css" />
    <!-- New Remix style icon-->
    <link  rel="stylesheet" href="/assets/css/remixicon.css" />

    <!-- Modernizer -->
    <script src="/assets/js/modernizer.js"></script>

<!-- Silde -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-V25N59QT07"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-V25N59QT07');
</script>


<style>
        /* jssor slider loading skin spin css */
        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-009-spin {
            from {
                transform: rotate(0deg);
            }

            to {
                transform: rotate(360deg);
            }
        }


        /*jssor slider bullet skin 132 css*/
        .jssorb132 {position:absolute;}
        .jssorb132 .i {position:absolute;cursor:pointer;}
        .jssorb132 .i .b {fill:#fff;fill-opacity:0.8;stroke:#000;stroke-width:1600;stroke-miterlimit:10;stroke-opacity:0.7;}
        .jssorb132 .i:hover .b {fill:#000;fill-opacity:.7;stroke:#fff;stroke-width:2000;stroke-opacity:0.8;}
        .jssorb132 .iav .b {fill:#000;stroke:#fff;stroke-width:2400;fill-opacity:0.8;stroke-opacity:1;}
        .jssorb132 .i.idn {opacity:0.3;}

        .jssora051 {display:block;position:absolute;cursor:pointer;}
        .jssora051 .a {fill:none;stroke:#fff;stroke-width:360;stroke-miterlimit:10;}
        .jssora051:hover {opacity:.8;}
        .jssora051.jssora051dn {opacity:.5;}
        .jssora051.jssora051ds {opacity:.3;pointer-events:none;}
        #more {display: none;}
    </style>

</head>
<body>

<!-- ส่วน Loader หน้า Landing homepage -->

<div id="site-header">
    <header id="header" class="header-block-top">
        <div class="container">
            <div class="row">
                <div class="main-menu">
                    <!-- navbar -->
                    <nav class="navbar navbar-default" id="mainNav">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>

                            </button>
                            <!-- <img src="/assets/img/T11.png"></a>   -->
                            <div class="logo">
                                <a class="navbar-brand js-scroll-trigger logo-header" href="#">
                                    <img src="" alt=""> <!-- img logo .png -->
                                </a>
                            </div>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="active"><a href="#home">Home</a></li>
                                <li><a href="#hot">HOT Promotion</a></li>
                                <li><a href="#product">Product</a></li>
                                <li><a href="partner2">Join Us</a></li>
                                <li><a href="#Contact">Contact</a></li>
                                <li><a href="#about">x-blog</a></li>
                            </ul>
                        </div>
                        <!-- end nav-collapse -->
                    </nav>
                    <!-- end navbar -->
                </div>
            </div>
            <!-- end row -->
        </div>
</div>
<!-- end container-fluid -->

</header>
<!-- end header -->
<!--======================================== //    Start Silde Show Banner // ======================================================-->
<div id="home" class=" full-screen-mode parallax">
    <div class="container text-center">
        <div class="alignfull-page">
            <!-- <a href="<?php echo base_url('about'); ?>"><img class="img-responsive"
                    src="/assets/img/T11.png"></a> -->
          <img class="img-responsive" src="/assets/img/T11.png">
        </div>
    </div>
    <!-- end container -->
</div>
<!-- end banner -->

<!--======================================== //    End Silde Show Banner // ===================================================-->


<!--======================================== //    Start HotPromotion // ======================================================-->
<div id="hot" class="menu-main pad-top-100  pad-bottom-100">
    <div class="special-menu parallax">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
                        <h1>
                            <p></p>
                        </h1>
                        <h1 class="block-title text-center"> Hot Promotion </h1>

                    </div>
                    <div class="special-box">
                        <div id="owl-demo">
                            <div class="item item-type-zoom">
                                <a value="1" href="#" data-toggle="modal" class="item-hover" data-target="#myModal6">
                                <!-- <a value="1" href="#" data-toggle="modal" data-target="#myModal6"> -->
                                    <div class="item-info">
                                        <div class="headline"> NT Connectivity
                                            <div class="line"></div>
                                            <div class="dit-line"></div>
                                        </div>
                                    </div>
                                </a>
                                <div class="item-img">
                                    <?php include 'connt.php';
echo "
                                    <img src='/assets/img/nt_connectivity/" . $_CONFIG['PROIMG']['HOT1'] . "' class='img-rounded' width='100%' height='100%'>
                                ";?>
                                </div>
                            </div>
                            <div class="item item-type-zoom">
                                <!-- <a href="#" class="item-hover"> -->
                                <a value="1" href="#" data-toggle="modal" class="item-hover" data-target="#myModal7">
                                    <div class="item-info">
                                        <div class="headline"> NT Connectivity
                                            <div class="line"></div>
                                            <div class="dit-line"></div>
                                        </div>
                                    </div>
                                </a>
                                <div class="item-img">
                                    <?php include 'connt.php';
echo "
                                    <img src='/assets/img/nt_connectivity/" . $_CONFIG['PROIMG']['HOT2'] . "' class='img-rounded' width='100%' height='100%'>
                                   ";?>
                                    <!-- <img src="/assets/img/nt_platform/5.png" alt="sp-menu"> -->
                                </div>
                            </div>
                            <div class="item item-type-zoom">
                                <!-- <a href="#" class="item-hover"> -->
                                <a value="1" href="#" data-toggle="modal" class="item-hover" data-target="#myModal8">
                                    <div class="item-info">
                                        <div class="headline"> NT Connectivity
                                            <div class="line"></div>
                                            <div class="dit-line"></div>
                                        </div>
                                    </div>
                                </a>
                                <div class="item-img">
                                    <!-- <img src="/assets/img/nt_solution/8.png" alt="sp-menu"> -->
                                    <?php include 'connt.php';
echo "

                                    <img src='/assets/img/nt_connectivity/" . $_CONFIG['PROIMG']['HOT3'] . "' class='img-rounded' width='100%' height='100%'>
                                   ";?>
                                </div>
                            </div>
                            <div class="item item-type-zoom">
                                <!-- <a href="#" class="item-hover"> -->
                                <a value="1" href="#" data-toggle="modal" class="item-hover" data-target="#myModal9">
                                    <div class="item-info">
                                        <div class="headline"> NT Platform
                                            <div class="line"></div>
                                            <div class="dit-line"></div>
                                        </div>
                                    </div>
                                </a>
                                <div class="item-img">
                                    <!--  <img src="/assets/img/nt_connectivity/3.png" alt="sp-menu"> -->
                                    <?php include 'connt.php';
echo "

                                    <img src='/assets/img/nt_platform/" . $_CONFIG['PROIMG']['HOT4'] . "' class='img-rounded' width='100%' height='100%'>
                                   ";?>
                                </div>
                            </div>
                            <div class="item item-type-zoom">
                                <!-- <a href="#" class="item-hover"> -->
                                <a value="1" href="#" data-toggle="modal" class="item-hover" data-target="#myModal12">
                                    <div class="item-info">
                                        <div class="headline"> Digital Solutions by NT ConnX
                                            <div class="line"></div>
                                            <div class="dit-line"></div>
                                        </div>
                                    </div>
                                </a>
                                <div class="item-img">
                                    <!-- <img src="/assets/img/nt_connectivity/2.png" alt="sp-menu"> -->
                                    <?php include 'connt.php';
echo "

                                    <img src='/assets/img/nt_solution/" . $_CONFIG['PROIMG']['HOT5'] . "' class='img-rounded' width='100%' height='100%'>
                                   ";?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end special-box -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end special-menu -->
</div>
<!--======================================== //    End HotPromotion // ========================================================-->
<!--======================================== //    Start Product Show // ======================================================-->
<div id="product" class="menu-main pad-top-100">
    <div class="wrapper">
        <div class="container">

            <h2><strong style="color:white;">NT Connectivity</strong></h2>

            <h1 style="color: black;">บริการวงจรสื่อสารไร้สายเทคโนโลยี 4G, LoRaWAN, Wi-Fi และอื่นๆ </h1>
            <h1>สำหรับเชื่อมโยง IoT Sensor/Smart Device กับ Digital Platform ของผู้ให้บริการดิจิตอลโซลูชั่น</h1>



            <!-- <div class="special-box"> -->
            <!-- <div id="owl-demo"> -->
            <div class="cards">
                <figure class="card">
                    <a value="1" href="#" data-toggle="modal" data-target="#myModal6">
                        <!-- myModal #6 -->
                        <!--  <img src="/assets/img/nt_Connectivity/1.png" /> -->
                        <?php include 'connt.php';
echo "

                                    <img src='/assets/img/nt_Connectivity/" . $_CONFIG['CONN']['CONN1'] . "' class='img-rounded' width='100%' height='100%'>
                                   ";?>
                        <figcaption>Details Product</figcaption>
                    </a>
                    <!-- <figcaption>Fig.1 - Trulli, Puglia, Italy</figcaption> -->
                </figure>

                <figure class="card">
                    <a href="#" data-toggle="modal" data-target="#myModal7">
                        <!-- myModal #7 -->
                        <!-- <img src="/assets/img/nt_Connectivity/2.png" /> -->
                        <?php include 'connt.php';
echo "

                                    <img src='/assets/img/nt_Connectivity/" . $_CONFIG['CONN']['CONN2'] . "' class='img-rounded' width='100%' height='100%'>
                                   ";?>
                        <figcaption>Details Product</figcaption>
                    </a>
                </figure>

                <figure class="card">
                    <a href="#" data-toggle="modal" data-target="#myModal8">
                        <!-- myModal #8 -->
                        <!-- <img src="/assets/img/nt_Connectivity/3.png" /> -->
                        <?php include 'connt.php';
echo "

                                    <img src='/assets/img/nt_Connectivity/" . $_CONFIG['CONN']['CONN3'] . "' class='img-rounded' width='100%' height='100%'>
                                   ";?>
                        <figcaption>Details Product</figcaption>
                    </a>

                </figure>


            </div>

            <p class="text-right"><button onclick="window.location.href='product'" class="button button1 text-right">See
                    All <img src="<?php echo base_url('/assets/img/toggle.png'); ?>" alt=""></button></p>
            <!--  </div> -->
            <!-- end container -->
            <!-- </div> -->

        </div>
    </div>
</div>
<!-- end menu -->
<div id="product" class="menu-main">
    <div class="wrapper">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"></div>
            <h2><strong style="color:white;">NT Platform</strong></h2>
            <h style="color: black;">
            </h>

            <h1 style="color: black; font-size: 24px;">บริการให้ใช้ทรัพยากรสำหรับการพัฒนา Digital Application (Computing
                and Storage, Data and Telecommunication Network และ API ที่จำเป็น)</h1>
            <h1>สำหรับการพัฒนา หรือการ Demo Digital Services ใน ลักษณะ Platform as a Service
                ซึ่งสามารถควบคุมค่าใช้จ่ายที่ใช้ในการพัฒนาได้</h1>


            <div class="cards">
                <figure class="card">
                    <a href="#" data-toggle="modal" data-target="#myModal9">
                        <!-- myModal #6 -->
                        <?php include 'connt.php';
echo "

                <img src='/assets/img/nt_platform/" . $_CONFIG['Platform']['Platform1'] . "'>

                ";?>
                        <figcaption>Details Product</figcaption>
                    </a>
                    <!-- <figcaption>Fig.1 - Trulli, Puglia, Italy</figcaption> -->
                </figure>

                <figure class="card">
                    <a href="#" data-toggle="modal" data-target="#myModal10">
                        <!-- myModal #7 -->

                        <?php include 'connt.php';
echo "

                <img src='/assets/img/nt_platform/" . $_CONFIG['Platform']['Platform2'] . "'>

                ";?>
                        <figcaption>Details Product</figcaption>
                    </a>
                </figure>

                <figure class="card">
                    <a href="#" data-toggle="modal" data-target="#myModal11">
                        <!-- myModal #8 -->

                        <?php include 'connt.php';
echo "

                <img src='/assets/img/nt_platform/" . $_CONFIG['Platform']['Platform3'] . "'>

                ";?>
                        <figcaption>Details Product</figcaption>
                    </a>

                </figure>



            </div>
            <p class="text-right"><button onclick="window.location.href='product'" class="button button1 text-right">See
                    All <img src="<?php echo base_url('/assets/img/toggle.png'); ?>" alt=""></button></p>

            <!-- end container -->


        </div>
    </div>
</div>

<!-- end menu -->
<div id="product" class="menu-main">
    <div class="wrapper">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"></div>
            <h2><strong style="color:white;">Digital Solutions by NT ConnX </strong></h2>
            <h style="color: black;">ตัวอย่าง Digital Solutions ของผู้ร่วมพัฒนาบริการ </2>
                <div class="cards">
                    <figure class="card">
                        <a href="#" data-toggle="modal" data-target="#myModal12">
                            <!-- myModal #6 -->
                            <?php include 'connt.php';
echo "

                <img src='/assets/img/nt_solution/" . $_CONFIG['solution']['solution1'] . "'>

                ";?>

                            <figcaption>Details Product</figcaption>
                        </a>
                        <!-- <figcaption>Fig.1 - Trulli, Puglia, Italy</figcaption> -->
                    </figure>

                    <figure class="card">
                        <a href="#" data-toggle="modal" data-target="#myModal13">
                            <!-- myModal #7 -->
                            <?php include 'connt.php';
echo "

                <img src='/assets/img/nt_solution/" . $_CONFIG['solution']['solution2'] . "'>

                ";?>
                            <figcaption>Details Product</figcaption>
                        </a>
                    </figure>




                </div>
                <p class="text-right"><button onclick="window.location.href='product'"
                        class="button button1 text-right">See All <img
                            src="<?php echo base_url('/assets/img/toggle.png'); ?>" alt=""></button></p>

                <!-- end container -->


        </div>
    </div>
</div>
<!-- end menu -->
<!--======================================== //    End Product Show // ===========================================================-->

<!--======================================== //    End Silde Show Banner // ===================================================-->
<div id="partner2" class="service">
    <div class="wrapper">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"></div>

            <h1 class="block-title text-center pad-top-70 "> ทำไมต้องเข้าร่วมพัฒนาสินค้า Digital Solution กับเรา
            </h1>
            <!-- service section -->
<div id="service" class="service">
         <div class="container">

            <div class="row">
               <div class="col-md-12">
                  <div class="service_main">
                     <div class="service_box blu_colo">
                        <i><img src="/assets/img/Partner/savecost.png" alt="#"/></i></br>
                        <h7>ช่วยลดต้นทุน </br>การพัฒนาสินค้า</h7>
                     </div>
                     <div class="service_box yelldark_colo">
                        <i><img src="/assets/img/Partner/dev.png" alt="#"/></i></br>
                        <h7>ช่วยพัฒนาสินค้า</br>ให้ทันตามความต้องการ</h7>
                     </div>
                     <div class="service_box yell_colo">
                        <i><img src="/assets/img/Partner/customer.png" alt="#"/></i></br>
                        <h7> มีฐานลูกค้า NT รองรับ </h7>
                     </div>
                  </div>
                  <div class="container">
                  <div class="col-md-12">

                  <!-- <a class="read_more" href="<?php echo base_url('partner'); ?>">Read More</a> -->
</div>
               </div>
               </div>
            </div>
         </div>
         </br>
      </div></br>
      <!-- end service section -->

        </div>
    </div>
</div>
<div class="menu-main pad-bottom-40 pad-top-40">
<div class="container-login103-form-btn">
						<button onclick="window.location.href='partner'" class="login103-form-btn">
                        ร่วมงานกับเรา >>>
						</button>
					</div>
</div>
<h1>

  <div class="message">
    <div class="word1">close</div>

  </div>
</h1>
<!--======================================== //    Start Extension Modal // ======================================================-->
<?php $this->load->view('teamplate/home_extension');?>
<!--======================================== //    Start Extension Modal // =======================================================-->
<!--======================================== //    Start Extension Modal2 // ======================================================-->

<a href="#" class="scrollup" style="display: none;">Scroll</a>

