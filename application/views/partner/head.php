<!DOCTYPE html>
<html lang="en">

<head>

  <!-- Basic -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">

  <!-- Site Metas -->
  <title>NT Conn|X</title>
  <meta name="keywords" content="">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Site Icons -->
  <link rel="shortcut icon" href="./assets/images/favicon-96x96.ico" type="image/x-icon">
  <link rel="apple-touch-icon" href="./assets/images/apple-touch-icon.png">
  <link rel="stylesheet" href="./assets/css/orange.css">
  <link rel="stylesheet" href="./assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="./assets/css/style.css">
  <link rel="stylesheet" href="./assets/css/responsive.css">
  <link rel="stylesheet" href="./assets/css/style_product.css">


  <!-- color -->
  <link rel="stylesheet" href="./assets/fonts/font.css">
  <link rel="stylesheet" href="./assets/fonts/fontnt.css">

  <!-- Modernizer -->
  <script src="./assets/js/modernizer.js"></script>
  <!-- fevicon -->
  <script src='https://www.google.com/recaptcha/api.js?hl=th'></script>
  <style>
    ul::marker {
      color: #ffd100;
      font-size: 23px;
    }

    input.button.button11.btn-block {
      border-radius: 30px;
    }
  </style>
  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-7QT9W6PG2Z"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-7QT9W6PG2Z');
  </script>

  <script>
    document.myform.id.focus();
  </script>

</head>

<body>