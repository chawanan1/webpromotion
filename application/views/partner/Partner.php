</br></br>
<!-- Section: intro -->
<section id="intro" class="intro">
   <div class="intro-content">
      <div class="container">
         <div class="row">
            <div class="col-lg-6">
               <div class="wow fadeInDown" data-wow-offset="0" data-wow-delay="0.1s">
                  <h23 class="h-ultra">ทำไมต้องเข้าร่วมพัฒนาสินค้า Digital Solution กับเรา</h23>
               </div>
               <!--<div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.1s">
				 	<h4 class="h-light">Provide <span class="color">best quality healthcare</span> for you</h4>
					</div>-->
               <div class="well well-trans">
                  <div class="wow fadeInRight" data-wow-delay="0.1s">

                     <ul class="lead-list">
                        <li><span class="fa fa-check fa-2x icon-success"></span>
                           <span class="list"><strong class="lists"> Reduce Cost.</strong>
                              <br>
                              <p class="lists-p">ช่วยลดต้นทุนการพัฒนาสินค้ามากยิ่งขึ้นในการจัดทำ Solotion ใหม่ๆ</p>
                           </span>
                        </li>

                        <li><span class="fa fa-check fa-2x icon-success"></span>
                           <span class="list"><strong class="lists">Development to meet the requirements.</strong>
                              <br>
                              <p class="lists-p">ช่วยพัฒนาสินค้าให้ทันตามความต้องการ</p>
                           </span>
                        </li>

                        <li><span class="fa fa-check fa-2x icon-success"></span>
                           <span class="list"><strong class="lists">Have a customer base.</strong>
                              <br>
                              <p class="lists-p">มีฐานลูกค้า NT รองรับ</p>
                           </span>
                        </li>
                     </ul>

                  </div>
               </div>


            </div>
            <div class="col-lg-6">
               <div class="form-wrapper">
                  <div class="wow fadeInRight" data-wow-duration="2s" data-wow-delay="0.2s">

                     <div class="panel panel-skin">
                        <div class="panel-heading">
                           <h3 class="panel-title"><span class="fa fa-pencil-square-o"></span> กรอกข้อมูลเพื่อเข้ามาเป็นส่วนหนึ่งในการพัฒนากับเรา <!-- <small>(It's free!)</small> --></h3>
                        </div>
                        <div class="panel-body">
                           <form class="lead" action="partner/line_nontify" method="post">
                              <!-- <form action="partner/line_nontify" method="post" > -->
                              <div class="row">
                                 <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                       <label>ชื่อบริษัทของคุณ</label>
                                       <!-- <input type="text" name="first_name" id="first_name" class="form-control input-md"> -->
                                       <input autofocus name="id" id="id" class="form-control input-md" value="" required="Company">
                                    </div>
                                 </div>
                                 <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                       <label>อีเมล</label>
                                       <!-- <input type="text" name="last_name" id="last_name" class="form-control input-md"> -->

                                       <input name="name" id="name" class="form-control input-md" type="email" required="@">
                                    </div>
                                 </div>
                              </div>

                              <div class="row">
                                 <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                       <label>ชื่อของผู้ให้ติดต่อกลับ</label>
                                       <!-- <input type="email" name="email" id="email" class="form-control input-md"> -->

                                       <input name="ss" id="ss" class="form-control input-md" type="text" value="" required="">
                                    </div>
                                 </div>
                                 <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                       <label>เบอร์โทรติดต่อ</label>
                                       <!-- <input type="text" name="phone" id="phone" class="form-control input-md"> -->

                                       <input name="bb" id="bb" class="form-control input-md" type="phone" value="" required="">
                                    </div>
                                 </div>
                                 <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                       <label>ข้อมูลหรือรายละเอียดสินค้าที่ต้องการพัฒนา</label>
                                       <!-- <input type="text" name="phone" id="phone" class="form-control input-md"> -->
                                       <!-- <span class="form-label" style="color:black;">ข้อมูลหรือรายละเอียดสินค้าที่ต้องการพัฒนา</span> -->
                                       <textarea class="form-control input-md" name="cc" id="cc" rows="5" cols="43"></textarea>
                                    </div>
                                 </div>

                              </div>
                              <div class="col-xs-12 col-sm-12 col-md-12">
                                 <div class="lab1">
                                    <input name="input_16.1" type="checkbox" value="Consent" id="choice_42_16_1" value="" required="">
                                    ในการดำเนินการสมัครเข้าร่วมพัฒนาสินค้า Digital Solution ข้าพเจ้าตกลงและยินยอมให้
                                    NT CONNX รวบรวมและใช้ข้อมูลที่ข้าพเจ้าให้ไว้เพื่อดำเนินการตามที่จำเป็นภายใต้<a style="color: #5B5BFF; " href="privacy_policy">นโยบายส่วนบุคคลของ NT
                                    </a>ที่ข้าพเจ้าได้อ่านและเข้าใจเนื้อหาทั้งหมดแล้ว
                                 </div>
                              </div>
                              <!-- <input type="submit" value="submit" onClick="insert_value()" class="btn btn-skin btn-block btn-lg"> -->
                              <!-- <input type = "submit" name="submit" class="btn btn-skin btn-block btn-lg" onClick="insert_value()" value = "เข้าร่วม"> -->
                              <script>
                                 function makeaction() {
                                    document.getElementById('submit').disabled = false;
                                 }
                              </script>
                              <div class="g-recaptcha" data-callback="makeaction" data-sitekey="6LcvWQIiAAAAABKylTfwFhwzs6XtA_OFom0ez23C"></div>
                              <div class="form-group">
                                 <br>
                                 <button class="btn btn-skin btn-block btn-lg join" type="submit" id="submit" name="submit" onClick="insert_value()" title="กรุณาติ๊กถูกเพื่อยืนยันตนของท่าน" disabled>เข้าร่วม</button>
                              </div>
                              <!-- <p class="lead-footer">* We'll contact you by phone & email later</p> -->

                           </form>
                        </div>
                     </div>

                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>


<section id="partner" class="home-section paddingbot-60 pad-top-30 pad-bottom-40">
   <div class="container marginbot-50">
      <div class="row">
         <div class="col-lg-8 col-lg-offset-2">
            <div class="wow lightSpeedIn" data-wow-delay="0.1s">
               <div class="section-heading text-center">
                  <h24 class="h-bold">Our Company Developed</h24>
                  <!-- <p>Take charge of your health today with our specially designed health packages</p> -->
               </div>
            </div>
            <div class="divider-short"></div>
         </div>
      </div>
   </div>

   <div class="container">
      <div class="row">
         <div class="col-sm-6 col-md-4">
            <div class="partner">
               <a href="#"><img src="./assets/img/Partner/3.png" alt="" /></a>
            </div>
         </div>

         <!-- <div class="col-sm-6 col-md-3">
						<div class="partner">
						<a href="#"><img src="/assets/img/Partner/part4.jpg" alt="" /></a>

						</div>
					</div> -->
      </div>
   </div>
</section>
<!-- /Section: intro -->
<!-- <div id="service" class="service top-100">
         <div class="container">
            <div class="row">
               <div class="col-md-12 offset-md-1">
                  <div class="titlepage">
                     <h11>ทำไมต้องเข้าร่วมพัฒนาสินค้า Digital Solution กับเรา</h11>
                     </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-12">
                  <div class="service_main">
                     <div class="service_box blu_colo">
                        <i><img src="/assets/img/Partner/savecost.png" class="dass" alt="#"/></i></br>
                        <h7>ช่วยลดต้นทุน </br>การพัฒนาสินค้า</h7>
                     </div>
                     <div class="service_box yelldark_colo">
                        <i><img src="/assets/img/Partner/dev.png" class="dass" alt="#"/></i></br>
                        <h7>ช่วยพัฒนาสินค้า</br>ให้ทันตามความต้องการ</h7>
                     </div>
                     <div class="service_box yell_colo">
                        <i><img src="/assets/img/Partner/customer.png" class="dass" alt="#"/></i></br>
                        <h7> มีฐานลูกค้า NT รองรับ </h7>
                     </div>
                  </div>
                  <div class="col-md-12">
               </div>
               </div>
            </div>
         </div>
      </div>

    <section class="banner_main">
         <div id="banner1" class="carousel slide banner_slide" data-ride="carousel">
            <ol class="carousel-indicators">
               <li data-target="#banner1" data-slide-to="0" class="active"></li>
            </ol>
            <div class="carousel-inner1">
               <div class="carousel-item active">
                  <div class="container-fluid">
                     <div class="">
                        <div class="row">
                           <div class="col-md-7 col-lg-5">
                              <div class="text-bg">
                                 <h1>ร่วมพัฒนาสินค้า</br> Digital Solutions </br>กับเรา</h1>

                              </div>
                           </div>
                           <div class="col-md-12 col-lg-7">
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="ban_track">
                                       <figure><img class="tuckz" src="/assets/images/track.png" alt="#"/></figure>
                                    </div>
                                 </div>
                                 <div class="col-md-6">

                                 <form action="partner/line_nontify" method="post" >
                            <div class="form-header">
                                <h22>กรุณากรอกรายละเอียดของท่านเพื่อร่วมพัฒนาสินค้า</h22>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">

                                    <div class="form-group">
                                        <span class="form-label" style="color:black;">ชื่อบริษัทของคุณ</span>
                                        <input name="id" id="id" class="form-control" value="" required="" autofocus>
                                         <input name="name" id="name" class="form-control" type="email" required=""  >
                                            <input name="ss" id="ss"  class="form-control" type="text" value="" required="" >
                                        <input name="bb" id="bb"  class="form-control" type="phone" value="" required="" >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <span class="form-label" style="color:black;">อีเมล</span>
                                        <input name="name" id="name" class="form-control" type="email" required=""  >
                                            <input name="ss" id="ss"  class="form-control" type="text" value="" required="" >
                                        <input name="bb" id="bb"  class="form-control" type="phone" value="" required="" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">

                                    <div class="form-group">
                                        <span class="form-label" style="color:black;">ชื่อของผู้ให้ติดต่อกลับ</span>
                                        <input name="ss" id="ss"  class="form-control" type="text" value="" required="" >
                                        <input name="bb" id="bb"  class="form-control" type="phone" value="" required="" >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <span class="form-label" style="color:black;">เบอร์โทรติดต่อ</span>
                                        <input name="bb" id="bb"  class="form-control" type="phone" value="" required="" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">

                                    <div class="form-group">
                                        <span class="form-label" style="color:black;">ข้อมูลหรือรายละเอียดสินค้าที่ต้องการพัฒนา</span>
                                            <textarea class="form-control" name="cc" id="cc" rows="5" cols="43"></textarea>
                                    </div>
                                </div>

                            </div>

                            <div class="lab1">
                                <input name="input_16.1" type="checkbox" value="Consent" id="choice_42_16_1" value="" required="">
                                ในการดำเนินการสมัครเข้าร่วมพัฒนาสินค้า Digital Solution ข้าพเจ้าตกลงและยินยอมให้
                                    NT CONNX รวบรวมและใช้ข้อมูลที่ข้าพเจ้าให้ไว้เพื่อดำเนินการตามที่จำเป็นภายใต้<a style="color: #5B5BFF; "
                                        href="privacy_policy">นโยบายส่วนบุคคลของ NT
                                    </a>ที่ข้าพเจ้าได้อ่านและเข้าใจเนื้อหาทั้งหมดแล้ว
                            </div>
                            <div class="form-group">

                     <input type = "submit" name="submit" class="button button11 btn-block " onClick="insert_value()" value = "เข้าร่วม">
                     </div>
                        </form>

                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section> -->
<!-- end banner -->
<!-- end banner -->
<section id="testimonial">
   <div class="sec-prefooter">
      <div class="container">
         <div class="box-prefooter t-bold">
            <div class="prefoot-contact-title">สอบถามข้อมูลเพิ่มเติมได้ที่</div>
            <div class="prefoot-contact-group">
               <div class="prefoot-contact-group-call">
                  <div class="prefoot-contact-group-call-text">ศูนย์บริการ<br>ลูกค้าธุรกิจ</div>
                  <a href="#" class="prefoot-contact-group-call-num txt-gdcolor">NT ConnX</a>
               </div>
               <div class="prefoot-contact-group-or t-medium"><span>หรือ</span></div>
               <div class="prefoot-contact-group-call"><a href="javascript:void(0)" onclick="window.location.href='form'" class="btn3 btn3-secondary"><span>สอบถามข้อมูลเพิ่มเติม</span></a></div>
            </div>
         </div>
      </div>
   </div>
</section>