<?php
//require_once __DIR__."/Config/config.php";
//require_once __DIR__."/Config/firebaseRDB.php";

if (!isset($_SESSION['id']) && !isset($_SESSION['email']) && !isset($_SESSION['employeeID']) && !isset($_SESSION['nameEN']) && !isset($_SESSION['role'])) {
    unset($_SESSION['id']);
    unset($_SESSION['email']);
    unset($_SESSION['employeeID']);
    unset($_SESSION['nameEN']);
    unset($_SESSION['role']);
}
session_destroy();
header("location: ./home");
