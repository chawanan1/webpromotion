<?php
foreach ($resultProduct as $item) {
    for ($i = 0; $i < count($item); $i++) {
?>
        <div class="modal fade" id="myModal<?php echo  $item[$i]->id ?>" role="dialog" value="1">
            <div class="modal-dialog">
                <div class="col-12 col-sm-12" style="color:#06245A">
                    <!-- Modal content-->
                    <div class="modal-content">

                        <div class="modal-body">
                            <p style="text-align:center">
                                <img src="<?php echo $item[$i]->Image ?>" alt="Image" class="img-fluid" width="40%">
                            </p>
                            <p style="text-align: center;font-size: 26px;"> <?php echo  $item[$i]->Package ?> </p>
                            <div class="modal-header"> </div>
                            <b>รายละเอียดสินค้า</b>
                            </br>
                            <ul>
                                <li class="pros">
                                    <?php echo  $item[$i]->Detail1 ?></li>
                                <li class="pros">
                                    <?php echo  $item[$i]->Detail2 ?></li>
                                <li class="pros">
                                    <?php echo  $item[$i]->Detail3 ?></li>
                                <li class="pros">
                                    <?php echo  $item[$i]->Detail4 ?></li>
                            </ul>

                            <b>หมวดหมู่</b></br>
                            <ul>
                                <li class="pros">
                                    <?php echo  $item[$i]->Service ?></li>

                            </ul>

                            <div class="col">
                            </div>

                            <b style="color:red">สนใจติดต่อได้ที่ E-Mail : </b>
                            <a href="mailto:connx@ntplc.co.th" class="clickl"><b>connx@ntplc.co.th</b></a></br>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php }
}

?>