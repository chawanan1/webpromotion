<div class ="container">
    <div class="row">
<div class="card pad-top-100">
    <div class="card-header">
        <h3 class="card-title">DataTable with default features</h3>
    </div>
</div>
    </div>
</div>
<div class="container1">
    <div class="card-body">
        <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="dt-buttons btn-group flex-wrap"> <button
                            class="btn btn-secondary buttons-copy buttons-html5" tabindex="0" aria-controls="example1"
                            type="button"><span>Copy</span></button> <button
                            class="btn btn-secondary buttons-csv buttons-html5" tabindex="0" aria-controls="example1"
                            type="button"><span>CSV</span></button> <button
                            class="btn btn-secondary buttons-excel buttons-html5" tabindex="0" aria-controls="example1"
                            type="button"><span>Excel</span></button> <button
                            class="btn btn-secondary buttons-pdf buttons-html5" tabindex="0" aria-controls="example1"
                            type="button"><span>PDF</span></button> <button class="btn btn-secondary buttons-print"
                            tabindex="0" aria-controls="example1" type="button"><span>Print</span></button>
                        <div class="btn-group"><button
                                class="btn btn-secondary buttons-collection dropdown-toggle buttons-colvis" tabindex="0"
                                aria-controls="example1" type="button" aria-haspopup="true"><span>Column
                                    visibility</span><span class="dt-down-arrow"></span></button></div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div id="example1_filter" class="dataTables_filter"><label>Search:<input type="search"
                                class="form-control form-control-sm" placeholder="" aria-controls="example1"></label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <table id='dataTbl' class="table table-bordered table-striped dataTable dtr-inline"
                        >
                        <thead>
                            <tr>
                            <!-- <th class="sorting sorting_asc" tabindex="0" aria-controls="" rowspan="1"
                                    colspan="1" aria-sort="ascending"
                                    aria-label="Rendering engine: activate to sort column descending">ลำดับ
                                </th> -->
                                <th class="sorting sorting_asc" tabindex="0" aria-controls="" rowspan="1"
                                    colspan="1" aria-sort="ascending"
                                    aria-label="Rendering engine: activate to sort column descending">รหัสพนักงาน
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="" rowspan="1" colspan="1"
                                    aria-label="Browser: activate to sort column ascending">ชื่อ-นามสกุล
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="" rowspan="1" colspan="1"
                                    aria-label="Platform(s): activate to sort column ascending">คำนำชื่อ-อังกฤษ
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="" rowspan="1" colspan="1"
                                    aria-label="Engine version: activate to sort column ascending">ชื่อ-อังกฤษ
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="" rowspan="1" colspan="1"
                                    aria-label="CSS grade: activate to sort column ascending">นามสกุล-อังกฤษ
                                </th>
                                <!--  -->
                                <th class="sorting" tabindex="0" aria-controls="" rowspan="1" colspan="1"
                                    aria-label="CSS grade: activate to sort column ascending">ตำแหน่งเต็ม
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="" rowspan="1" colspan="1"
                                    aria-label="CSS grade: activate to sort column ascending">ตำแหน่ง
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="" rowspan="1" colspan="1"
                                    aria-label="CSS grade: activate to sort column ascending">ต.บริหาร
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="" rowspan="1" colspan="1"
                                    aria-label="CSS grade: activate to sort column ascending">CCTR
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="" rowspan="1" colspan="1"
                                    aria-label="CSS grade: activate to sort column ascending">รหัสส่วนงาน
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="" rowspan="1" colspan="1"
                                    aria-label="CSS grade: activate to sort column ascending">ส่วนงาน
                                </th>
                               <!--  <th class="sorting" tabindex="0" aria-controls="" rowspan="1" colspan="1"
                                    aria-label="CSS grade: activate to sort column ascending">ชื่อเต็มส่วนงาน
                                </th> -->
                                <th class="sorting" tabindex="0" aria-controls="" rowspan="1" colspan="1"
                                    aria-label="CSS grade: activate to sort column ascending">e-mail
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="" rowspan="1" colspan="1"
                                    aria-label="CSS grade: activate to sort column ascending">โทรศัพท์
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="" rowspan="1" colspan="1"
                                    aria-label="CSS grade: activate to sort column ascending">สิทธิ์เข้าถึง
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="" rowspan="1" colspan="1"
                                    aria-label="CSS grade: activate to sort column ascending">โทรสาร
                                </th>
                               <!--  <th class="sorting" tabindex="0" aria-controls="" rowspan="1" colspan="1"
                                    aria-label="CSS grade: activate to sort column ascending">จังหวัด
                                </th> -->
                                <th class="sorting" tabindex="0" aria-controls="" rowspan="1" colspan="1"
                                    aria-label="CSS grade: activate to sort column ascending">รหัสสถานที่ทำงาน
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="" rowspan="1" colspan="1"
                                    aria-label="CSS grade: activate to sort column ascending">Organization Key
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="" rowspan="1" colspan="1"
                                    aria-label="CSS grade: activate to sort column ascending">Manager
                                </th>
                                    
                            </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                        <tfoot>
                            <tr>
                            <!-- <th rowspan="1" colspan="1">ลำดับ</th> -->
                                <th rowspan="1" colspan="1">รหัสพนักงาน</th>
                                <th rowspan="1" colspan="1">ชื่อ-นามสกุล</th>
                                <th rowspan="1" colspan="1">คำนำชื่อ-อังกฤษ</th>
                                <th rowspan="1" colspan="1">ชื่อ-อังกฤษ</th>
                                <th rowspan="1" colspan="1">นามสกุล-อังกฤษ</th>
                                <th rowspan="1" colspan="1">ตำแหน่งเต็ม</th>
                                <th rowspan="1" colspan="1">ตำแหน่ง</th>
                                <th rowspan="1" colspan="1">ต.บริหาร</th>
                                <th rowspan="1" colspan="1">CCTR</th>
                                <th rowspan="1" colspan="1">รหัสส่วนงาน</th>
                                <th rowspan="1" colspan="1">ส่วนงาน</th>
                                <!-- <th rowspan="1" colspan="1">ชื่อเต็มส่วนงาน</th> -->
                                <th rowspan="1" colspan="1">e-mail</th>
                                <th rowspan="1" colspan="1">โทรศัพท์</th>
                                <th rowspan="1" colspan="1">สิทธิ์เข้าถึง</th>
                                <th rowspan="1" colspan="1">โทรสาร</th>
                                <!-- <th rowspan="1" colspan="1">จังหวัด</th> -->
                                <th rowspan="1" colspan="1">รหัสสถานที่ทำงาน</th>
                                <th rowspan="1" colspan="1">Organization Key</th>
                                <th rowspan="1" colspan="1">Manager</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            
        </div>
    </div>
</div>



<div class="col-md-6">
  <form>
    
  <button type="submit" id="submit" ></button>
</form>
</div>

  <!-- <button type="submit" id="getData" ></button> -->


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

<script type="module">
  // Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/9.6.1/firebase-app.js";
  import { getDatabase, set, ref ,push, child, onValue} from "https://www.gstatic.com/firebasejs/9.6.1/firebase-database.js";

  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  const firebaseConfig = {
    /* apiKey: "AIzaSyDxp3na6_IkbB3AUXZfpfcyKjv6IdahTPo", */
    /* authDomain: "real-v3-d6dd7.firebaseapp.com", */
    /* databaseURL: "https://real-v3-d6dd7-default-rtdb.firebaseio.com",  Real V3*/ 
   /*  projectId: "real-v3-d6dd7", */
   /*  storageBucket: "real-v3-d6dd7.appspot.com", */
    /* messagingSenderId: "714190408837", */
    /* appId: "1:714190408837:web:f0cb0c2c0f26df659fadec",
    measurementId: "G-BD3YD0YRK1" */
    /* ConnX-Web */
    apiKey: "AIzaSyAwf-isuNoI5w3g3OwOE5LA_BeZeWAgyR8",
    authDomain: "connx-db-website.firebaseapp.com",
    databaseURL: "https://connx-db-website-default-rtdb.firebaseio.com",
    projectId: "connx-db-website",
    storageBucket: "connx-db-website.appspot.com",
    messagingSenderId: "715014044723",
    appId: "1:715014044723:web:b98b399d3615d8ec41b746",
    measurementId: "G-790THM0GRL"
  };

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  
  // Get a reference to the database service
  const database = getDatabase(app);

  // write data
  submit.addEventListener('click',(e) => {
    var employeeID = document.getElementById('employeeID').value;  
    var full_name = document.getElementById('full_name').value;  
    /*  */
    var prefixEN = document.getElementById('prefixEN').value;  
    var nameEN = document.getElementById('nameEN').value;  
    var lastnameEN = document.getElementById('lastnameEN').value;  
    var rankFull = document.getElementById('rankFull').value;  
    var rank = document.getElementById('rank').value;  
    var rankAd = document.getElementById('rankAd').value;  
    var cctr = document.getElementById('cctr').value;  
    var no_Segment = document.getElementById('no_Segment').value;  
    var segment = document.getElementById('segment').value;  
    var full_Segment = document.getElementById('full_Segment').value;  
    var email = document.getElementById('email').value;  
    var phone = document.getElementById('phone').value;  
    var role = document.getElementById('role').value;  
    var fax = document.getElementById('fax').value;  
    var county = document.getElementById('county').value; 
    var workplace_Code = document.getElementById('workplace_Code').value; 
    /* var full_name = document.getElementById('full_name').value;  */
    /*  */
    var ogt_Key = document.getElementById('ogt_Key').value;  

    const userId = push(child(ref(database), 'user')).key;
   
    set(ref(database, 'user' + userId), {
    employeeID: employeeID,
    full_name: full_name,
    /*  */
    prefixEN: prefixEN,
    nameEN: nameEN,
    lastnameEN: lastnameEN,
    rankFull: rankFull,
    rank: rank,
    rankAd: rankAd,
    cctr: cctr,
    no_Segment: no_Segment,
    segment: segment,
    full_Segment: full_Segment,
    email: email,
    phone: phone,
    role: role,
    fax: fax,
    county: county,
    workplace_Code: workplace_Code,
    /* lastName: lastName, */
    /*  */
    ogt_Key : ogt_Key
   });
   alert('saved');
  });

  // read data
  

    $('#dataTbl td').remove();
    var rowNum = 0; 
    const dbRef = ref(database, 'user');

    onValue(dbRef, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
      const childKey = childSnapshot.key;
      const childData = childSnapshot.val();
      // ...
      rowNum += 1; 
      var row = "<tr><td>" 
      + childData.employeeID + "</td><td>" 
      + childData.full_name + "</td><td>" 
      + childData.prefixEN + "</td><td>"
      + childData.nameEN + "</td><td>"
      + childData.lastnameEN + "</td><td>"
      + childData.rankFull + "</td><td>"
      + childData.rank + "</td><td>"
      + childData.rankAd + "</td><td>"
      + childData.cctr + "</td><td>"
      + childData.no_Segment + "</td><td>"
      + childData.segment + "</td><td>"
      /* + childData.full_Segment + "</td><td>" */
      + childData.email + "</td><td>"
      + childData.phone + "</td><td>"
      + childData.role + "</td><td>"
      + childData.fax + "</td><td>"
      /* + childData.county + "</td><td>" */
      + childData.workplace_Code + "</td><td>"
      + childData.ogt_Key + "</td><td>"
     /*  + childData.Ation + "</td></tr>" */
      + "<input type='button' id='btnDelete' value='Delete' class='btn btn-danger' /></td></tr>"
      /*  */

      $(row).appendTo('#dataTbl');
      
      });
    }, {
       onlyOnce: true
    });
 // remove data
 removeData.addEventListener('click',(e) => {
     var username = document.getElementById('email').value;
    
     remove(ref(database, 'users/' + username));
     alert('removed');
    });
    

  ;
</script>

