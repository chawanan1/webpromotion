<div class="limiter pad-top-80">
    <div class="container-login100">

        <div class="wrap-login100">

            <div class="login100-pic js-tilt" data-tilt>
                <img src="./assets/vendor/images/img-01.png" alt="IMG">
            </div>

            <form method="post" action="Auth/Login" onsubmit="return validate()" class="login100-form validate-form">
                <span class="login100-form-title">
                    <button style="font-size:40px; font-weight:bold; color:black;"><i class="fa fa-home"> </i> NT
                        Conn|X</button>
                </span>

                <div class="wrap-input100 validate-input" data-validate="Valid EMAIL NT is required: your EMAIL NT">
                    <input class="input100" type="text" id="email" name="email" placeholder="xxx.xxx@ntplc.co.th" oninput="validate()">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                    </span>
                    <center><small id="emaillog"></small></center>
                </div>




                <div class="wrap-input100 validate-input" data-validate="Your Password">
                    <input class="input100" type="password" id="employeeID" name="employeeID" placeholder="Password[รหัสพนักงาน]" oninput="validate()" maxlength="8">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
                        <i class="fa fa-lock" aria-hidden="true"></i>
                    </span>
                    <center><small id="employeeIDlog"></small></center>

                </div>


                <div class="container-login100-form-btn">
                    <button type="submit" class="login100-form-btn">
                        LOGIN
                    </button>
                    <!-- <input type="submit" value="Login" /> -->
                </div>

                <div class="text-center p-t-136">

                </div>
            </form>

        </div>
    </div>
</div>

<script>
    function validate() {
        var email = document.getElementById('email').value;
        var employeeID = document.getElementById('employeeID').value;
        var status = false;

        if (!email) {
            document.getElementById("emaillog").innerHTML = "<div class='text-danger'> กรุณากรอก Email </div>";
            status = false;
        } else {
            if (/\S+@\S+\.\S+/.test(email)) {
                document.getElementById("emaillog").innerHTML = "";
                status = true;
            } else {
                document.getElementById("emaillog").innerHTML = "<div class='text-danger'> กรุณากรอกข้อมูลในรูปแบบ Email เท่านั้น </div>";
                status = false;
            }
        }


        if (!employeeID) {
            document.getElementById("employeeIDlog").innerHTML = "<div class='text-danger'> กรุณากรอกรหัสพนักงาน </div>";
            status = false;
        } else if (employeeID.length < 8) {
            document.getElementById("employeeIDlog").innerHTML = "<div class='text-danger'> กรุณากรอกรหัสพนักงานให้ครบ 8 หลัก </div>";
            status = false;
        } else {
            if (/^\d+$/.test(employeeID)) {} else {
                document.getElementById("employeeIDlog").innerHTML = "<div class='text-danger'> กรุณากรอกข้อมูลในรูปแบบตัวเลขเท่านั้น </div>";
                status = false;
            }
        }
        return status;
    }
</script>