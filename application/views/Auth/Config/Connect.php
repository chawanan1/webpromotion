<!-- ** [ วิธีแก้ไขรูป แก้ไขหลัง = ตัวอย่าง ->> ='1.png'  <<- ] ** -->
<?php
// ===============================[ส่วนของ หน้า Home Sell ]===================================//
//PROMOTION - ตั้งค่ารูปโปรโมชั่น [หน้า HOME Sell HOT PROMOTION]  อัพโหลดรูปเข้าไปที่ /assets/img/HOTPROMOTION
$_CONFIG['PROIMG']['HOT1'] = '1.png';
$_CONFIG['PROIMG']['HOT2'] = '2.png';
$_CONFIG['PROIMG']['HOT3'] = '3.png';
$_CONFIG['PROIMG']['HOT4'] = '4.png';
$_CONFIG['PROIMG']['HOT5'] = '7.png';

// ===============================[ส่วนของ หน้า Home  Sell]===================================//
// ตั้งค่ารูปแบบเนอร์ [หน้า HOME & Home Sell BANNER]  อัพโหลดรูปเข้าไปที่ /assets/img/
$_CONFIG['Banner-Home']['1'] = 'Banner3.png';


// ===============================[ ส่วนของ หน้า Sell ประกาศในหน้า HomeSell ]===================================//
$_CONFIG['News-sell']['1'] = 'ConnX Product Catalog'; //ข้อความที่ต้องการให้โชว์ 1
$_CONFIG['News-sell']['2'] = 'หนังสือสัญญา Connectivity Hub'; //ข้อความที่ต้องการให้โชว์ 2
$_CONFIG['News-sell']['3'] = 'วิธีปฏิบัติ NT ConnX'; //ข้อความที่ต้องการให้โชว์ 3
$_CONFIG['News-date']['1'] = '24/11/2565'; // วันที่ต่อท้ายข้อความ 

$_CONFIG['img-newsell']['1'] = 'neww.gif'; // รูปภาพ New ต่อท้ายข้อความ รูปเอาไปไว้ที่ /assets/images/neww.gif


// ===============================[ ส่วนของ หน้า Sell ประกาศในหน้า New_event ]===================================//
$_CONFIG['News-event']['1'] = 'ConnX Product Catalog'; //ข้อความที่ต้องการให้โชว์ 1
$_CONFIG['News-event']['2'] = 'สัญญาใช้บริการ บริการ Connectivity Hub (NT ConnX)'; //ข้อความที่ต้องการให้โชว์ 2
$_CONFIG['News-event']['3'] = 'วิธีปฏิบัติ NT ConnX'; //ข้อความที่ต้องการให้โชว์ 3
$_CONFIG['News-event-date']['1'] = '24 พฤศจิกายน 2565 | จำนวนการเข้าชม 1'; // วันที่ต่อท้ายข้อความ 


// ===========================================================================================================//

//NT Connectivity - ตั้งค่ารูปโปรโมชั่น [ NT Connectivity]  อัพโหลดรูปเข้าไปที่ /assets/img/NT_Connectivity
//รูปจะลิงค์ทุกหน้าที่เป็น NT Connectivity
$_CONFIG['CONN']['CONN1'] = '1.png'; // หมวดหมู่ [บริการ] NT Connectivity
$_CONFIG['Text-CONN']['1'] = 'Eval'; //ข้อความที่ต้องการให้โชว์  //[แพ็กเกจ] : Name Product
$_CONFIG['Text-Price']['1'] = '฿999.00'; //ราคาที่ต้องการให้โชว์  // [Price] : ฿999.00
$_CONFIG['Text-Detail']['1-0'] = 'Internet : 3G/4G (2100,2300)'; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Detail']['1-1'] = 'Speed : 1 Mbps'; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Detail']['1-2'] = 'Data : 1 GB/ปี'; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Detail']['1-3'] = 'สำหรับ : ผู้เริ่มต้นธุรกิจ'; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Category']['1'] = 'NT Connectivity'; //หมวดหมู่ ที่ต้องการให้โชว์  // [หมวดหมู่] : NT Connectivity
$_CONFIG['Text-feature']['1'] = 'แพ็กเกจ : เหมาะสำหรับเริ่มต้นธุระกิจ'; //คุณสมบัติ ที่ต้องการให้โชว์  // [แพ็กเกจ] : สำหรับผู้ประกอบธุระกิจ Digital และ IoT



// =================================================[2]================================================= //
$_CONFIG['CONN']['CONN2'] = '2.png'; // หมวดหมู่ [บริการ] NT Connectivity
$_CONFIG['Text-CONN']['2'] = 'iConn1'; //ข้อความที่ต้องการให้โชว์  //[แพ็กเกจ] : Name Product
$_CONFIG['Text-Price']['2'] = '฿999.00'; //ราคาที่ต้องการให้โชว์ // [Price] : ฿999.00
$_CONFIG['Text-Detail']['2-0'] = 'Internet : 3G/4G (2100,2300)'; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Detail']['2-1'] = 'Speed : Max Speed'; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Detail']['2-2'] = 'Data : 6 GB/เดือน Fub 512 Kbps'; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Detail']['2-3'] = 'สำหรับ : ผู้ประกอบการธุรกิจ Digital และ IoT'; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Category']['2'] = 'NT Connectivity'; //หมวดหมู่ ที่ต้องการให้โชว์  // [หมวดหมู่] : NT Connectivity
$_CONFIG['Text-feature']['2'] = 'แพ็กเกจ : สำหรับผู้ประกอบธุระกิจ Digitalและ IoT'; //คุณสมบัติ ที่ต้องการให้โชว์  // [แพ็กเกจ] : สำหรับผู้ประกอบธุระกิจ Digital และ IoT

                                

// =================================================[3]================================================= //
$_CONFIG['CONN']['CONN3'] = '3-1.png'; // หมวดหมู่ [บริการ] NT Connectivity
$_CONFIG['Text-CONN']['3'] = 'Confirm / Confix SIM'; //ข้อความที่ต้องการให้โชว์  //[แพ็กเกจ] : Name Product
$_CONFIG['Text-Price']['3'] = '฿999.00'; //ราคาที่ต้องการให้โชว์  // [Price] : ฿999.00
$_CONFIG['Text-Detail']['3-0'] = 'Internet : 4G (2300)'; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Detail']['3-1'] = 'Speed : Max Speed'; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Detail']['3-2'] = 'Data : 1 TB/เดือน'; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Detail']['3-3'] = 'สำหรับ : ผู้ประกอบการธุรกิจ Digital และ IoT ที่ต้องการ Fix IP หรือ Remote เข้าตัวอุปกรณ์ IoT หรือใช้งาน CCTV'; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Category']['3'] = 'NT Connectivity'; //หมวดหมู่ ที่ต้องการให้โชว์  // [หมวดหมู่] : NT Connectivity
$_CONFIG['Text-feature']['3'] = ' แพ็กเกจ : ผู้ประกอบการธุรกิจ ที่ต้องการFix IP หรือ Remote เข้าตัวอุปกรณ์ IoT หรือใช้งาน CCTV '; //คุณสมบัติ ที่ต้องการให้โชว์  // [แพ็กเกจ] : สำหรับผู้ประกอบธุระกิจ Digital และ IoT



// =================================================[4]================================================= //
//NT Platform - ตั้งค่ารูปโปรโมชั่น [ NT Platform]  อัพโหลดรูปเข้าไปที่ /assets/img/NT_Platform
//รูปจะลิงค์ทุกหน้าที่เป็น NT Platform
$_CONFIG['Platform']['Platform1'] = '4.png'; // หมวดหมู่ [บริการ] NT Platform1
$_CONFIG['Text-Platform']['4'] = 'Blooming ConnX Creative'; //ข้อความที่ต้องการให้โชว์  //[แพ็กเกจ] : Name Product
$_CONFIG['Text-Price']['4'] = '฿999.00'; //ราคาที่ต้องการให้โชว์  // [Price] : ฿999.00
$_CONFIG['Text-Detail']['4-0'] = 'SIM ConnX Eval : จำนวน 2 SIM'; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Detail']['4-1'] = 'ใช้งาน Thingsboard Platform ด้วย Data 5 GB/เดือน'; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Detail']['4-2'] = ''; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Detail']['4-3'] = ''; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Category']['4'] = 'NT Platform'; //หมวดหมู่ ที่ต้องการให้โชว์  // [หมวดหมู่] : NT Connectivity
$_CONFIG['Text-feature']['4'] = 'แพ็กเกจ : Thingsboard Platform ด้วย Data 5 GB/เดือน / 2 SIM'; //คุณสมบัติ ที่ต้องการให้โชว์  // [แพ็กเกจ] : สำหรับผู้ประกอบธุระกิจ Digital และ IoT



// =================================================[5]================================================= //
$_CONFIG['Platform']['Platform2'] = '5.png'; // หมวดหมู่ [บริการ] NT Platform1
$_CONFIG['Text-Platform']['5'] = 'Blooming ConnX Productive'; //ข้อความที่ต้องการให้โชว์  //[แพ็กเกจ] : Name Product
$_CONFIG['Text-Price']['5'] = '฿999.00'; //ราคาที่ต้องการให้โชว์  // [Price] : ฿999.00
$_CONFIG['Text-Detail']['5-0'] = 'Blooming ConnX Productive'; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Detail']['5-1'] = 'SIM ConnX Eval : จำนวน 8 SIM'; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Detail']['5-2'] = 'ใช้งาน Thingsboard Platform ด้วย Data 5 GB/เดือน '; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Detail']['5-3'] = ''; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Category']['5'] = 'NT Platform'; //หมวดหมู่ ที่ต้องการให้โชว์  // [หมวดหมู่] : NT Connectivity
$_CONFIG['Text-feature']['5'] = 'แพ็กเกจ : Thingsboard Platform ด้วย Data 5 GB/เดือน / 8 SIM '; //คุณสมบัติ ที่ต้องการให้โชว์  // [แพ็กเกจ] : สำหรับผู้ประกอบธุระกิจ Digital และ IoT



// =================================================[6]================================================= //
$_CONFIG['Platform']['Platform3'] = '6.png'; // หมวดหมู่ [บริการ] NT Platform1
$_CONFIG['Text-Platform']['6'] = 'Blooming ConnX Performance'; //ข้อความที่ต้องการให้โชว์  //[แพ็กเกจ] : Name Product
$_CONFIG['Text-Price']['6'] = '฿999.00'; //ราคาที่ต้องการให้โชว์  // [Price] : ฿999.00
$_CONFIG['Text-Detail']['6-0'] = 'SIM ConnX Eval : จำนวน 16 SIM'; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Detail']['6-1'] = 'ใช้งาน Thingsboard Platform ด้วย Data 5 GB/เดือน '; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Detail']['6-2'] = 'ใช้งาน SMS API จำนวน 500 Message/เดือน'; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Detail']['6-3'] = ''; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Category']['6'] = 'NT Platform'; //หมวดหมู่ ที่ต้องการให้โชว์  // [หมวดหมู่] : NT Connectivity
$_CONFIG['Text-feature']['6'] = 'แพ็กเกจ : Thingsboard Platform ด้วย Data 5 GB/เดือน / 16 SIM / SMS API 500 Message'; //คุณสมบัติ ที่ต้องการให้โชว์  // [แพ็กเกจ] : สำหรับผู้ประกอบธุระกิจ Digital และ IoT



// =================================================[7]================================================= //
//NT Platform - ตั้งค่ารูปโปรโมชั่น [ NT solution]  อัพโหลดรูปเข้าไปที่ /assets/img/NT_solution
//รูปจะลิงค์ทุกหน้าที่เป็น NT Solution
$_CONFIG['solution']['solution1'] = '7.png'; // หมวดหมู่ [บริการ] NT Solution
$_CONFIG['Text-solution']['7'] = 'Internet Wi-Fi HotSpot by EasyNet'; //ข้อความที่ต้องการให้โชว์  //[บริการ] : Name Product
$_CONFIG['Text-Price']['7'] = '฿999.00'; //ราคาที่ต้องการให้โชว์  // [Price] : ฿999.00
$_CONFIG['Text-Detail']['7-0'] = ' บริการ Internet Wi-Fi HotSpot'; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Detail']['7-1'] = ''; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Detail']['7-2'] = ''; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Detail']['7-3'] = ''; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Category']['7'] = 'Digital Solutions by NT ConnX'; //หมวดหมู่ ที่ต้องการให้โชว์  // [หมวดหมู่] : NT Connectivity
$_CONFIG['Text-feature']['7'] = ' บริการ Internet Wi-Fi HotSpot'; //คุณสมบัติ ที่ต้องการให้โชว์  // [แพ็กเกจ] : สำหรับผู้ประกอบธุระกิจ Digital และ IoT



// =================================================[8]================================================= //
$_CONFIG['solution']['solution2'] = '8.png'; // หมวดหมู่ [บริการ] NT Solution
$_CONFIG['Text-solution']['8'] = 'Security Solution by AZKing'; //ข้อความที่ต้องการให้โชว์  //[บริการ] : Name Product
$_CONFIG['Text-Price']['8'] = '฿999.00'; //ราคาที่ต้องการให้โชว์  // [Price] : ฿999.00
$_CONFIG['Text-Detail']['8-0'] = 'บริการ Access Control สำหรับบ้านพัก และออฟฟิศ เช่น CCTV, IP Camera และ License page'; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Detail']['8-1'] = ''; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Detail']['8-2'] = ''; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Detail']['8-3'] = ''; //ดีเทล์ ที่ต้องการให้โชว์  // [รายละเอียดสินค้า] : 
$_CONFIG['Text-Category']['8'] = 'Digital Solutions by NT ConnX'; //หมวดหมู่ ที่ต้องการให้โชว์  // [หมวดหมู่] : NT Connectivity
$_CONFIG['Text-feature']['8'] = 'บริการ Access Control สำหรับบ้านพักและออฟฟิศ เช่น CCTV, IP Camera และ License page'; //คุณสมบัติ ที่ต้องการให้โชว์  // [แพ็กเกจ] : สำหรับผู้ประกอบธุระกิจ Digital และ IoT



// ===============================[สิ้นสุด ส่วนของ หน้า Home Sell]===================================//

// ===============================[ ส่วนของ หน้า Product sell ]===================================//
//BANNER Silde [หน้า Producer]  อัพโหลดรูปเข้าไปที่ /assets/img/product
//แก้ไขรูปที่เป็น แบนเนอร์หน้า โปรดัก สไลด์
$_CONFIG['Banner']['Slid1'] = 'B1.png';
$_CONFIG['Banner']['Slid2'] = 'B2.png';
$_CONFIG['Banner']['Slid3'] = 'B3.png';

$_CONFIG['Product-name']['1'] = 'ConnX Eval : SIM Internet'; // ชื่อสินค้าหน้า Product Sell
$_CONFIG['Product-name']['2'] = 'iConn1 : SIM Internet'; // ชื่อสินค้าหน้า Product Sell
$_CONFIG['Product-name']['3'] = 'conn Firm : SIM Fixed IP แบบ Public IP หรือ Private IP'; // ชื่อสินค้าหน้า Product Sell
$_CONFIG['Product-name']['4'] = 'Blooming ConnX : Creative'; // ชื่อสินค้าหน้า Product Sell
$_CONFIG['Product-name']['5'] = 'Blooming ConnX : Productive'; // ชื่อสินค้าหน้า Product Sell
$_CONFIG['Product-name']['6'] = 'Blooming ConnX : Performance'; // ชื่อสินค้าหน้า Product Sell
$_CONFIG['Product-name']['7'] = 'Hotspot WiFi Solution by : EasyNet'; // ชื่อสินค้าหน้า Product Sell
$_CONFIG['Product-name']['8'] = 'Security Solution by : AZKing'; // ชื่อสินค้าหน้า Product Sell


// =================[ ส่วนของ หน้า Document sell สำหรับโหลดเอกสาร & เพิ่มเอกสารให้โหลด ]===================================//
$_CONFIG['Doc-PDF']['1'] = 'NT ConnX Product Catalog.pdf';
$_CONFIG['Text-Doc']['1'] = 'ConnX Product Catalog';

$_CONFIG['Doc-PDF']['2'] = 'สัญญาใช้บริการ บริการ Connectivity Hub (NT ConnX)_ฉบับ 8 ก.ย. 65.pdf';
$_CONFIG['Text-Doc']['2'] = 'สัญญาใช้บริการ บริการ Connectivity Hub (NT ConnX)_ฉบับ 8 ก.ย. 65';

$_CONFIG['Doc-PDF']['3'] = 'วิธีปฏิบัติ NT ConnX.pdf';
$_CONFIG['Text-Doc']['3']  = 'วิธีปฏิบัติ NT ConnX  ';

$_CONFIG['Doc-PDF']['4'] = ''; // เพิ่ม PDF นำไฟล์ไปวางไว้ที่ /assets/pdf/ [แล้วนำชื่อไฟล์พร้อมนามสกุลมาใส่ใน '']
$_CONFIG['Text-Doc']['4'] = ''; // เพิ่ม TEXT ชื่อของเอกสาร '']

$_CONFIG['Doc-PDF']['5'] = '';
$_CONFIG['Text-Doc']['5'] = '';

$_CONFIG['Doc-PDF']['6'] = '';
$_CONFIG['Text-Doc']['6'] = '';

$_CONFIG['Doc-PDF']['7'] = '';
$_CONFIG['Text-Doc']['7'] = '';
$_CONFIG['Doc-PDF']['8'] = '';
$_CONFIG['Text-Doc']['8'] = '';
$_CONFIG['Doc-PDF']['9'] = '';
$_CONFIG['Text-Doc']['9'] = '';
$_CONFIG['Doc-PDF']['10'] = '';
$_CONFIG['Text-Doc']['10'] = '';



























// =====================================[ Extention // ส่วนเสริมของเว็ปไซต์]=====================================//
$_CONFIG['img-Doc-pdf']['4'] = 'pdf-icn.png'; // pdf-icn.png
$_CONFIG['Text-Doc-Dowloads']['4'] = 'ดาวน์โหลด'; // ดาวน์โหลด
$_CONFIG['img-Doc-Downloads']['4'] = 'arrow-icn.png'; // arrow-icn.png

$_CONFIG['img-Doc-pdf']['5'] = 'pdf-icn.png'; // pdf-icn.png
$_CONFIG['Text-Doc-Dowloads']['5'] = 'ดาวน์โหลด'; // ดาวน์โหลด
$_CONFIG['img-Doc-Downloads']['5'] = 'arrow-icn.png'; // arrow-icn.png
