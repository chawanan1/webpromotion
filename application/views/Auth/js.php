<!-- end footer-main -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script>
    var script_url =
        "https://script.google.com/macros/s/AKfycbwO9Q3M44UjadS5k06M1WallIMAdJyB5RyFyYb3NUa3OkulOzUZU2s8NIg_981gV-sSjQ/exec";

    // Make an AJAX call to Google Script
    function insert_value() {

        var id1 = $("#id").val();
        var name = $("#name").val();
        var ph = $("#ph").val();
        var f1 = $("#f1").val();
        var f2 = $("#f2").val();


        var url = script_url + "?callback=ctrlq&name=" + name + "&id=" + id1 + "&ph=" + ph + "&f1=" + f1 + "&f2=" + f2 +
            "&action=insert";


        var request = jQuery.ajax({
            crossDomain: true,
            url: url,
            method: "GET",
            dataType: "jsonp"
        });

    }
</script>
<script>
    function openFormID() {
        document.getElementById("myFormID").style.display = "block";
    }

    function openForm1() {
        document.getElementById("myForm1").style.display = "block";
    }

    function closeFormID() {
        document.getElementById("myFormID").style.display = "none";
    }

    function closeForm1() {
        document.getElementById("myForm1").style.display = "none";
    }
</script>
<script src="../assets/js/cookie.js">
</script>
<!-- ALL JS FILES -->
<script src="../assets/js/all.js"></script>
<script src="../assets/js/bootstrap.min.js">
</script>
<!-- ALL PLUGINS -->
<script src="../assets/js/custom.js">
</script>
<script src="../assets/js/owl.carousel.min.js">
</script>
<!-- [LOGIN Firerbase] -->
<script src="https://www.gstatic.com/firebasejs/8.6.7/firebase-app.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
    https://firebase.google.com/docs/web/setup#available-libraries -->
<script src="https://www.gstatic.com/firebasejs/8.6.7/firebase-analytics.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.6.7/firebase-auth.js"></script>

<script src="../assets/js/firebase.js"></script>
<script src="../assets/js/welcome.js"></script>
<script type="module">
    // Import the functions you need from the SDKs you need
    import {
        initializeApp
    } from "https://www.gstatic.com/firebasejs/9.10.0/firebase-app.js";
    import {
        getAnalytics
    } from "https://www.gstatic.com/firebasejs/9.10.0/firebase-analytics.js";
    // TODO: Add SDKs for Firebase products that you want to use
    // https://firebase.google.com/docs/web/setup#available-libraries

    // Your web app's Firebase configuration
    // For Firebase JS SDK v7.20.0 and later, measurementId is optional
    const firebaseConfig = {
        apiKey: "AIzaSyDxp3na6_IkbB3AUXZfpfcyKjv6IdahTPo",
        authDomain: "real-v3-d6dd7.firebaseapp.com",
        projectId: "real-v3-d6dd7",
        storageBucket: "real-v3-d6dd7.appspot.com",
        messagingSenderId: "714190408837",
        appId: "1:714190408837:web:ddaa56a489518e359fadec",
        measurementId: "G-PT22VJSFVH"
    };

    // Initialize Firebase
    const app = initializeApp(firebaseConfig);
    const analytics = getAnalytics(app);
</script>

</body>

</html>