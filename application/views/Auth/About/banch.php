<!--======================================== //    Start Silde Show Banner // ======================================================-->
<div class="container text-center">
    <div class="alignfull-page">
        <!-- <a href="about"><img class="img-responsive"
                    src="/assets/img/T11.png"></a> -->
        <img class="img-responsive" src="/assets/img/commu/digital-tech.png">
    </div>
</div>

<!--======================================== //    End Silde Show Banner // ===================================================-->
<section class="page-section" id="services" style="padding-bottom: 2rem;">
    <div class="container" style="margin-top: 5%;">
        <a class="btn1 btn-link" href="../home" style="color: #545859;margin-bottom: 2%; text-decoration: underline" type="button">
            <svg class="svg-inline--fa fa-arrow-circle-left fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-circle-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                <path fill="currentColor" d="M256 504C119 504 8 393 8 256S119 8 256 8s248 111 248 248-111 248-248 248zm28.9-143.6L209.4 288H392c13.3 0 24-10.7 24-24v-16c0-13.3-10.7-24-24-24H209.4l75.5-72.4c9.7-9.3 9.9-24.8.4-34.3l-11-10.9c-9.4-9.4-24.6-9.4-33.9 0L107.7 239c-9.4 9.4-9.4 24.6 0 33.9l132.7 132.7c9.4 9.4 24.6 9.4 33.9 0l11-10.9c9.5-9.5 9.3-25-.4-34.3z"></path>
            </svg>

            กลับสู่หน้าข่าวทั้งหมด
        </a>
        <div class="text-left margin">
            <h12>ตัวอย่างผลงาน NT Smart City BanChang</h12>
        </div>
        <br>
        <p style="text-indent: 4rem; margin-top: 2%;">
            <strong><i>เมื่อวันที่ 12 มีนาคม 2564 บริษัท โทรคมนาคมแห่งชาติ จำกัด(มหาชน) ร่วมกับ คณะกรรมการนโยบายเขตพัฒนาเศรษฐกิจพิเศษภาคตะวันออก (สกพอ.) และภาคเอกชนหลายแห่ง ร่วมกันจัดหาเทคโนโลยีชั้นนำจากทั่วโลกมาเพื่อออกแบบ 5G Solution ที่ตอบ
                    โจทย์ระบบการบริหารจัดการเมืองเป็นเมืองต้นแบบ 5G ในพื้นที่เทศบาลบ้านฉาง อำเภอบ้านฉาง จังหวัดระยอง
                </i></strong>
        </p>
        <div style="text-align: center;">
            <div class="row justify-content-lg-center">
                <div class="col-12 col-sm-12 col-lg-12">

                    <img style="height: 20em;" class="img-fluid" src="/assets/img/commu/b9-1.png">


                </div>
            </div>
        </div>
        <br>
        <p class="text-ab">
            <strong1>ลักษณะการพัฒนาเมืองอัจฉริยะ (SMART CITY)</strong1> รองกรรมการผู้จัดการใหญ่ สายงานการตลาด บริษัท โทรคมนาคมแห่งชาติ จำกัด(มหาชน) หรือ NT เปิดเผยความคืบหน้ากลุ่มธุรกิจดาวเทียมว่า NT ได้รับความไว้วางใจให้ดำเนินการจัดสร้างสถานีเกตเวย์ในภูมิภาคอาเซียนสำหรับ OneWeb เครือข่ายดาวเทียมบรอดแบนด์ระดับโลกจากประเทศอังกฤษ เพื่อเป็นสถานีเกตเวย์ภาคพื้นดินทำหน้าที่บริหารจัดการเครือข่ายดาวเทียม OneWeb ซึ่งมีเป้าหมายให้บริการเครือข่ายสื่อสารผ่านดาวเทียมวงโคจรต่ำทั่วโลก และเป้าหมายให้บริการอินเทอร์เน็ตผ่านดาวเทียมครอบคลุมภูมิภาคอาเซียนในช่วงไตรมาสที่ 2 ของปี 2566

        </p>
        <p class="text-ab">
            <strong1>สิ่งแวดล้อมอัจฉริยะ (Smart Environment)</strong1> เป็นเมืองที่มุ่งเน้นปรับปรุงคุณภาพและเพิ่มประสิทธิภาพ ประสิทธิผล
            การบริหารจัดการ และติดตามเฝ้าระวัง สิ่งแวดล้อมและสภาวะ
            แวดล้อมอย่างเป็นระบบ เช่น การจัดการน้ำ การดูแลสภาพอากาศ การ
            เฝ้าระวังภัยพิบัติตลอดจนเพิ่มการมีส่วนร่วมของประชาชนในการ
            อนุรักษ์ทรัพยากรธรรมชาติ

        </p>
        <p class="text-ab">
            <strong1>การเดินทางและขนส่งอัจฉริยะ (Smart Mobility)</strong1> เป็นเมืองที่มุ่งเน้นเพิ่มความสะดวก ประสิทธิภาพ และความปลอดภัย
            ในการเดินทางและขนส่ง และเป็นมิตรกับสิ่งแวดล้อม

        </p>
        <p class="text-ab">
            <strong1>การดำรงชีวิตอัจฉริยะ (Smart Living)</strong1>
            เป็นเมืองที่มุ่งเน้นให้บริการที่อำนวยความสะดวกต่อการดำรงชีวิต เช่น
            การบริการด้านสุขภาพให้ประชาชนมีสุขภาพและสุขภาวะที่ดี โดยเฉพาะ
            อย่างยิ่งเพื่อเตรียมพร้อมเข้าสู่สังคมสูงอายุ การเพิ่มความปลอดภัย
            ของประชาชนด้วยการเฝ้าระวังภัยจากอาชญากรรม ไปจนถึงการส่ง
            เสริมให้เกิดสิ่งอำนวยความสะดวกสำหรับการดำรงชีวิตที่เหมาะสม

        </p>
        <p class="text-ab">
            <strong1>พลเมืองอัจฉริยะ (Smart People)</strong1>
            เป็นเมืองที่มุ่งเน้นพัฒนาพลเมืองให้มีความรู้และสามารถประยุกต์ใช้
            เทคโนโลยีให้เกิดประโยชน์ทั้งในเชิงเศรษฐกิจและการดำรงชีวิต สร้าง
            สภาพแวดล้อมที่ส่งเสริมความคิดสร้างสรรค์และการเรียนรู้นอกระบบ
            รวมถึงการส่งเสริมการอยู่ร่วมกันด้วยความหลากหลายทางสังคม

        </p>
        <p class="text-ab">
            <strong1>พลังงานทางเลือกอันเป็นพลังงานสะอาด (Renewable Energy)</strong1>

            เช่น เชื้อเพลงชีวมวล ไฟฟ้าจากพลังงานหมุนเวียน และไฟฟ้าจาก
            พลังงานอื่นๆ เป็นต้น

        </p>
        <p class="text-ab">
            <strong1>เศรษฐกิจอัจฉริยะ (Smart Economy)</strong1>
            เป็นเมืองที่มุ่งเน้นเพิ่มประสิทธิภาพและความคล่องตัวในการดำเนิน
            ธุรกิจ สร้างให้เกิดความเชื่อมโยงและความร่วมมือทางธุรกิจ และ
            ประยุกต์ใช้นวัตกรรม ในการพัฒนาเพื่อปรับเปลี่ยนธุรกิจ ( เช่น
            เมืองเกษตรอัจฉริยะ เมืองท่องเที่ยวอัจฉริยะ เป็นต้น )

        </p>
        <p class="text-ab">
            <strong1>การบริหารภาครัฐอัจฉริยะ (Smart Governance)</strong1>

            เป็นเมืองที่มุ่งเน้นพัฒนาระบบบริการเพื่อให้ประชาชนเข้าถึงบริการ
            ภาครัฐสะดวก รวดเร็ว เพิ่มช่องทางการมีส่วนร่วมของประชาชน
            รวมถึงการเปิดให้ประชาชนเข้าถึงข้อมูลทำให้เกิดความโปร่งใส ตรวจ
            สอบได้

        </p>




        <br>
        <!--         <div style="text-align: center;">
            <div class="row justify-content-lg-center">
                <div class="col-12 col-sm-12 col-lg-4">
                        <img style="width: 60%;" class="img-fluid" src="/img/news/002022/july/148/148-03.jpg">
                </div>
            </div>
        </div> -->
    </div>
</section>
<!--======================================== //    Start HotPromotion // ======================================================-->


<!-- <a href="#" class="scrollup" style="display: none;">Scroll</a> -->