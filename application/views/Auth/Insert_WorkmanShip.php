<div class="container pad-top-100">

    <form method="post" action="Action_Insert_WorkmanShip" onsubmit="return validate()" class="well form-horizontal" id="contact_form" enctype="multipart/form-data">
        <fieldset>

            <!-- Form Name -->
            <legend>
                <center>
                    <h1><b>เพิ่มข้อมูลใน NT CONNX Work and Experience</b></h1>
                </center>
            </legend><br>

            <!-- Text input-->

            <div class="form-group">
                <label class="col-md-4 control-label">URL รูปภาพ</label>
                <div class="col-md-4 inputGroupContainer">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-picture"></i></span>
                        <input id="Image" name=" Image" placeholder="https://image" class="form-control" type="text" oninput="validate()">
                        <!--<input name="Image" placeholder="" class="custom-file-input" type="file" accept="image/png, image/jpeg">-->
                    </div>
                </div>
                <small id="Imagelog"></small>
            </div>

            <!-- Text input-->

            <div class="form-group">
                <label class="col-md-4 control-label">ข่าวสั้น</label>
                <div class="col-md-4 inputGroupContainer">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-list-alt"></i></span>
                        <textarea id="work_header" rows="5" name="work_header" class="form-control" aria-label="With textarea" oninput="validate()"></textarea>
                    </div>
                </div>
                <small id="work_headerlog"></small>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">เนื้อหาข่าว</label>
                <div class="col-md-4 inputGroupContainer">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-list-alt"></i></span>
                        <textarea id="work_content" rows="5" name="work_content" class="form-control" aria-label="With textarea" oninput="validate()"></textarea>
                    </div>
                </div>
                <small id="work_contentlog"></small>
            </div>

            <!-- Select Basic -->

            <!-- Success message -->
            <div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Success!.</div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label"></label>
                <div class="col-md-4"><br>
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button name="insert" type="submit" class=" btn btn-warning">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspบันทึก <span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                </div>
            </div>

        </fieldset>
    </form>
</div>
</div><!-- /.container -->
<script>
    function validate() {
        var Image = document.getElementById('Image').value;
        var work_header = document.getElementById('work_header').value;
        var work_content = document.getElementById('work_content').value;
        var status = false;

        if (!Image) {
            document.getElementById("Imagelog").innerHTML = "<div class='text-danger'> กรุณากรอก URL รูปภาพ </div>";
            status = false;
        } else {
            if (/^(ftp|http|https):\/\/[^ "]+$/.test(Image)) {
                document.getElementById("Imagelog").innerHTML = "<span class='text-success glyphicon glyphicon-ok'></span>";
                status = true;

            } else {
                document.getElementById("Imagelog").innerHTML = "<div class='text-danger'> กรุณากรอกขURL รูปภาพในรูปแบบ URL </div>";
                status = false;
            }
        }

        if (!work_header) {
            document.getElementById("work_headerlog").innerHTML = "<div class='text-danger'> กรุณากรอกข่าวสั้น </div>";
            status = false;
        } else if (work_header.length > 500) {
            document.getElementById("work_headerlog").innerHTML = "<div class='text-danger'> กรุณากรอกข่าวสั้นแค่ 255 ตัวอักษร </div>";
            status = false;
        } else {
            document.getElementById("work_headerlog").innerHTML = "<span class='text-success glyphicon glyphicon-ok'></span>";
        }

        if (!work_content) {
            document.getElementById("work_contentlog").innerHTML = "<div class='text-danger'> กรุณากรอกเนื้อหาข่าว </div>";
            status = false;
        } else {
            document.getElementById("work_contentlog").innerHTML = "<span class='text-success glyphicon glyphicon-ok'></span>";
        }

        return status;
    }
</script>