<!--======================================== //    Start Silde Show Banner // ======================================================-->
<div id="home" class=" full-screen-mode parallax">
    <div class="container text-center">
        <div class="alignfull-page">

            <!-- <img class="img-responsive" src="/assets/img/T11.png"> -->
        </div>
    </div>

</div>
<!-- end banner -->


<div class="form-style-10 pad-top-100">
    <div class="container ">
        <div class="row">
            <h1>
                <span1>ฟอร์มกรอกข้อมูลเพื่อให้เจ้าหน้าที่ติดต่อกับ</span1><span>รายละเอียด และ
                    เงื่อนไขการให้บริการ</span>
                <hr class="pro-hr" style="width: 20%;">
                <?php require_once __DIR__.'/../Config/Connect.php';
echo "<img src='/assets/img/nt_platform/" . $_CONFIG['Platform']['Platform1'] . "' class='shows' > ";?>
            </h1>
            <form action="line_noti" method="post">


                <div class="inner-wrap">
                    <div class="form-group">

                        <label><span>สินค้าที่ท่านเลือก</span></br>
                            <small class="smform">( Product you choose)</small>
                            <input class="form-control" type="text" name="id" id="id"
                                value="NT Platform : Blooming ConnX Creative" readonly />
                        </label>
                    </div>
                    <div class="form-group">
                        <label>กรุณาเลือกช่องทางที่สะดวกให้ติดต่อกับ <span class="text-danger"> *</span>
                            <!-- ===================================== -->
                            <p><input name="r1" type="radio" value="" required="" onclick="show('check1')" /> อีเมล
                                <input name="r1" type="radio" value="" onclick="show('check2')" /> เบอร์โทรศัพท์
                            </p>
                            <div id="check1" style="display:none">อีเมล (กรุณากรอกข้อมูลให้ครบถ้วน) <span
                                    class="text-danger"> * </span><br /><small class="smform">(Email)</small>
                                <input class="form-control" name="name" id="name" type="email" autofocus />
                            </div>
                            <div id="check2" style="display:none">เบอร์โทรศัพท์ (กรุณากรอกข้อมูลให้ครบถ้วน)<span
                                    class="text-danger"> * </span><br /><small class="smform">(Phone)</small>
                                <input class="form-control" class="form-control" type="phone" name="ph" id="ph"
                                    pattern="[0-9]{3}[0-9]{3}[0-9]{4}" />
                            </div>
                            <!-- ===================================== -->
                    </div>
                    <label>ข้อมูลที่ท่านต้องการเพิ่มเติม(ถ้ามี) </br>
                        <small class="smform">(Expectation and demands) </small></label><textarea class="form-control"
                        class="form-control" name="f1" id="f1" rows="4" cols="30"></textarea>

                    <label><span>ช่วงเวลาที่ท่านสะดวกให้ติดต่อกับ</span></br>
                        <small class="smform">( Preferred time for contact )</small>
                        <select class="form-control" name="f2" id="f2" class="select-field">
                            <option value="9.00-12.00">9.00-12.00</option>
                            <option value="12.00-13.00">12.00-13.00</option>
                            <option value="13.00-17.00">13.00-17.00</option>
                            <option value="17.00-19.00">17.00-19.00</option>
                        </select></label>

                    <div class="g-recaptcha" data-callback="makeaction"
                        data-sitekey="6LcvWQIiAAAAABKylTfwFhwzs6XtA_OFom0ez23C"></div>
                    <div class="form-group pad-bottom-40">
                        <br>
                        <button class="button-87" type="submit" id="submit" name="submit" onClick="insert_value()"
                            title="กรุณาติ๊กถูกเพื่อยืนยันตนของท่าน" disabled>Send</button>
                    </div>

                </div>

            </form>

        </div>
    </div>
</div>