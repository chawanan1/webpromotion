<?php

$header = "แจ้งเตือนขอใช้บริการ";
$id = $_POST['id'];
$name = $_POST['name'];
$ph = $_POST['ph'];
$f1 = $_POST['f1'];
$f2 = $_POST['f2'];
$foot = "เปิดในGoogle Sheet: https://shorturl.asia/YZKba";
$message = $header .
    "\n" . "สินค้าที่เลือก: " . $id .
    "\n" . "เมล: " . $name .
    "\n" . "เบอร์: " . $ph .
    "\n" . "รายละเอียด: " . $f1 .
    "\n" . "ช่วงเวลาที่สะดวก: " . $f2 .
    "\n" . $foot;

if (isset($_POST['submit']) && isset($_POST['g-recaptcha-response'])) {
    $recaptcha_secret = "6LcvWQIiAAAAABljCYlAgiaYR1ZVRDFkd-2kQGgO";
    $recaptcha_response = trim($_POST['g-recaptcha-response']);
    $recaptcha_remote_ip = $_SERVER['REMOTE_ADDR'];

    $recaptcha_api = "https://www.google.com/recaptcha/api/siteverify?" .
        http_build_query(
            array(
                'secret' => $recaptcha_secret,
                'response' => $recaptcha_response,
                'remoteip' => $recaptcha_remote_ip,
            )
        );
    $response = json_decode(file_get_contents($recaptcha_api), true);
}
if (isset($response) && $response['success'] == true) { // ตรวจสอบสำเร็จ
    echo "Successful!"; // ทำคำสั่งเพิ่มข้อมูลหรืออื่นๆ
} else {
    echo "Access denied!";
}
if (isset($_POST["submit"])) {
    if ($id != "" || $name != "" || $ph != "" || $f1 != "" || $f2 != "") {
        sendlinemesg();
        header('Content-Type: text/html; charset=utf8');
        $res = notify_message($message);
        /* echo "<body onload=\"window.alert('ส่งข้อมูลไปหาเจ้าหน้าที่เรียบร้อยแล้ว')"; */
        echo "<script>alert('ส่งข้อมูลไปหาเจ้าหน้าที่เรียบร้อยแล้ว');</script>";
        echo ("<script>window.location = '../Auth/Home_Sell';</script>");
        /*  header("location: ../home"); */
    } else {
        echo "<script>alert('กรุณากรอกข้อมูลให้ครบถ้วน');</script>";
        /* header("location: home.php"); */
    }
}

function sendlinemesg()
{
    // LINE LINE_API https://notify-api.line.me/api/notify
    // LINE TOKEN mhIYaeEr9u3YUfSH1u7h9a9GlIx3Ry6TlHtfVxn1bEu แนะนำให้ใช้ของตัวเองนะครับเพราะของผมยกเลิกแล้วไม่สามารถใช้ได้
    define('LINE_API', "https://notify-api.line.me/api/notify");
    /*define('LINE_TOKEN', "7luKNCxRmoX17rNnHx3K2LZSENLE1oAQiq0TtKmgHHd"); //ในกลุ่มตัวเอง */
    define('LINE_TOKEN', "8vBqt7yzUzpJwXy70G0fOInLCwJ1G3tDgznzi6vxIPY"); //ในกลุ่ม NT*/

    function notify_message($message)
    {
        $queryData = array('message' => $message);
        $queryData = http_build_query($queryData, '', '&');
        $headerOptions = array(
            'http' => array(
                'method' => 'POST',
                'header' => "Content-Type: application/x-www-form-urlencoded\r\n"
                    . "Authorization: Bearer " . LINE_TOKEN . "\r\n"
                    . "Content-Length: " . strlen($queryData) . "\r\n",
                'content' => $queryData,
            ),
        );
        $context = stream_context_create($headerOptions);
        $result = file_get_contents(LINE_API, false, $context);
        $res = json_decode($result);
        return $res;
    }
}
//////////////////////////////////////////////////////