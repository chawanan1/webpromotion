<?php

echo '
<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">';
//เช็คว่ามีตัวแปร session อะไรบ้าง
/* print_r($_SESSION); */
//exit();
//สร้างเงื่อนไขตรวจสอบสิทธิ์การเข้าใช้งานจาก session
if (empty($_SESSION['id']) && empty($_SESSION['nameEN']) && empty($_SESSION['employeeID']) && empty($_SESSION['role'])) {
    echo '<script>
                setTimeout(function() {
                swal({
                title: "คุณไม่มีสิทธิ์ใช้งานหน้านี้",
                type: "error"
                }, function() {
                window.location = "../"; //หน้าที่ต้องการให้กระโดดไป
                });
                }, 1000);
                </script>';
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">

    <!-- Site Metas -->
    <title>NT Conn|X</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="../assets/images/favicon-96x96.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="../assets/images/apple-touch-icon.png">

    <link rel="stylesheet" href="../assets/css/orange.css">
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" href="../assets/css/responsive.css">
    <link rel="stylesheet" href="../assets/css/style_product.css">
    <!-- Bootstrap CSS -->
    <!-- Responsive CSS -->
    <!-- color -->
    <link rel="stylesheet" href="../assets/fonts/font.css">
    <link rel="stylesheet" href="../assets/fonts/fontnt.css">
    <!-- Modernizer -->
    <script src="../assets/js/modernizer.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.23/angular.min.js"></script> -->
    <script src='https://cdn.jsdelivr.net/npm/sweetalert2'></script>
    <!-- <script src="https://www.google.com/recaptcha/api.js" async defer></script> -->
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <!-- <script src='https://www.google.com/recaptcha/api.js?hl=th'></script> -->
    <!--===============================================================================================-->
    <script>
        function show(fname) {
            var div1 = document.getElementById("check1")
            var div2 = document.getElementById("check2")
            if (fname == "check1") {
                div1.style.display = "block"
                div2.style.display = "none"
            } else {
                div2.style.display = "block"
                div1.style.display = "none"
            }
        }
    </script>

    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-7QT9W6PG2Z"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-7QT9W6PG2Z');
    </script>
</head>

<body>