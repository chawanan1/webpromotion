<div id="site-header">
    <header id="header" class="header-block-top">
        <div class="container">
            <div class="row">
                <div class="main-menu">
                    <!-- navbar -->
                    <nav class="navbar navbar-default" id="mainNav">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <div class="logo">
                                <a class="navbar-brand js-scroll-trigger logo-header" href='../../Auth/Home_Sell'>
                                    <img src="../../assets/images/LGConnX2.png" alt=""> <!-- img logo .png -->
                                </a>
                            </div>
                        </div>

                        <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <?php
                                if ($_SESSION['role'] == "admin" || $_SESSION['role'] == "superadmin") {
                                    /* เมนูที่ให้เห็นเฉพาะสิทธิ์ admin */
                                    echo "        
                                <li><a href='../../Auth/Product_Sell'>Product</a></li>
                                <li><a href='../../Auth/Document'>Document</a></li>
                                <li><a href='../../Auth/New_Event'>NewEvent</a></li>

                                <li class='nav-item dropdown'>
                                    <a class='nav-link dropdown-toggle' href='#' id='navbarDropdownMenuLink' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                    <b class='welcomelogin'>Product Manager <i style='color:#545859; -webkit-text-stroke-width: thick; margin-left: 10px;' class='fa fa-sort-down' id='user'></i> </b> 
                                    </a>
                                    <div class='dropdown-menu TEST' aria-labelledby='navbarDropdownMenuLink'>                    
                                        <a class='dropdown-item' href='../../Crud/add_product'>ADD Product</a>
                                        <a class='dropdown-item' href='../../Auth/Insert_NewsEvent'>ADD News&Event</a>
                                        <a class='dropdown-item' href='../../Auth/Insert_DocumentFile'>ADD DocumentFile</a>
                                        <a class='dropdown-item' href='../../Auth/Insert_WorkmanShip'>ADD WorkmanShip</a>
                                    </div>
                                </li>
                                
                                <li class='nav-item dropdown'>
                                <a class='nav-link dropdown-toggle' href='#' id='navbarDropdownMenuLink' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                <i style='color:#545859; letter-spacing: 5px;vertical-align: -2px;' class='fa fa-user-circle-o' id='user'></i> <b class='welcomelogin'>สวัสดีคุณ:  {$_SESSION['nameEN']}</b><i style='color:#545859; -webkit-text-stroke-width: thick; margin-left: 10px;' class='fa fa-sort-down' id='user'></i>
                                </a>
                                <div class='dropdown-menu TEST' aria-labelledby='navbarDropdownMenuLink'>
                                    <a class='dropdown-item' href='../../Auth/signup'>ADD User</a>            
                                    <a class='dropdown-item' href='../../logout'><i style='color:black; letter-spacing: 5px;vertical-align: -2px;' class='fa fa-sign-out' id='user'></i>LOGOUT</a>
                                </div>
                                </li>";
                                } else if ($_SESSION['role'] == "sale") {
                                    /* เมนูที่ให้เห็นเฉพาะสิทธิ์ sale */
                                    echo "        <li><a href='../../Auth/Product_Sell'>Product</a></li>
                                <li><a href='../../Auth/Document'>Document</a></li>
                                <li><a href='../../Auth/New_Event'>NewsEvents</a></li>
                                <li class='nav-item dropdown'>
        <a class='nav-link dropdown-toggle' href='#' id='navbarDropdownMenuLink' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
        <i style='color:#545859; letter-spacing: 5px;vertical-align: -2px;' class='fa fa-user-circle-o' id='user'></i><b class='welcomelogin'>สวัสดีคุณ:  {$_SESSION['nameEN']}</b><i style='color:#545859; -webkit-text-stroke-width: thick; margin-left: 10px;' class='fa fa-sort-down' id='user'></i>
        </a>
        <div class='dropdown-menu' aria-labelledby='navbarDropdownMenuLink'>
          <!-- <a class='dropdown-item' href='#'>Action</a> -->
          <!-- <a class='dropdown-item' href='#'>MY Account</a> -->
          <a class='dropdown-item' href='logout'><i style='color:black; letter-spacing: 5px;vertical-align: -2px;' class='fa fa-sign-out' id='user'></i>LOGOUT</a>
        </div>
      </li>";
                                }
                                ?>

                            </ul>
                        </div>
                        <!-- end nav-collapse -->
                    </nav>
                    <!-- end navbar -->
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end container-fluid -->
    </header>
    <!-- end header -->
</div>
<!--======================================== //       End Navarbar    // ==========================================================-->