<!--======================================== //    Start Silde Show Banner // ======================================================-->
<div id="home" class=" full-screen-mode parallax">
    <div class="container text-center pad-bottom-100">
        <div class="alignfull-page">
        </div>
    </div>
    <!-- end container -->
</div>
<!-- end banner -->

<!--======================================== //    End Silde Show Banner // ===================================================-->

<div class="content pad-bottom-100">

    <div class="container">
        <h1 class="block-product text-center pad-top-40"> เอกสาร / คู่มือ </h1>
        <hr class="pro-hr" style="width: 20%;">
        <!--  <h1 class="block-product1 text-center"> For Digital Solution Provider </h1> -->
        <!-- Table -->
        <div class="fileupload">
            <div class="item-list">

                <?php
                foreach ($resultDocumentFile as $item) {
                    $html = '';
                    for ($i = 0; $i < count($item); $i++) {
                        $html .=
                            '                
            <a href="' . $item[$i]->doc_file . '" class="item" target="_blank">
                <div class=" itemcell">
                    <div class="row">
                        <div class="col-xs-12 col-md-10 col-sm-9">
                            <div class="dwn-icn"><img style="width:100%" src="../assets/img/pdf-icn.png"></div>
                            <div class="txt_content3 desc-txt text">
                            ' . $item[$i]->doc_name . '
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-3 hidden-xs">
                            <span class="txt_content3 text">ดาวน์โหลด</span><img style="width:15%" src="../assets/img/arrow-icn.png" class="anm-arrow">
                        </div>
                    </div>
                </div>
            </a>
                ';
                    }
                }
                echo $html;
                ?>

                <!-- <a href="/assets/pdf/NT ConnX Product Catalog.pdf" class="item" target="_blank"> -->

            </div>
        </div>
        <!-- END Table -->
    </div>

</div>

<!--======================================== //    Start Extension Modal2 // ======================================================-->

<a href="#" class="scrollup" style="display: none;">Scroll</a>

<section id="testimonial">
    <div class="sec-prefooter">
        <div class="container">
            <div class="box-prefooter t-bold">
                <div class="prefoot-contact-title">สอบถามข้อมูลเพิ่มเติมได้ที่</div>
                <div class="prefoot-contact-group">
                    <div class="prefoot-contact-group-call">
                        <div class="prefoot-contact-group-call-text">ศูนย์บริการ<br>ลูกค้าธุรกิจ</div>
                        <a href="#" class="prefoot-contact-group-call-num txt-gdcolor">NT ConnX</a>
                    </div>
                    <div class="prefoot-contact-group-or t-medium"><span>หรือ</span></div>
                    <div class="prefoot-contact-group-call"><a href="javascript:void(0)" onclick="window.location.href='form'" class="btn3 btn3-secondary"><span>สอบถามข้อมูลเพิ่มเติม</span></a></div>
                </div>
            </div>
        </div>
    </div>
</section>