<div class="container pad-top-100">

    <form method="post" name="myForm" action="Action_Insert_NewsEvent" onsubmit="return validatenews()" class="well form-horizontal" id="contact_form">
        <fieldset>

            <!-- Form Name -->
            <legend>
                <center>
                    <h1><b>เพิ่มข่าวประกาศ</b></h1>
                </center>
            </legend><br>

            <!-- Text input-->

            <div class="form-group">
                <label class="col-md-4 control-label">หัวข้อข่าว</label>
                <div class="col-md-4 inputGroupContainer">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-list-alt"></i></span>
                        <input id="news_header" name="news_header" placeholder="" class="form-control" type="text" oninput="validate()">
                    </div>
                </div>
                <small id="headerlog"></small>
            </div>

            <!-- Text input-->

            <div class="form-group">
                <label class="col-md-4 control-label">เนื้อหาข่าว</label>
                <div class="col-md-4 inputGroupContainer">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-list-alt"></i></span>
                        <textarea id="content" rows="5" name="news_content" class="form-control" aria-label="With textarea" oninput="validate()"></textarea>
                    </div>
                </div>
                <small id="contentlog"></small>
            </div>
            <!-- Select Basic -->

            <!-- Success message -->
            <div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Success!.</div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label"></label>
                <div class="col-md-4"><br>
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button name="insert" type="submit" class=" btn btn-warning">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspบันทึก <span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                </div>
            </div>

        </fieldset>
    </form>
</div>
</div><!-- /.container -->

<script>
    function validatenews() {
        var header = document.getElementById('news_header').value;
        var content = document.getElementById('content').value;
        var status = false;

        if (!header) {
            document.getElementById("headerlog").innerHTML = "<div class='text-danger'> กรุณากรอกห้วข้อข่าว </div>";
            status = false;
        } else {
            document.getElementById("headerlog").innerHTML = "<span class='text-success glyphicon glyphicon-ok'></span>";
            status = true;
        }

        if (!content) {
            document.getElementById("contentlog").innerHTML = "<div class='text-danger'> กรุณากรอกเนื้อหาข่าว </div>";
            status = false;
        } else {
            document.getElementById("contentlog").innerHTML = "<span class='text-success glyphicon glyphicon-ok'></span>";
        }
        return status;
    }
</script>