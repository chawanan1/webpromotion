<div class="container" style="padding-top: 10rem; padding-bottom: 5rem;">
    <h2></h2><br>
    <strong>
        <h1 style="color:Blue" class="text-center1">Cookie Policy (นโยบายคุกกี้) <br> บริษัท โทรคมนาคมแห่งชาติ จำกัด (มหาชน)
        </h1>
    </strong>
    <br>
    <h2></h2>
    <!-- <div style="margin-left:18pt; margin-bottom: 10px; text-indent:-18pt;"><a name="_Toc104376741"><strong>1.&nbsp;</strong><strong1>หลักการและเหตุผล</strong1></a><strong></strong></div>
 -->
    <div style="margin-bottom:0.0001pt;text-indent:30pt;line-height:normal; color:black;color:black;">เมื่อท่านได้เข้าสู่เว็บไซต์ ntplc.co.th ข้อมูลที่เกี่ยวข้องกับการเข้าสู่เว็บไซต์ของท่านจะถูกเก็บเอาไว้ในรูปแบบของ Cookies โดยนโยบาย Cookies นี้จะอธิบายถึงความหมาย การทำงาน วัตถุประสงค์ รวมถึงการลบและการปฏิเสธการเก็บ Cookies เพื่อความเป็นส่วนตัวของท่าน โดยการเข้าสู่เว็บไซต์นี้ถือว่าท่านได้อนุญาตให้บริษัทใช้ Cookies ตามนโยบาย Cookies ที่มีรายละเอียดดังต่อไปนี้</div>
    <div style="margin-bottom:0.0001pt;text-indent:30pt;line-height:normal; color:black;color:black;">
        บริษัทให้ความสำคัญกับการปฏิบัติตามกฎหมายจึงตระหนักในความสำคัญของการคุ้มครองข้อมูลส่วนบุคคล (Data Privacy)
        ซึ่งเป็นสิทธิขั้นพื้นฐานสำคัญในความเป็นส่วนตัว
        ที่ต้องได้รับความคุ้มครองและปฏิบัติตามกฎหมายและกฎเกณฑ์ที่กำหนดโดยต้องจัดระบบเพื่อควบคุมดูแลอย่างเข้มงวดและรัดกุม
        เพื่อให้ข้อมูลส่วนบุคคลของเจ้าของข้อมูลปลอดภัย มีเสถียรภาพ และการประมวลผลข้อมูลเป็นไปอย่างโปร่งใส</div>
    <div style="margin-bottom:0.0001pt;text-indent:30pt;line-height:normal; color:black;color:black;">&nbsp;</div>

    <div style="margin-left:18pt; margin-bottom: 10px; text-indent:-18pt;">
        <a name="_Toc104376742"><strong>2.&nbsp;</strong><strong>Cookies คืออะไร ?</strong></a><strong></strong>
    </div>

    <div style="margin-bottom:0.0001pt;text-indent:30pt;line-height:normal; color:black;">Cookies คือ Text Files ที่อยู่ในคอมพิวเตอร์ของเจ้าของข้อมูลส่วนบุคคลที่ใช้เพื่อจัดเก็บรายละเอียดข้อมูล Log การใช้งาน Internet ของเจ้าของข้อมูลส่วนบุคคล โดยเจ้าของข้อมูลส่วนบุคคลสามารถศึกษารายละเอียดเพิ่มเติมของ Cookies ได้จาก www.allaboutcookies.org</div>
    <!-- <div style="margin-bottom:0.0001pt;text-indent:30pt;line-height:normal; color:black;">โดยให้นโยบายฉบับนี้
        มีผลใช้บังคับกับทุกกิจกรรมการดำเนินงานของบริษัทที่เกี่ยวข้องกับข้อมูลส่วนบุคคล เป็นต้นว่า
        ประเภทและรูปแบบของข้อมูลที่จัดเก็บ ช่องทางการจัดเก็บข้อมูล วัตถุประสงค์ของบริษัทในการเก็บรวบรวมข้อมูล
        การนำข้อมูลส่วนบุคคลไปใช้ การเปิดเผยข้อมูลดังกล่าวให้กับบุคคลอื่น
        ตลอดจนวิธีการที่บริษัทดำเนินการปกป้องข้อมูลส่วนบุคคลของเจ้าของข้อมูล
    </div> -->

    <div style="margin-bottom:0.0001pt;text-indent:30pt;line-height:normal; color:black;">&nbsp;</div>
    <div style="margin-left:18pt; margin-bottom: 10px; text-indent:-18pt;"><a name="_Toc104376743"><strong>3.&nbsp;</strong><strong>บริษัทใช้ Cookies อย่างไร ?</strong></a><strong></strong></div>

    <div style="text-indent:30pt; color:black;">
        บริษัทจะจัดเก็บข้อมูลการเข้าเยี่ยมชมเว็บไซต์ จากผู้เข้าเยี่ยมชมทุกรายผ่าน Cookies หรือ เทคโนโลยี ที่ใกล้เคียง และบริษัทจะใช้ Cookies เพื่อประโยชน์ ในการพัฒนาประสิทธิภาพในการเข้าถึงบริการของบริษัทผ่าน Internet รวมถึงพัฒนาประสิทธิภาพในการใช้งานบริการของบริษัททาง Internet ให้สามารถสร้างประสบการณ์ และความพึงพอใจให้ท่าน โดยจะใช้เพื่อกรณี ดังต่อไปนี้
    </div>
    <div style="text-indent:30pt; color:black;">
        1.เพื่อให้เจ้าของข้อมูลส่วนบุคคลสามารถ Sign in บัญชีของเจ้าของข้อมูลส่วนบุคคลในเว็บไซต์ ของบริษัทได้อย่างต่อเนื่อง<br></div>
    <div style="text-indent:30pt; color:black;">
        2.เพื่อศึกษาพฤติกรรมการใช้งานเว็บไซต์ ของเจ้าของข้อมูลส่วนบุคคล เพื่อนำไปพัฒนาให้บริการดิจิทัลของบริษัทสามารถใช้งานได้ง่าย รวดเร็ว และมีประสิทธิภาพยิ่งขึ้น
    </div>
    <!--  <div style="text-indent:30pt; color:black;">
        ทั้งนี้ ประกาศฉบับนี้อาจได้รับการปรับปรุงแก้ไขเมื่อมีการแก้ไขเพิ่มเติมกฎหมายที่เกี่ยวข้อง
        หรือมีการเปลี่ยนแปลงนโยบายการคุ้มครองข้อมูลส่วนบุคคลของบริษัท
        บริษัทขอแนะนำให้ท่านอ่านประกาศนี้และตรวจสอบข้อมูลล่าสุดให้เป็นประจำ</div>
 -->
    <div style="margin-bottom:0.0001pt;text-indent:30pt;line-height:normal; color:black;">&nbsp;</div>
    <div style="margin-left:18pt; margin-bottom: 10px; text-indent:-18pt;"><a name="_Toc104376744"><strong>4.&nbsp;</strong><strong>ประเภทของ Cookies ที่บริษัทใช้ ?</strong></a><strong></strong></div>
    <div style="margin-bottom:0.0001pt;text-indent:30pt;line-height:normal; color:black;">"บริษัทใช้ Cookies ดังต่อไปนี้ สำหรับเว็บไซต์ ของบริษัท</div>
    <div style="margin-bottom:0.0001pt;text-indent:30pt;line-height:normal; color:black;">
        1. Required Cookies – ที่ต้องใช้เพื่อเปิดใช้งานฟังก์ชั่นหลักของเว็บไซต์ Cookies ประเภทนี้ช่วยให้ประสบการณ์การใช้เว็บไซต์ของท่านเป็นไปอย่างต่อเนื่อง เช่น การจดจำการเข้าสู่ระบบ การจดจำข้อมูล ที่ท่านให้ไว้บนเว็บไซต์
        Functionality
    </div>
    <div style="margin-bottom:0.0001pt;text-indent:30pt;line-height:normal; color:black;">
        2. Cookies – Cookies เพื่อการทำงานของเว็บไซต์ (Functionality Cookies) Cookies ประเภทนี้ใช้ในการจดจำตัวตนท่านเมื่อท่านกลับมาใช้งานเว็บไซต์ อีกครั้ง ซึ่งจะช่วยให้บริษัทสามารถปรับแต่งเนื้อหาสำหรับท่าน ปรับให้เว็บไซต์ของบริษัทตอบสนองความต้องการใช้งานของท่าน รวมถึงจดจำการตั้งค่าของท่าน อาทิ ภาษา หรือภูมิภาค หรือขนาดของตัวอักษรที่ท่านเลือกใช้ในการใช้งาน ในเว็บไซต์
    </div>
    <div style="margin-bottom:0.0001pt;text-indent:30pt;line-height:normal; color:black;">
        3. Advertising – Cookies ที่ใช้ในการจดจำสิ่งที่ลูกค้าเคยเยี่ยมชม เพื่อนำเสนอสินค้า บริการ หรือสื่อโฆษณาที่เกี่ยวข้องเพื่อให้ตรงกับความสนใจของผู้ใช้งาน Cookies ประเภทนี้จะถูกบันทึก บนอุปกรณ์ของท่านเพื่อเก็บข้อมูลการเข้าใช้งานและลิงก์ที่ท่านได้เยี่ยมชมและติดตาม นอกจากนี้ Cookies จากบุคคลที่สาม อาจใช้ข้อมูลที่มีการส่งต่อข่าวสารในสื่อออนไลน์และเนื้อหาที่จัดเก็บจาก การให้บริการ เพื่อเข้าใจความต้องการของผู้ใช้งานโดยมีวัตถุประสงค์ในการปรับแต่งเว็บไซต์ แคมเปญโฆษณาให้เหมาะสมกับความสนใจของท่าน
    </div>
    <div style="margin-bottom:0.0001pt;text-indent:30pt;line-height:normal; color:black;">
        4. Cookies ประเภทการวิเคราะห์ และวัดผลการทำงาน Cookies ประเภทนี้ช่วยให้บริษัทสามารถวัดผลการทำงาน เช่น การประมวลจำนวนหน้าที่ท่านเข้าใช้งานจำนวนลักษณะเฉพาะของกลุ่มผู้ใช้งานนั้น ๆ โดยข้อมูลดังกล่าวจะนำมาใช้ในการวิเคราะห์รูปแบบพฤติกรรมของผู้ใช้งาน
    </div>
    <div style="margin-bottom:0.0001pt;line-height:normal; color:black;">
        ทั้งนี้ นโยบายส่วนบุคคลของบริษัท จะกำหนดรายละเอียดทั้งหมดเกี่ยวกับข้อมูลอื่น ๆ ที่บริษัทจัดเก็บ และวิธีการที่บริษัทนำข้อมูลส่วนบุคคลของท่านไปใช้
    </div>

    <div style="margin-bottom:0.0001pt;text-indent:30pt;line-height:normal; color:black;">&nbsp;</div>
    <div style="margin-left:18pt; margin-bottom: 10px; text-indent:-18pt;"><a name="_Toc104376745"><strong>5.
            </strong><strong>การจัดการ Cookies</strong></a><strong></strong></div>
    <div style="margin-bottom:0.0001pt;text-indent:30pt;line-height:normal; color:black;">เจ้าของข้อมูลส่วนบุคคลสามารถตั้งค่ามิให้เบราว์เซอร์ของเจ้าของข้อมูลส่วนบุคคล ตกลงรับ Cookies ของบริษัทได้ โดยมีขั้นตอนตามที่ผู้ผลิต Web Browser เช่น Internet Explorer, Microsoft Edge, Firefox, Safari, หรือ Web Browser อื่นที่กำหนด</div>

    <br>
    <div style="margin-bottom:0.0001pt;text-indent:45pt;line-height:normal; color:black;">
        ท่านสามารถลบและปฏิเสธการเก็บ Cookies ได้โดยศึกษาตามวิธีการที่ระบุในแต่ละ Web Browser ที่ท่านใช้งานอยู่ตามลิงก์ดังนี้
    </div>
    <ui>
        <li><a href="#" ::MARKER>Chrome</a></li>
        <li><a href="#">Firefox</a></li>
        <li><a href="#">Internet Explorer</a></li>
        <li><a href="#">Microsoft Edge</a></li>
        <li><a href="#">Safari</a></li>
        <li><a href="#">Safari for ios</a></li>
        <li><a href="#">Chrome for android</a></li>
        <li><a href="#">Chrome for ios</a></li>
    </ui>
    <br>
    <div style="margin-bottom:0.0001pt;text-indent:45pt;line-height:normal; color:black;">
        ทั้งนี้ พึงตระหนักว่า หากท่านตั้งค่าเบราว์เซอร์ของท่านด้วยการลบและการปฏิเสธการเก็บ Cookies ทั้งหมด (รวมถึง Cookies ที่จำเป็นต่อการใช้งาน) ท่านอาจจะไม่สามารถเข้าสู่เว็บไซต์ทั้งหมด หรือบางส่วนของเราได้
    </div>



</div>
<!--======================================== //    Start HotPromotion // ======================================================-->


<a href="#" class="scrollup" style="display: none;">Scroll</a>