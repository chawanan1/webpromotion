
<div class="container pad-top-100">
<?php
require_once __DIR__."/../../Config/config.php";
/* require_once __DIR__."/../Config/firebaseRDB.php"; */
/* include("firebaseRDB.php"); */

$db = new firebaseRDB($databaseURL);
$id = $_GET['id'];
$retrieve = $db->retrieve("items/$id");
$data = json_decode($retrieve, 1);

?>
    <form method="post" action="../Crud/action_edit" class="well form-horizontal"  id="contact_form">
<fieldset>

<!-- Form Name -->
<legend><center><h1><b>EDIT PRODUCT</b></h1></center></legend><br>

<!-- Text input-->

<div class="form-group">
  <label class="col-md-4 control-label">รูปภาพ</label>  
  <div class="col-md-4 inputGroupContainer">
  <div class="input-group">
  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
  <input  name="Image" placeholder="" value="<?php echo $data['Image']?>" class="form-control"  type="text">
    </div>
  </div>
</div>

<!-- Text input-->

<div class="form-group">
  <label class="col-md-4 control-label" >แพ็กเกจ</label> 
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
  <input name="Paket" value="<?php echo $data['Paket']?>" class="form-control"  type="text">
    </div>
  </div>
</div>
<div class="form-group"> 
  <label class="col-md-4 control-label">ชื่อเต็ม</label>
    <div class="col-md-4 selectContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
        <input name="Title" value="<?php echo $data['Title']?>" class="form-control"  type="text">
  </div>
</div>
</div>
<div class="form-group"> 
  <label class="col-md-4 control-label">บริการ/หมวดหมู่</label>
    <div class="col-md-4 selectContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
        <input name="Service" value="<?php echo $data['Service']?>" class="form-control"  type="text">
  </div>
</div>
</div>
<div class="form-group">
  <label class="col-md-4 control-label" >รายละเอียดสินค้า - บรรทัด 1</label> 
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
  <input name="Detail1" value="<?php echo $data['Detail1']?>" class="form-control"  type="text">
    </div>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" >รายละเอียดสินค้า - บรรทัด 2</label> 
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
  <input name="Detail2" value="<?php echo $data['Detail2']?>" class="form-control"  type="text">
    </div>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" >รายละเอียดสินค้า - บรรทัด 3</label> 
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
  <input name="Detail3" value="<?php echo $data['Detail3']?>" class="form-control"  type="text">
    </div>
  </div>
</div>
<!--  -->

<div class="form-group">
  <label class="col-md-4 control-label" >รายละเอียดสินค้า - บรรทัด 4</label> 
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
  <span class="input-group-addon"><i class="glyphicon glyphicon-briefcase"></i></span>
  <input name="Detail4" value="<?php echo $data['Detail4']?>" class="form-control"  type="text">
    </div>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" >เหมาะสำหรับ</label> 
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
  <span class="input-group-addon"><i class="glyphicon glyphicon-briefcase"></i></span>
  <input name="Feature" value="<?php echo $data['Feature']?>" class="form-control"  type="text">
    </div>
  </div>
</div>

<!-- Success message -->
<div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Success!.</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label"></label>
  <div class="col-md-4"><br>
  <input type="hidden" name="id" value="<?php echo $id?>">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button name="insert" type="submit" class="btn btn-warning" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspSUBMIT <span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
  </div>
</div>

</fieldset>
</form>
</div>
    </div><!-- /.container -->