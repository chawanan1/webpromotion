<div class="container pad-top-100">

  <form method="post" name="myForm" action="../Crud/insert_product" onsubmit="return validate()" class=" well form-horizontal" id="contact_form" enctype="multipart/form-data">
    <fieldset>

      <!-- Form Name -->
      <legend>
        <center>
          <h1><b>เพิ่มสินค้า</b></h1>
        </center>
      </legend><br>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label">ชื่อแพ็กเกจ</label>
        <div class="col-md-4 inputGroupContainer">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
            <input id="Package" name="Package" placeholder="(ตัวอย่างPromotion : Blooming ConnX Creative)" class="form-control" type="text" oninput="validate()">
          </div>
        </div>
        <small id="Packagelog"></small>
      </div>

      <div class="form-group">
        <label class="col-md-4 control-label">บริการ/หมวดหมู่</label>
        <div class="col-md-4 selectContainer">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
            <select id="Service" name="Service" class="form-control selectpicker" oninput="validate()">
              <option value="">Select your Service</option>
              <option value="NT Connectivity">NT Connectivity</option>
              <option value="NT Platform">NT Platform</option>
              <option value="Digital Solutions by NT ConnX">Digital Solutions by NT ConnX</option>
            </select>
          </div>
        </div>
        <small id="Servicelog"></small>
      </div>

      <div class="form-group">
        <label class="col-md-4 control-label">URL รูปภาพ</label>
        <div class="col-md-4 inputGroupContainer">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-picture"></i></span>
            <input id="Image" name="Image" placeholder="https://image" class=" form-control" type="text" oninput="validate()">
            <!--<input name="Image" placeholder="" class="custom-file-input" type="file" accept="image/png, image/jpeg">-->
          </div>
        </div>
        <small id="Imagelog"></small>
      </div>

      <div class="form-group">
        <label class="col-md-4 control-label">รายละเอียดของแพ็กเกจ <small>(บรรทัดที่ 1)</small> </label>
        <div class="col-md-4 inputGroupContainer">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-list-alt"></i></span>
            <input id="Detail1" name="Detail1" placeholder="(ตัวอย่าง SIM ConnX Eval : จำนวน 16 SIM)" class="form-control" type="text" oninput="validate()">
          </div>
        </div>
        <small id="Detail1log"></small>
      </div>

      <div class="form-group">
        <label class="col-md-4 control-label">รายละเอียดของแพ็กเกจ <small>(บรรทัดที่ 2)</small></label>
        <div class="col-md-4 inputGroupContainer">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-list-alt"></i></span>
            <input name="Detail2" placeholder="(ตัวอย่าง ใช้งาน Thingsboard Platform ด้วย Data 5 GB/เดือน)" class="form-control" type="text" oninput="validate()">
          </div>
        </div>
      </div>

      <div class="form-group">
        <label class="col-md-4 control-label">รายละเอียดของแพ็กเกจ <small>(บรรทัดที่ 3)</small></label>
        <div class="col-md-4 inputGroupContainer">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-list-alt"></i></span>
            <input name="Detail3" placeholder="(ตัวอย่าง ใช้งาน SMS API จำนวน 500 Message/เดือน)" class="form-control" type="text" oninput="validate()">
          </div>
        </div>
      </div>

      <div class="form-group">
        <label class="col-md-4 control-label">รายละเอียดของแพ็กเกจ <small>(บรรทัดที่ 4)</small></label>
        <div class="col-md-4 inputGroupContainer">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-list-alt"></i></span>
            <input name="Detail4" placeholder="(ตัวอย่าง ใช้งาน SMS API จำนวน 500 Message/เดือน)" class="form-control" type="text" oninput="validate()">
          </div>
        </div>
      </div>

      <div class="form-group">
        <label class="col-md-4 control-label">อธิบายรายละเอียดแพ็กเกจสั้นๆ</label>
        <div class="col-md-4 inputGroupContainer">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-briefcase"></i></span>
            <input id="Feature" name=" Feature" placeholder="(ตัวอย่าง  สำหรับผู้ประกอบธุระกิจ Digital และ IoT)" class="form-control" type="text" oninput="validate()">
          </div>
        </div>
        <small id="Featurelog"></small>
      </div>

      <!-- Success message -->
      <div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Success!.</div>

      <!-- Button -->
      <div class="form-group">
        <label class="col-md-4 control-label"></label>
        <div class="col-md-4"><br>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button name="submit" type="submit" class=" btn btn-warning">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspบันทึก <span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
        </div>
      </div>
    </fieldset>
  </form>
</div>
</div><!-- /.container -->


<script>
  function validate() {
    var Image = document.getElementById('Image').value;
    var Package = document.getElementById('Package').value;
    var Service = document.getElementById('Service').value;
    var Detail1 = document.getElementById('Detail1').value;
    var Feature = document.getElementById('Feature').value;
    var status = false;


    if (!Image) {
      document.getElementById("Imagelog").innerHTML = "<div class='text-danger'> กรุณากรอก URL รูปภาพ </div>";
      status = false;
    } else {
      if (/^(ftp|http|https):\/\/[^ "]+$/.test(Image)) {
        document.getElementById("Imagelog").innerHTML = "<span class='text-success glyphicon glyphicon-ok'></span>";
        status = true;
      } else {
        document.getElementById("Imagelog").innerHTML = "<div class='text-danger'> กรุณากรอก URL รูปภาพในรูปแบบ URL </div>";
        status = false;
      }
    }

    if (!Package) {
      document.getElementById("Packagelog").innerHTML = "<div class='text-danger'> กรุณากรอกชื่อแพ็กเกจ </div>";
      status = false;
    } else {
      document.getElementById("Packagelog").innerHTML = "<span class='text-success glyphicon glyphicon-ok'></span>";
    }

    if (!Service) {
      document.getElementById("Servicelog").innerHTML = "<div class='text-danger'> กรุณาเลือกบริการ/หมวดหมู่ </div>";
      status = false;
    } else {
      document.getElementById("Servicelog").innerHTML = "<span class='text-success glyphicon glyphicon-ok'></span>";
    }

    if (!Detail1) {
      document.getElementById("Detail1log").innerHTML = "<div class='text-danger'> กรุณากรอกรายละเอียดของแพ็กเกจ </div>";
      status = false;
    } else {
      document.getElementById("Detail1log").innerHTML = "<span class='text-success glyphicon glyphicon-ok'></span>";
    }

    if (!Feature) {
      document.getElementById("Featurelog").innerHTML = "<div class='text-danger'> กรุณากรอกอธิบายรายละเอียดแพ็กเกจสั้นๆ </div>";
      status = false;
    } else {
      document.getElementById("Featurelog").innerHTML = "<span class='text-success glyphicon glyphicon-ok'></span>";
    }

    return status;

  }
</script>