<?php
 require_once __DIR__."/../../Config/config.php";
 require_once __DIR__."/../../Config/firebaseRDB.php";

$db = new firebaseRDB($databaseURL);
$id = $_POST['id'];

$update = $db->update("items", $id, [
    "Image"     => $_POST['Image'],
    "Paket" => $_POST['Paket'],
    "Title" => $_POST['Title'],
    "Service"    => $_POST['Service'],
    "Detail1"      => $_POST['Detail1'],
    "Detail2"    => $_POST['Detail2'],
    "Detail3"    => $_POST['Detail3'],
    "Detail4"    => $_POST['Detail4'],
    "Feature"    => $_POST['Feature']
 ]);

if($update){
	echo "<script type='text/javascript'>";
	echo "alert('💡Update Succesfuly💡');";
	/* echo "window.location = 'add_product'; "; */
	echo "</script>";
	}
	else{
	echo "<script type='text/javascript'>";
	echo "alert('⚠️Error⚠️ back to Update again');";
	echo "</script>";
}
