<?php
 require_once __DIR__."/../../Config/config.php";
 require_once __DIR__."/../../Config/firebaseRDB.php";

$db = new firebaseRDB($databaseURL);

$insert = $db->insert("items", [
   "Image"     => $_POST['Image'],
   "Paket" => $_POST['Paket'],
   "Title"    => $_POST['Title'],
   "Service" => $_POST['Service'],
   "Detail1"      => $_POST['Detail1'],
   "Detail2"    => $_POST['Detail2'],
   "Detail3"    => $_POST['Detail3'],
   "Detail4"    => $_POST['Detail4'],
   "Feature"    => $_POST['Feature']
]);

if($insert){
	echo "<script type='text/javascript'>";
	echo "alert('💡Upload Succesfuly💡');";
	echo "window.location = 'add_product'; ";
	echo "</script>";
	}
	else{
	echo "<script type='text/javascript'>";
	echo "alert('⚠️Error⚠️ back to upload again');";
	echo "</script>";
}
