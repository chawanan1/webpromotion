
<div class="container pad-top-100">

<table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>รูปภาพ</th>
                <th>แพ็กเกจ</th>
                <th>ชื่อเต็ม</th>
                <th>บริการ/หมวดหมู่</th>
                <th>รายละเอียดสินค้า#1</th>
                <th>รายละเอียดสินค้า#2</th>
                <th>รายละเอียดสินค้า#3</th>
                <th>รายละเอียดสินค้า#4</th>
                <th>ประเภท</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        <?php
        require_once __DIR__."/../../Config/config.php";
   $db = new firebaseRDB($databaseURL);
   $data = $db->retrieve("items");
   $data = json_decode($data, 1);
   
   if(is_array($data)){
      foreach($data as $id => $items){
       echo"
            <tr>
                <td width='10%'> <img width='100%' src='{$items['Image']}'> </td>
                <td> {$items['Paket']} </td>
                <td width='15%'> {$items['Title']} </td>
                <td> {$items['Service']} </td>
                <td> {$items['Detail1']} </td>
                <td> {$items['Detail2']} </td>
                <td> {$items['Detail3']} </td>
                <td> {$items['Detail4']} </td>
                <td width='15%'> {$items['Feature']} </td>
                <td> 
                <div class='btn-group btn-group-sm' role='group'>
                <a href='edit_product?id=$id'><button type='button' class='btn btn-outline-dark'><i class='fa fa-pencil-square-o'></i></button></a>
                <a href='delete_product?id=$id'><button type='button' class='btn btn-outline-dark'><i class='fa fa-trash'></i></button></a>
              </div>
            </td>
            </tr>
            ";
        }
     }
     ?>
        </tbody>
        <tfoot>
            <tr>
                <th>รูปภาพ</th>
                <th>แพ็กเกจ</th>
                <th>ชื่อเต็ม</th>
                <th>บริการ/หมวดหมู่</th>
                <th>รายละเอียดสินค้า#1</th>
                <th>รายละเอียดสินค้า#2</th>
                <th>รายละเอียดสินค้า#3</th>
                <th>รายละเอียดสินค้า#4</th>
                <th>ประเภท</th>
                <th>Action</th>
            </tr>
        </tfoot>
    </table>
</div>
    </div><!-- /.container -->