<?php
require_once __DIR__."/Config/config.php";
require_once __DIR__."/Config/firebaseRDB.php";

$employeeID = $_POST['employeeID'];
$full_name = $_POST['full_name'];
$prefixEN = $_POST['prefixEN'];
$nameEN = $_POST['nameEN'];
$lastnameEN = $_POST['lastnameEN'];
$rankFull = $_POST['rankFull'];
$rank = $_POST['rank'];
$rankAd = $_POST['rankAd'];
$cctr = $_POST['cctr'];
$no_Segment = $_POST['no_Segment'];
$segment = $_POST['segment'];
$full_Segment = $_POST['full_Segment'];
$email = $_POST['email'];
$phone = $_POST['phone'];
$fax = $_POST['fax'];
$county = $_POST['county'];
$workplace_Code = $_POST['workplace_Code'];
$ogt_Key= $_POST['ogt_Key'];
$role= $_POST['role'];
/* $password = $_POST['password']; */



if ($employeeID == "") {
    echo "employeeID is required";
}else if ($full_name == ""){
    echo "name is Required !";
} else if ($prefixEN == "") {
    echo "prefixEN is required";
} else if ($nameEN == "") {
    echo "nameEN is required";
}else if ($lastnameEN == "") {
    echo "LastnameEN is required";
}else if ($rankFull == "") {
    echo "rankFull is required";
}else if ($rank == "") {
    echo "rank is required";
}else if ($rankAd == "") {
    echo "rankAd is required";
}else if ($cctr == "") {
    echo "cctr is required";
}else if ($no_Segment == "") {
    echo "no_Segment is required";
}else if ($segment == "") {
    echo "Segment is required";
}else if ($full_Segment == "") {
    echo "Full_Segment is required";
}else if ($email == "") {
    echo "email is required";
}else if ($phone == "") {
    echo "phone is required";
}else if ($fax == "") {
    echo "Fax is required";
}else if ($county == "") {
    echo "county is required";
}else if ($workplace_Code == "") {
    echo "workplace_Code is required";
}else if ($ogt_Key == "") {
    echo "ogt_Key is required";
}else if ($role == "") {
    echo "role is required";
}

/* if ($email == "") {
    echo "Email is required";
}else if ($lastname == ""){
    echo "Lastname is Required !";
} else if ($name == "") {
    echo "Email is required";
} else if ($password == "") {
    echo "Password is required";
} */ 
else {
    $rdb = new firebaseRDB($databaseURL);
    $retrieve = $rdb->retrieve("/user", "email", "EQUAL", $email);
    $data = json_decode($retrieve, 1);

    if (isset($data['email'])) {
        echo "Email already used";
    } else {
        $insert = $rdb->insert("/user", [

            "employeeID" => $employeeID,
            "full_name" => $full_name,
            "prefixEN" => $prefixEN,
            "nameEN" => $nameEN,
            "lastnameEN" => $lastnameEN,
            "rankFull" => $rankFull,
            "rank" => $rank,
            "rankAd" => $rankAd,
            "cctr" => $cctr,
            "no_Segment" => $no_Segment,
            "segment" => $segment,
            "full_Segment" => $full_Segment,
            "email" => $email,
            "phone" => $phone,
            "fax" => $fax,
            "county" => $county,
            "workplace_Code" => $workplace_Code,
            "ogt_Key" => $ogt_Key,
            "role" => $role
/* 
            "name" => $name,
            "lastname" => $lastname,
            "email" => $email,
            "password" => $password */


        ]);

        $result = json_decode($insert, 1);
        if (isset($result['name'])) {
            echo "Signup Success, Please login";
            header("location:signup");
        } else {
            echo "Signup Failed";
        }
    }
}