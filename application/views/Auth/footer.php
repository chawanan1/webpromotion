<!-- <section id="testimonial">
    <div class="sec-prefooter">
        <div class="container">
            <div class="box-prefooter t-bold">
                <div class="prefoot-contact-title">สอบถามข้อมูลเพิ่มเติมได้ที่</div>
                <div class="prefoot-contact-group">
                    <div class="prefoot-contact-group-call">
                        <div class="prefoot-contact-group-call-text">ศูนย์บริการ<br>ลูกค้าธุรกิจ</div>
                        <a href="tel:1239" class="prefoot-contact-group-call-num txt-gdcolor">NT ConnX</a>
                    </div>
                    <div class="prefoot-contact-group-or t-medium"><span>หรือ</span></div>
                    <div class="prefoot-contact-group-call"><a href="javascript:void(0)" onclick="window.location.href='form'"
                            class="btn3 btn3-secondary"><span>สอบถามข้อมูลเพิ่มเติม</span></a></div>
                </div>
            </div>
        </div>
    </div>
</section> -->
<div id="about" class="footer-main pad-top-50">
    <!-- end footer-news -->
    <div class="footer-box">
        <div class="container">
            <div class="row">
                <div class="row justify-content-md-center text-left about-fcolor animate__animated animate__bounce">
                    <div class="col-6 col-md-2">
                        <ul>
                            <li class="nav-footer-col"> NAVIGATION</li>
                            <li><a class="next-link-to" href="../Auth/Home_Sell">Home</a></li>
                            <li><a class="next-link-to" href="../Auth/Product_Sell">Product</a></li>
                            <li><a class="next-link-to" href="../Auth/Document">Document</a></li>
                            <li><a class="next-link-to" href="../Auth/New_Event">New / Event </a></li>

                        </ul>
                    </div>
                    <div class="col-6 col-md-2">
                        <ul>
                            <li class="nav-footer-col"> สินค้าและบริการ</li>
                            <li><a class="next-link-to" href="../Auth/Product_connSell">NT Connectivity</a></li>
                            <li><a class="next-link-to" href="../Auth/Product_plathSell">NT Platform</a></li>
                        </ul>
                        <ul>
                            <li class="nav-footer-col">Digital Solutions</li>
                            <li><a class="next-link-to" href="../Auth/Product_soluSell">Digital Solutions by NT ConnX</a>
                            </li>
                        </ul>
                        <ul>
                            <li class="nav-footer-col">สั่งซื้อสินค้า</li>
                            <li><a class="next-link-to" href="https://shop.ntconnx.com/">สมัครใช้บริการ</a></li>
                        </ul>
                    </div>
                    <div class="col-6 col-md-2">
                        <ul class="pad-top">
                            <li class="nav-footer-col"> Contact Us</li>
                            <li class="Ai-li"><i class="fa fa-map-signs" aria-hidden="true"></i><a class="next-link-to as" href="https://www.google.co.th/maps/dir//connx/data=!4m6!4m5!1m1!4e2!1m2!1m1!1s0x30e283090ebee641:0x80ba2cbe12f1ba23?sa=X&ved=2ahUKEwiI-IfcvJb6AhVuRmwGHY9eBUcQ9Rd6BAgEEAU">NT
                                    ConnX</a></li>
                            <!--  <li class="Ai-li"><i class="fa fa-mobile" aria-hidden="true"></i><a class="next-link-to as"
                                    target="_blank" href="tel:025055203">0 2505 5203 (ในเวลาราชการ)</a></li> -->
                            <li class="Ai-li-1"><i class="fa fa-envelope" aria-hidden="true"></i><a class="next-link-to as" href="mailto:connx@ntplc.co.th">connx@ntplc.co.th</a>
                            </li>
                        </ul>
                        <div style="padding-top:22px"></div>
                    </div>

                </div>
            </div>
            <!-- end footer-in-main -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->

    <div id="copyright" class="copyright-main">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h6 class="copy-title"> Copyright &copy; 2022 NT ConnX 2022. All rights reserved <a href="#" target="_blank"></a> </h6>
                    <a class="next-link-to pol" href="../Auth/auth_policy" target="_blank">นโยบายคุ้มครองข้อมูลส่วนบุคคล</a>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end copyright-main -->
</div>
<!-- end footer-box -->
</div>