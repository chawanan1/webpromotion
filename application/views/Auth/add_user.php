<div class="container pad-top-100">

  <form method="post" action="Action_Adduser" onsubmit="return validate()" class=" well form-horizontal" id="contact_form myForm">
    <fieldset>

      <!-- Form Name -->
      <legend>
        <center>
          <h1><b>เพิ่มผู้ใช้งาน</b></h1>
        </center>
      </legend><br>

      <!-- Text input-->

      <div class="form-group">
        <label class="col-md-4 control-label">อีเมล</label>
        <div class="col-md-4 inputGroupContainer">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
            <input id="email" name="email" placeholder="xxx.xxx@ntplc.com" class="form-control" type="text" oninput="validate()">
          </div>
        </div>
        <small id="emaillog"></small>
      </div>

      <div class="form-group">
        <label class="col-md-4 control-label">รหัสพนักงาน</label>
        <div class="col-md-4 inputGroupContainer">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <input id="employeeID" name=" employeeID" placeholder="รหัสพนักงาน" class="form-control" type="text" oninput="validate()" maxlength="8">
          </div>
        </div>
        <small id="employeeIDlog"></small>
      </div>

      <!-- Text input-->

      <div class="form-group">
        <label class="col-md-4 control-label">ชื่อ (ภาษาไทย)</label>
        <div class="col-md-4 inputGroupContainer">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <input id="fname" name="fname" placeholder="" class="form-control" type="text" oninput="validate()">
          </div>
        </div>
        <small id="fnamelog"></small>
      </div>

      <div class="form-group">
        <label class="col-md-4 control-label">นามสกุล (ภาษาไทย)</label>
        <div class="col-md-4 inputGroupContainer">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <input id="lname" name="lname" placeholder="" class="form-control" type="text" oninput="validate()">
          </div>
        </div>
        <small id="lnamelog"></small>
      </div>

      <div class="form-group">
        <label class="col-md-4 control-label">ชื่อ (อังกฤษ)</label>
        <div class="col-md-4 inputGroupContainer">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <input id="fname_en" name="fname_en" placeholder="" class="form-control" type="text" oninput="validate()">
          </div>
        </div>
        <small id="fname_enlog"></small>
      </div>

      <div class="form-group">
        <label class="col-md-4 control-label">นามสกุล (อังกฤษ)</label>
        <div class="col-md-4 inputGroupContainer">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <input id="lname_en" name="lname_en" placeholder="" class="form-control" type="text" oninput="validate()">
          </div>
        </div>
        <small id="lname_enlog"></small>
      </div>

      <div class="form-group">
        <label class="col-md-4 control-label">สิทธิ์ในการเข้าถึง Sale / Admin</label>
        <div class="col-md-4 selectContainer">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
            <select id="role" name="role" class="form-control selectpicker" oninput="validate()">
              <option value="">Select your Sale/Admin</option>
              <option value="sale">sale</option>
              <option value="admin">admin</option>
            </select>
          </div>
        </div>
        <small id="rolelog"></small>
      </div>

      <div class="form-group">
        <label class="col-md-4 control-label">ชื่อเต็มส่วนงาน</label>
        <div class="col-md-4 inputGroupContainer">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-book"></i></span>
            <input id="full_segment" name="full_segment" placeholder="" class="form-control" type="text" oninput="validate()">
          </div>
        </div>
        <small id="full_segmentlog"></small>
      </div>



      <div class="form-group">
        <label class="col-md-4 control-label">โทรศัพท์มือถือ</label>
        <div class="col-md-4 inputGroupContainer">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
            <input id="mobile" name="mobile" placeholder="" class="form-control" type="text" oninput="validate()" maxlength="10">
          </div>
        </div>
        <small id="mobilelog"></small>
      </div>

      <div class="form-group">
        <label class="col-md-4 control-label">โทรศัพท์</label>
        <div class="col-md-4 inputGroupContainer">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
            <input id="phone" name="phone" placeholder="" class="form-control" type="text" oninput="validate()" maxlength="9">
          </div>
        </div>
        <small id="phonelog"></small>
      </div>

      <!-- Select Basic -->

      <!-- Success message -->
      <div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Success!.</div>

      <!-- Button -->
      <div class="form-group">
        <label class="col-md-4 control-label"></label>
        <div class="col-md-4"><br>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button name="insert" type="submit" class=" btn btn-warning">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspบันทึก <span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
        </div>
      </div>

    </fieldset>
  </form>
</div>
</div><!-- /.container -->

<script>
  function validate() {
    var email = document.getElementById('email').value;
    var employeeID = document.getElementById('employeeID').value;
    var fname = document.getElementById('fname').value;
    var lname = document.getElementById('lname').value;
    var role = document.getElementById('role').value;
    var fname_en = document.getElementById('fname_en').value;
    var lname_en = document.getElementById('lname_en').value;
    var full_segment = document.getElementById('full_segment').value;
    var mobile = document.getElementById('mobile').value;
    var phone = document.getElementById('phone').value;
    var status = false;


    if (!employeeID) {
      document.getElementById("employeeIDlog").innerHTML = "<div class='text-danger'> กรุณากรอกรหัสพนักงาน </div>";
      status = false;
    } else if (employeeID.length < 8) {
      document.getElementById("employeeIDlog").innerHTML = "<div class='text-danger'> กรุณากรอกรหัสพนักงานให้ครบ 8 หลัก </div>";
      status = false;
    } else {
      if (/^\d+$/.test(employeeID)) {
        document.getElementById("employeeIDlog").innerHTML = "<span class='text-success glyphicon glyphicon-ok'></span>";
        status = true;
      } else {
        document.getElementById("employeeIDlog").innerHTML = "<div class='text-danger'> กรุณากรอกข้อมูลในรูปแบบตัวเลขเท่านั้น </div>";
        status = false;
      }
    }

    if (!fname) {
      document.getElementById("fnamelog").innerHTML = "<div class='text-danger'> กรุณากรอกชื่อเป็นภาษาไทย </div>";
      status = false;
    } else {
      document.getElementById("fnamelog").innerHTML = "<span class='text-success glyphicon glyphicon-ok'></span>";
    }

    if (!lname) {
      document.getElementById("lnamelog").innerHTML = "<div class='text-danger'> กรุณากรอกนามสกุลเป็นภาษาไทย </div>";
      status = false;
    } else {
      document.getElementById("lnamelog").innerHTML = "<span class='text-success glyphicon glyphicon-ok'></span>";
    }


    if (!role) {
      document.getElementById("rolelog").innerHTML = "<div class='text-danger'> กรุณาเลือกสิทธิ์การใช้งาน </div>";
      status = false;
    } else {
      document.getElementById("rolelog").innerHTML = "<span class='text-success glyphicon glyphicon-ok'></span>";
    }

    if (!fname_en) {
      document.getElementById("fname_enlog").innerHTML = "<div class='text-danger'> กรุณากรอกชื่อเป็นภาษาอังกฤษ </div>";
      status = false;
    } else {
      document.getElementById("fname_enlog").innerHTML = "<span class='text-success glyphicon glyphicon-ok'></span>";
    }

    if (!lname_en) {
      document.getElementById("lname_enlog").innerHTML = "<div class='text-danger'> กรุณากรอกนามสกุลเป็นภาษาอังกฤษ </div>";
      status = false;
    } else {
      document.getElementById("lname_enlog").innerHTML = "<span class='text-success glyphicon glyphicon-ok'></span>";
    }

    if (!full_segment) {
      document.getElementById("full_segmentlog").innerHTML = "<div class='text-danger'> กรุณากรอกชื่อเต็มส่วนงาน </div>";
      status = false;
    } else {
      document.getElementById("full_segmentlog").innerHTML = "<span class='text-success glyphicon glyphicon-ok'></span>";
    }

    if (!email) {
      document.getElementById("emaillog").innerHTML = "<div class='text-danger'> กรุณากรอก Email </div>";
      status = false;
    } else {
      if (/\S+@\S+\.\S+/.test(email)) {
        document.getElementById("emaillog").innerHTML = "<span class='text-success glyphicon glyphicon-ok'></span>";
      } else {
        document.getElementById("emaillog").innerHTML = "<div class='text-danger'> กรุณากรอกข้อมูลในรูปแบบ Email เท่านั้น </div>";
        status = false;
      }
    }

    if (!mobile) {
      document.getElementById("mobilelog").innerHTML = "<div class='text-danger'> กรุณากรอกเบอร์มือถือ </div>";
      status = false;
    } else if (mobile.length < 10) {
      document.getElementById("mobilelog").innerHTML = "<div class='text-danger'> กรุณากรอกรหัสเบอร์โทรสารให้ครบ 10 หลัก </div>";
      status = false;
    } else {
      if (/^\d+$/.test(mobile)) {
        document.getElementById("mobilelog").innerHTML = "<span class='text-success glyphicon glyphicon-ok'></span>";
      } else {
        document.getElementById("mobilelog").innerHTML = "<div class='text-danger'> กรุณากรอกข้อมูลในรูปแบบตัวเลขเท่านั้น </div>";
        status = false;
      }
    }

    if (!phone) {
      document.getElementById("phonelog").innerHTML = "<div class='text-danger'> กรุณากรอกเบอร์โทรศัพท์ </div>";
      status = false;
    } else if (phone.length < 9) {
      document.getElementById("phonelog").innerHTML = "<div class='text-danger'> กรุณากรอกรหัสเบอร์โทรศัพท์ให้ครบ 9 หลัก </div>";
      status = false;
    } else {
      if (/^\d+$/.test(phone)) {
        document.getElementById("phonelog").innerHTML = "<span class='text-success glyphicon glyphicon-ok'></span>";
      } else {
        document.getElementById("phonelog").innerHTML = "<div class='text-danger'> กรุณากรอกข้อมูลในรูปแบบตัวเลขเท่านั้น </div>";
        status = false;
      }
    }
    return status;
  }
</script>