<!--======================================== //    Start Silde Show Banner // ======================================================-->
<div id="home" class=" full-screen-mode parallax">
    <div class="container text-center pad-bottom-100">
        <div class="alignfull-page">
        </div>
    </div>
    <!-- end container -->
</div>
<!-- end banner -->

<!--======================================== //    End Silde Show Banner // ===================================================-->

<div class="content pad-bottom-100">

    <div class="container">
        <h1 class="block-product text-center pad-top-40"> ข่าวสาร / กิจกรรม </h1>
        <hr class="pro-hr" style="width: 20%;">
        <!--  <h1 class="block-product1 text-center"> For Digital Solution Provider </h1> -->
        <!-- Table -->
        <div class="fileupload">
            <div class="item-list">

                <?php
                foreach ($resultNewsEvent as $item) {
                    $html = '';
                    for ($i = 0; $i < count($item); $i++) {

                        $date = date_create($item[$i]->date);
                        $NewDate = date('d-m-Y', strtotime("+7 day", strtotime(date_format($date, "d-m-Y"))));

                        if ($NewDate > date("d-m-Y")) {

                            $html .=
                                '<div class="item"> 
                                    <div class="itemcell">
                                        <div class="row">
                                            <div class="class="col-xs-12 col-md-12 col-sm-12">

                                                <div class="dwn-icn"><img class="img-new-event" src="../assets/images/new.gif"></div>                                                   
        
                                                <div class="txt_content3 desc-txt text">
                                                    <h1>' . $item[$i]->header . '</h1>
                                                    <p>' . $item[$i]->content . '</p>
                                                    <p class="click-date">' . date_format($date, "d/m/Y") . '</p>
                                                </div>

                                            </div>
                                        </div>        
                                    </div>            
                                </div>';
                        } else {

                            $html .=
                                '<div class="item"> 
                                    <div class="itemcell">
                                        <div class="row">
                                            <div class="class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="dwn-icn"></div>
                                        
                                                <div class="txt_content3 desc-txt text">
                                                    <h1>' . $item[$i]->header . '</h1>
                                                    <p>' . $item[$i]->content . '</p>
                                                    <p class="click-date">' . date_format($date, "d/m/Y") . '</p>
                                                </div>
                                            </div>
                                        </div>        
                                    </div>            
                                </div>';
                        }
                    }
                }
                echo $html;
                ?>

            </div>
        </div>
    </div>
</div>
<!-- END Table -->
</div>



<!--======================================== //    Start Extension Modal2 // ======================================================-->

<a href="#" class="scrollup" style="display: none;">Scroll</a>

<section id="testimonial">
    <div class="sec-prefooter">
        <div class="container">
            <div class="box-prefooter t-bold">
                <div class="prefoot-contact-title">สอบถามข้อมูลเพิ่มเติมได้ที่</div>
                <div class="prefoot-contact-group">
                    <div class="prefoot-contact-group-call">
                        <div class="prefoot-contact-group-call-text">ศูนย์บริการ<br>ลูกค้าธุรกิจ</div>
                        <a href="#" class="prefoot-contact-group-call-num txt-gdcolor">NT ConnX</a>
                    </div>
                    <div class="prefoot-contact-group-or t-medium"><span>หรือ</span></div>
                    <div class="prefoot-contact-group-call"><a href="javascript:void(0)" onclick="window.location.href='form'" class="btn3 btn3-secondary"><span>สอบถามข้อมูลเพิ่มเติม</span></a></div>
                </div>
            </div>
        </div>
    </div>
</section>