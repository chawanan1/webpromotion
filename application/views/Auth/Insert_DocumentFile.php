<div class="container pad-top-100">

    <form method="post" action="Action_Insert_DocumentFile" class="well form-horizontal" id="contact_form" enctype="multipart/form-data">
        <fieldset>

            <!-- Form Name -->
            <legend>
                <center>
                    <h1><b>เพิ่มเอกสาร</b></h1>
                </center>
            </legend><br>

            <!-- Text input-->

            <div class="form-group">
                <label class="col-md-4 control-label">หัวข้อเอกสาร</label>
                <div class="col-md-4 inputGroupContainer">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-file"></i></span>
                        <input id="doc_name" name=" doc_name" placeholder="" class="form-control" type="text" oninput="validate()">
                    </div>
                </div>
                <small id="doc_namelog"></small>
            </div>

            <!-- File input-->
            <div class="form-group">
                <label class="col-md-4 control-label">URL ไฟล์</label>
                <div class="col-md-4 inputGroupContainer">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-link"></i></span>
                        <input id="doc_file" name="doc_file" placeholder="https://file" class="form-control" type="text" oninput="validate()">
                    </div>
                </div>
                <small id="doc_filelog"></small>
            </div>

            <!-- Select Basic -->

            <!-- Success message -->
            <div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Success!.</div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label"></label>
                <div class="col-md-4"><br>
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button name="insert" type="submit" onclick="return validate()" class=" btn btn-warning">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspบันทึก <span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                </div>
            </div>

        </fieldset>
    </form>
</div>
</div><!-- /.container -->
<script>
    function validate() {
        var doc_name = document.getElementById('doc_name').value;
        var doc_file = document.getElementById('doc_file').value;
        var status = false;

        if (!doc_name) {
            document.getElementById("doc_namelog").innerHTML = "<div class='text-danger'> กรุณากรอกหัวข้อเอกสาร </div>";
            status = false;
        } else {
            document.getElementById("doc_namelog").innerHTML = "<span class='text-success glyphicon glyphicon-ok'></span>";
            status = true;
        }

        if (!doc_file) {
            document.getElementById("doc_filelog").innerHTML = "<div class='text-danger'> กรุณากรอก URL ไฟล์ </div>";
            status = false;
        } else {
            if (/^(ftp|http|https):\/\/[^ "]+$/.test(doc_file)) {
                document.getElementById("doc_filelog").innerHTML = "<span class='text-success glyphicon glyphicon-ok'></span>";


            } else {
                document.getElementById("doc_filelog").innerHTML = "<div class='text-danger'> กรุณากรอก URL ไฟล์ในรูปแบบ Link URL </div>";
                status = false;
            }
        }
        return status;
    }
</script>