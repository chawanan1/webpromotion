<?php
echo '
<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">';
//เช็คว่ามีตัวแปร session อะไรบ้าง
/* print_r($_SESSION); */
//exit();
//สร้างเงื่อนไขตรวจสอบสิทธิ์การเข้าใช้งานจาก session
if (empty($_SESSION['id']) && empty($_SESSION['nameEN']) && empty($_SESSION['employeeID']) && empty($_SESSION['role'])) {
    echo '<script>
                setTimeout(function() {
                swal({
                title: "คุณไม่มีสิทธิ์ใช้งานหน้านี้",
                type: "error"
                }, function() {
                window.location = "../"; //หน้าที่ต้องการให้กระโดดไป
                });
                }, 1000);
                </script>';
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <!-- <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Site Metas -->
    <title>NT Conn|X</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Site Icons -->
    <link rel="shortcut icon" href="../assets/images/favicon-96x96.ico" type="image/x-icon">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../assets/css/orange.css">
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">

    <!-- Site CSS -->
    <link rel="stylesheet" href="../assets/css/style_card.css">
    <link rel="stylesheet" href="../assets/css/style.css">

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="../assets/css/responsive.css">

    <!-- color -->
    <link rel="stylesheet" href="../assets/fonts/font.css">
    <link rel="stylesheet" href="../assets/fonts/fontnt.css">
    <!-- new style Product -->
    <link rel="stylesheet" href="../assets/css/style_product.css">
    <!-- <link  rel="stylesheet" href="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1356355/responsiveslides.css" /> -->
    <script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/ResponsiveSlides.js/1.55/responsiveslides.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ResponsiveSlides.js/1.55/responsiveslides.min.js"></script>
    <!-- Modernizer -->
    <script src="../assets/js/modernizer.js"></script>

    <!-- ===================================[]======================================================= -->

    <!-- ===================================[]======================================================= -->
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-7QT9W6PG2Z"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-7QT9W6PG2Z');
    </script>
</head>

<body>