<div id="site-header">
    <header id="header" class="header-block-top">
        <div class="container">
            <div class="row">
                <div class="main-menu">
                    <!-- navbar -->
                    <nav class="navbar navbar-default" id="mainNav">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <div class="logo">
                                <a class="navbar-brand js-scroll-trigger logo-header" href="home">
                                    <img src="./assets/images/LGConnX2.png" alt=""> <!-- img logo .png -->
                                </a>
                            </div>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <!-- <li><a href="../home">Home</a></li> -->
                                <!-- <li><a href="#">HOT Promotion</a></li> -->
                                <li><a href="Product">Product</a></li>
                                <li><a href="partner">Join Us</a></li>
                                <li><a href="Auth"><i style="color:#545859; letter-spacing: 5px;vertical-align: -2px;" class="fa fa-user-circle-o"></i>LOGIN</a></li>
                                <!-- <li><a href="../XBlog">X-Blog</a></li> -->
                                <!--  <li style="color:white"><a href="Login"><i class="fa fa-user-circle-o"></i> Login</a></li> -->
                            </ul>
                        </div>
                        <!-- end nav-collapse -->
                    </nav>
                    <!-- end navbar -->
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end container-fluid -->
    </header>
    <!-- end header -->
</div>
<!--======================================== //       End Navarbar    // ==========================================================-->