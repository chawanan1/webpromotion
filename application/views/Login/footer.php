<!--===============================================================================================-->
<script src="./assets/Vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="./assets/Vendor/bootstrap/js/popper.js"></script>
<script src="./assets/Vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="./assets/Vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="./assets/Vendor/tilt/tilt.jquery.min.js"></script>
<script>
	$('.js-tilt').tilt({
		scale: 1.1
	})
</script>
<!--===============================================================================================-->
<script src="./Vendor/js/main.js"></script>



</body>

</html>