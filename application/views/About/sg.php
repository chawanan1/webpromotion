<!--======================================== //    Start Silde Show Banner // ======================================================-->
<div class="container text-center">
    <div class="alignfull-page">
        <!-- <a href="about"><img class="img-responsive"
                    src="/assets/img/T11.png"></a> -->
        <img class="img-responsive" src="/assets/img/commu/digital-tech.png">
    </div>
</div>

<!--======================================== //    End Silde Show Banner // ===================================================-->
<section class="page-section" id="services" style="padding-bottom: 2rem;">
    <div class="container" style="margin-top: 5%;">
        <a class="btn1 btn-link" href="../home" style="color: #545859;margin-bottom: 2%; text-decoration: underline" type="button">
            <svg class="svg-inline--fa fa-arrow-circle-left fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-circle-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                <path fill="currentColor" d="M256 504C119 504 8 393 8 256S119 8 256 8s248 111 248 248-111 248-248 248zm28.9-143.6L209.4 288H392c13.3 0 24-10.7 24-24v-16c0-13.3-10.7-24-24-24H209.4l75.5-72.4c9.7-9.3 9.9-24.8.4-34.3l-11-10.9c-9.4-9.4-24.6-9.4-33.9 0L107.7 239c-9.4 9.4-9.4 24.6 0 33.9l132.7 132.7c9.4 9.4 24.6 9.4 33.9 0l11-10.9c9.5-9.5 9.3-25-.4-34.3z"></path>
            </svg>

            กลับสู่หน้าข่าวทั้งหมด
        </a>

        <div class="text-left margin">
            <h12>คุณสมบัติความสามารถ ข้อดี ข้อจำกัด ของ 5G 26GHz.</h12>
        </div>
        <br>
        <p style="text-indent: 4rem; margin-top: 2%;">
            <strong><i>เทคโนโลยี 5G จะเข้ามาช่วยสนับสนุน และขยายโอกาสการบริการดิจิทัลให้กว้างขวาง
                    และครอบคลุมตอบโจทย์แก่ผู้ใช้งานได้มากขึ้นควบคู่ไปกับบริการ 3G หรือ 4G ที่มีอยู่
                    แล้วในปัจจุบัน ซึ่งเทคโนโลยีที่เรียกว่า 5G นั้น จะประกอบไปด้วย 3 คุณสมบัติหลัก คือ EMBB, URLLC, MMTC
                </i></strong>
        </p>
        <div style="text-align: center;">
            <div class="row justify-content-lg-center">
                <div class="col-12 col-sm-12 col-lg-12">

                    <img class="img-fluid center-block" src="https://snz04pap002files.storage.live.com/y4mh3Sj7WLlAnKh4rlAtL3YOq1mKZfYsyRTxmrTstApn7xfu8w9rj7EXfZuopjUEG5Qgk-3-RxCJl-nzs7CemX4vKnrowCvXMILTaN8A7iQ63klN7o1G5inADtHEw5U4t-kV5m0FB0j4Yp1xpvE9uFxms8bbOqHByB8j0Cm5el-pHfdn3rceRZXN8DXYbhJu1bE?width=660&height=408&cropmode=none">

                </div>
            </div>
        </div>
        <br>
        <div class="text-center margin">
            <h12>3 คุณสมบัติหลัก ของเทคโนโลยี 5G</h12>
        </div>
        <p class="text-ab">
            <strong1>Enhanced Mobile Broadband หรือ (EMBB)</strong1>
            คือ การใช้งานในลักษณะที่ต้องการส่งข้อมูลความเร็วสูงใน
            ระดับกิกะบิตต่อวินาที (Gbps.) ซึ่งการใช้งานลักษณะนี้ตอบ
            สนองรูปแบบการใช้งานที่ต้องการ รับ-ส่ง ข้อมูลปริมาณมากๆ

        </p>
        <p class="text-ab">
            <strong1>Ultra-reliable and Low Latency Communications หรือ (URLLC)</strong1>
            คือ การใช้งานที่ต้องการความสามารถในการส่งข้อมูลที่มีความเสถียร
            มาก รวมทั้งมีความหน่วงเวลา (latency) ในการส่งข้อมูลต่ำในระดับ 1
            มิลลิวินาที (ระบบ 4G ในปัจจุบันรองรับความหน่วงเวลาในระดับ 10
            มิลลิวินาที) ซึ่งความสามารถนี้ทำให้ระบบ 5G เหมาะกับการใช้งานระบบ
            ที่ต้องการความแม่นยำสูง (critical application) เช่น การผ่าตัดทาง
            ไกล การควบคุมเครื่องจักรในโรงงาน หรือการควบคุมรถยนต์ไร้คนขับ
            เป็นต้น
        </p>
        <p class="text-ab">
            <strong1>Massive Machine Type Communications หรือ (MMTC)</strong1>

            คือ การใช้งานที่มีการเชื่อมต่อของอุปกรณ์จำนวนมากในพื้นที่เดียวกัน
            โดยมีปริมาณมาก ถึงระดับล้านอุปรณ์ต่อตารางกิโลเมตร ซึ่งการส่ง
            ข้อมูลของอุปกรณประเภทนี้จะเป็นการส่งข้อมูลปริมาณน้อยๆ ที่ไม่ต้อง
            การความเร็วสูง หรือความหน่วงเวลาต่ำ อุปกรณ์โดยทั่วไปมีราคาถูก
            และมีอายุการใช้งานของแบตเตอรี่ที่มากกว่าอุปกรณ์ทั่วไป ซึ่งความ
            สามารถนี้ทำให้ระบบ 5G เหมาะสมกับการทำงานของอุปกรณ์จำพวก IoT

        </p>

        <br>
        <div class="text-center margin">
            <h12>จุดเด่นของ 5G 26 GHz.</h12>
        </div>
        <p class="text-ab">
            <strong1>ความถี่ย่าน 26 GHz. จัดอยู่ในคลื่นย่านความถี่ที่สูงมาก มีแถบความถี่ หรือ
                แบนวิธที่กว้างสามารถนำมาใช้รับส่งข้อมูลขนาดใหญ่สามารถส่งผ่านข้อมูลความ
                ละเอียดสูงได้ อีกทั้งสามารถนำมาจัดสรรเพื่อให้เกิดความเหมาะสมในแต่ละประเภทการ
                ใช้งานได้ในพื้นที่เดียวกัน ทำให้ตอบรับการใช้งานที่แม่นยำ และสามารถนำไปสร้างเป็น
                5G Private Network ที่สามารถแยกเน็ตเวิร์ก (Dedicated) ใช้งานเฉพาะพื้นที่ให้
                เกิดความปลอดภัย (Secure) ของข้อมูลภายในโรงงานอุตสาหกรรม สามารถนำคลื่น
                ความถี่มาแบ่งใช้งานให้เหมาะสม (Optimized) กับแต่ละรูปแบบการใช้งานทั้งการส่ง
                ข้อมูลปริมาณมหาศาลเพื่อนำไปวิเคราะห์ หรือเน้นที่ความเร็วในการตอบสนอง ซึ่งผู้ให้
                บริการเครือข่ายและโรงงานอุตสาหกรรมต้องทำงานร่วมกันอย่างใกล้ชิด</strong1>

        </p>

        <br>

        <div class="text-center margin">
            <h12>ข้อจำกัดของคลื่น 5G 26 GHz.</h12>
        </div>
        <p class="text-ab">
            <strong1>1</strong1>
            เนื่องจากเป็นคลื่นความถี่ที่สูงมาก อาจจะไม่สามารถส่ง
            ผ่านสิ่งกีดขวางได้ดี เมื่อเทียบกับคลื่นความถี่ที่ต่ำกว่า

        </p>
        <p class="text-ab">
            <strong1>2</strong1>
            เนื่องจากเป็นคลื่นความถี่สูง เมื่อใช้กำลังส่งแผ่กระจาย
            คลื่นเท่ากัน จะครอบคลุมพื้นที่แคบกว่าคลื่นย่านอื่น
        </p>
        <p class="text-ab">
            <strong1>3</strong1>
            อุปกรณ์ปลายทางอาจต้องใช้พลังงานในการส่งข้อมูล
            กลับมาเพิ่มมากขึ้นส่งผลให้อายุการใช้งาน Battery จะ
            สั้นลง

        </p>

        <br>

    </div>
</section>
<!--======================================== //    Start HotPromotion // ======================================================-->


<!-- <a href="#" class="scrollup" style="display: none;">Scroll</a> -->