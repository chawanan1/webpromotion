<!--======================================== //    Start Silde Show Banner // ======================================================-->
<div class="container text-center">
    <div class="alignfull-page">
        <!-- <a href="about"><img class="img-responsive"
                    src="/assets/img/T11.png"></a> -->
        <img class="img-responsive" src="../../assets/img/commu/digital-tech.png">
    </div>
</div>

<!--======================================== //    End Silde Show Banner // ===================================================-->
<section class="page-section" id="services" style="padding-bottom: 2rem;">
    <div class="container" style="margin-top: 5%;">
        <a class="btn1 btn-link" href="../../home" style="color: #545859;margin-bottom: 2%; text-decoration: underline" type="button">
            <svg class="svg-inline--fa fa-arrow-circle-left fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-circle-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                <path fill="currentColor" d="M256 504C119 504 8 393 8 256S119 8 256 8s248 111 248 248-111 248-248 248zm28.9-143.6L209.4 288H392c13.3 0 24-10.7 24-24v-16c0-13.3-10.7-24-24-24H209.4l75.5-72.4c9.7-9.3 9.9-24.8.4-34.3l-11-10.9c-9.4-9.4-24.6-9.4-33.9 0L107.7 239c-9.4 9.4-9.4 24.6 0 33.9l132.7 132.7c9.4 9.4 24.6 9.4 33.9 0l11-10.9c9.5-9.5 9.3-25-.4-34.3z"></path>
            </svg>
            กลับสู่หน้าข่าวทั้งหมด
        </a>

        <?php
        foreach ($resultWorkmanShip as $item) {
            $html = '';
            for ($i = 0; $i < count($item); $i++) {
                if ($item[$i]->id == $A_id) {

                    $html .= '' . $item[$i]->content . '';
                }
            }
        }
        echo $html;
        ?>
    </div>
</section>

<a href="#" class="scrollup" style="display: none;">Scroll</a>
<!--======================================== //    Start HotPromotion // ======================================================-->


<!-- <a href="#" class="scrollup" style="display: none;">Scroll</a> -->