<!--======================================== //    Start Silde Show Banner // ======================================================-->
<div class="container text-center">
    <div class="alignfull-page">
        <!-- <a href="about"><img class="img-responsive"
                    src="./assets/img/T11.png"></a> -->
        <img class="img-responsive" src="./assets/img/commu/digital-tech.png">
    </div>
</div>

<!--======================================== //    End Silde Show Banner // ===================================================-->
<section class="page-section" id="services" style="padding-bottom: 2rem;">
    <div class="container" style="margin-top: 5%;">
        <a class="btn1 btn-link" href="home" style="color: #545859;margin-bottom: 2%; text-decoration: underline" type="button">
            <svg class="svg-inline--fa fa-arrow-circle-left fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-circle-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                <path fill="currentColor" d="M256 504C119 504 8 393 8 256S119 8 256 8s248 111 248 248-111 248-248 248zm28.9-143.6L209.4 288H392c13.3 0 24-10.7 24-24v-16c0-13.3-10.7-24-24-24H209.4l75.5-72.4c9.7-9.3 9.9-24.8.4-34.3l-11-10.9c-9.4-9.4-24.6-9.4-33.9 0L107.7 239c-9.4 9.4-9.4 24.6 0 33.9l132.7 132.7c9.4 9.4 24.6 9.4 33.9 0l11-10.9c9.5-9.5 9.3-25-.4-34.3z"></path>
            </svg>

            กลับสู่หน้าข่าวทั้งหมด
        </a>

        <div style="text-align: center;">
            <div class="row justify-content-lg-center">
                <div class="col-12 col-sm-12 col-lg-12">

                    <img class="img-fluid center-block" src="https://snz04pap002files.storage.live.com/y4m7VSHZH9NR44cWIYpM6fFK-B_DAIRumPujbtYxp9UozF6UlYErs4TiJZ0u8rGw5bHeOOfO74W-MAc7lyAh11ziaL4VXf7Edx-Icoik4lPezUCkMDq4-qnR2UsZ-k9KqQxu5_9RWjWOqk0tiYz8XyqB02ysXY7p6n8hD_TXsuVbibZMfS9TOWkH82BRYRRNWRJ?width=467&height=660&cropmode=none">


                </div>
            </div>
        </div>
        <br>
        <div style="text-align: center;">
            <div class="row justify-content-lg-center">
                <div class="col-12 col-sm-12 col-lg-12">

                    <img class="img-fluid center-block" src="https://snz04pap002files.storage.live.com/y4mjk_CyjmFMKhv7ZbKkYSgW2d7bcxHxL0OQ7ZigBonyE4spSxKmhDeepu2jqPOioFkDe08x6pz2qsBtw4c_JbeuE9-1KEb6oGdS1CHHEKEFfkb8bOVJwCi1XMxi3pvLNr9DBGtXr0WX9W5We8TZGAhb5gSj6kHpyDL5ZSSE9JMwd6tLCXzb2YV6mRhGFOYjYhC?width=467&height=660&cropmode=none">


                </div>
            </div>
        </div>
        <br>
        <div style="text-align: center;">
            <div class="row justify-content-lg-center">
                <div class="col-12 col-sm-12 col-lg-12">

                    <img class="img-fluid center-block" src="https://snz04pap002files.storage.live.com/y4mgf5rJ9MzzvDJz0YbSCgDC9eJxF3gS0P98ojQ59WltkcbWf7vGYgjSZ2HkSfB-FZknaBrdtiaAe751zV0pBGLkBuLhDPmMS4ZyBNsTkD4HSaB79a1fF6T0OqExvVNdzUSO8uvNuNAM5-W9wZ4RPKd_bl7HzuKc6nRwdovWtwF7N-AklFocox20aQbBV8NYaay?width=467&height=660&cropmode=none">

                </div>
            </div>
        </div>
        <br>
        <div style="text-align: center;">
            <div class="row justify-content-lg-center">
                <div class="col-12 col-sm-12 col-lg-12">

                    <img class="img-fluid center-block" src="https://snz04pap002files.storage.live.com/y4mGpl2HRUEIZVrRC3AZ7xP-KYFW3ZwljXGfO7jH-cltmOkrRn4_TqLgFfIezyPcDh9rAzh1TCx0sYnVZCB5knFJXTs-ArGaZue0dDeHBb8GayQblvn7NTSTvQnrrfPOn9ubDZpPW3ISB4XtZY-R2cc-3jiK5LKzEMvcYBxXWN2-mN6PizxWJ_x_l8M1gvR-MMF?width=467&height=660&cropmode=none">

                </div>
            </div>
        </div>

    </div>
</section>
<!--======================================== //    Start HotPromotion // ======================================================-->


<!-- <a href="#" class="scrollup" style="display: none;">Scroll</a> -->