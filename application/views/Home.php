<!--======================================== //    Start Silde Show Banner // ======================================================-->
<div id="home" class=" full-screen-mode parallax">
  <div class="container text-center">
    <div class="alignfull-page">
      <!-- <a href="about"><img class="img-responsive"
                    src="/assets/img/T11.png"></a> -->
      <img class="img-responsive" src="./assets/img/Banner3.png">


    </div>
  </div>
  <!-- end container -->
</div>
<!-- end banner -->

<!--======================================== //    End Silde Show Banner // ===================================================-->

<div class="content">

  <div class="container">
    <h1 class="block-product text-center pad-top-40"> Product </h1>
    <h1 class="block-product1 text-center"> For Digital Solution Provider </h1>
  </div>


  <div class="site-section bg-left-half mb-5 pad-bottom-50">
    <div class="container owl-3-style">
      <div class="owl-carousel owl-3">
        <!-- ============================================================================================ -->
        <?php
        foreach ($resultProduct as $item) {
          $html = '';
          for ($i = 0; $i < count($item); $i++) {
            if ($item[$i]->Service == "NT Connectivity") {
              $html .=
                '<div class="media-29101">
                  <div class="col">
                    <img src="' . $item[$i]->Image . '" alt="Image" class="img-fluid">        
                    <small class="smsolt">[บริการ] : ' . $item[$i]->Service . '</small>
                  </div>
                  <div class="col">
                    <p class="quote">         
                      [แพ็กเกจ]  ' . $item[$i]->Package . '          
                      <br><br>                       
                      <button class="button-50" role="button" data-toggle="modal" data-target="#myModal' . $item[$i]->id . '">ดูเพิ่มเติม++</button>
                      <br>                                          
                      <a class="btn button-51" href="form/Form_Read/' . $item[$i]->id . '" role="button">ให้เจ้าหน้าที่ติดต่อกับ</a>
                    </p>
                  </div>
                </div>';
            }
            if ($item[$i]->Service == "NT Platform") {
              $html .=
                '<div class="media-29101">
                  <div class="col">
                    <img src="' . $item[$i]->Image . '" alt="Image" class="img-fluid">              
                    <small class="smsolt">[บริการ] : ' . $item[$i]->Service . '</small>
                  </div>
                  <div class="col">
                    <p class="quote">         
                      [แพ็กเกจ]  ' . $item[$i]->Package . '          
                      <br><br>
                      <button class="button-50" role="button" data-toggle="modal" data-target="#myModal' . $item[$i]->id . '">ดูเพิ่มเติม++</button>
                      <br>        
                      <a class="btn button-51" href="form/Form_Read/' . $item[$i]->id . '" role="button">ให้เจ้าหน้าที่ติดต่อกับ</a>
                    </p>
                  </div>
                </div>';
            }
          }
        }
        echo $html;
        ?>
        <!-- ============================================================================================ -->
      </div>
    </div>
    <div class="button-container-1">
      <span class="mas">More</span>
      <button id='work' type="button" name="Hover" onClick="javascript:window.open('Product', '_blank');">More</button>
    </div>
  </div>
</div>

<!--======================================== //    Start HotPromotion // ======================================================-->
<div class="content bg-colors2">

  <div class="container">
    <h1 class="block-product text-center pad-top-40"> Digital Solution <small class="sm1">Power By </small>NT ConnX </h1>
    <h1 class="block-product1 text-center"> Digital Solution Make Business Easy </h1>
  </div>

  <div class="site-section bg-left-half mb-5 pad-bottom-50">
    <div class="container owl-3-style">
      <div class="owl-carousel owl-3">
        <!-- ============================================================================================ -->
        <?php
        foreach ($resultProduct as $item) {
          $html = '';
          for ($i = 0; $i < count($item); $i++) {
            if ($item[$i]->Service == "Digital Solutions by NT ConnX") {
              $html .=
                '<div class="media-29101">
                  <div class="col">
                    <img src="' . $item[$i]->Image . '" alt="Image" class="img-fluid">        
                    <small class="smsolt">[บริการ] : ' . $item[$i]->Service . '</small>
                  </div>
                  <div class="col">
                    <p class="quote">         
                      [แพ็กเกจ]  ' . $item[$i]->Package . '          
                      <br><br>                       
                      <button class="button-50" role="button" data-toggle="modal" data-target="#myModal' . $item[$i]->id . '">ดูเพิ่มเติม++</button>
                      <br>                                          
                      <a class="btn button-51" href="form/Form_Read/' . $item[$i]->id . '" role="button">ให้เจ้าหน้าที่ติดต่อกับ</a>
                    </p>
                  </div>
                </div>';
            }
          }
        }
        echo $html;
        ?>
        <!-- ============================================================================================ -->
      </div>
    </div>
    <div class="button-container-1">
      <span class="mas">More</span>
      <button id='work' type="button" name="Hover" onClick="javascript:window.open('Product_solu', '_blank');">More</button>
    </div>
  </div>
</div>
<!--=========================================================================================================================-->
<div class="content">

  <div class="container">
    <h1 class="block-product text-center pad-top-40"> NT CONNX Work and Experience </h1>
    <h1 class="block-product2 text-center pad-top-40"> Digital and Technology </h1>
  </div>

  <div class="site-section bg2-left-half mb-5 pad-bottom-60">
    <div class="container owl-2-style">
      <div class="owl-carousel owl-2">

        <?php
        foreach ($resultWorkmanShip as $item) {
          $html = '';
          for ($i = 0; $i < count($item); $i++) {
            $date = date_create($item[$i]->date);
            $html .=
              '<div class="media-29103">
                <img src="' . $item[$i]->image . '" alt="Image" class="img-fluid">
                <p class="quote1"><a class="aq1" href="About/about_read/' . $item[$i]->id . '">' . $item[$i]->header . '</a></p>
                <p></p>
                <p></p>
                <p></p></br>
                <p class="qdate">' . date_format($date, "d/m/Y") . '</p>
              </div>';
          }
        }
        echo $html;
        ?>

      </div>
    </div>
  </div>

</div>
<!-- ======================================= //    START SLIDE FOOTER // ===================================================== -->
<!-- <div class="container">
    <h1 class="block-product text-center pad-top-40"> CONTACT </h1> -->
<!-- <h1 class="block-product1 text-center pad-top-40"> Digital and Technology </h1> -->
<section class="ftco-section pad-top-50">
  <div class="container">
    <h1 class="block-product text-center "> CONTACT </h1>
    <div class="row justify-content-center">
      <div class="col-md-6 text-center mb-5">
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="wrapper">
          <div class="row no-gutters mb-5">
            <div class="col-md-7">

              <div class="row">
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-sm-1" style="text-align: right;">
                      <svg class="svg-inline--fa fa-map-marker-alt fa-w-12" style="color: #FFD100; width:13px;" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="map-marker-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" data-fa-i2svg="">
                        <path fill="currentColor" d="M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z"></path>
                      </svg><!-- <i class="fas fa-map-marker-alt" style="color: #FFD100;"></i> -->
                    </div>
                    <div class="col-sm-11" style="color:#545859 ;font-weight: 300; font-size:21px;">
                      <p>บริษัท โทรคมนาคมแห่งชาติ จำกัด (มหาชน) <br>89/2 ถนนแจ้งวัฒนะ แขวงทุ่งสองห้อง
                        เขตหลักสี่ กรุงเทพ 10210
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-sm-1" style="text-align: right;">
                    </div>
                    <div class="col-sm-11" style="color:#545859 ;font-weight: 300; font-weight: bold;">
                      <p>ติดต่อสินค้าและบริการ</p>

                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-1" style="text-align: right;">
                      <svg class="svg-inline--fa fa-envelope fa-w-16" style="color: #FFD100; width:13px;" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="envelope" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                        <path fill="currentColor" d="M502.3 190.8c3.9-3.1 9.7-.2 9.7 4.7V400c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V195.6c0-5 5.7-7.8 9.7-4.7 22.4 17.4 52.1 39.5 154.1 113.6 21.1 15.4 56.7 47.8 92.2 47.6 35.7.3 72-32.8 92.3-47.6 102-74.1 131.6-96.3 154-113.7zM256 320c23.2.4 56.6-29.2 73.4-41.4 132.7-96.3 142.8-104.7 173.4-128.7 5.8-4.5 9.2-11.5 9.2-18.9v-19c0-26.5-21.5-48-48-48H48C21.5 64 0 85.5 0 112v19c0 7.4 3.4 14.3 9.2 18.9 30.6 23.9 40.7 32.4 173.4 128.7 16.8 12.2 50.2 41.8 73.4 41.4z"></path>
                      </svg><!-- <i class="fas fa-envelope" style="color: #FFD100;"></i> -->
                    </div>
                    <div class="col-sm-11" style="color:#06245a ;font-weight: 300; font-size:21px;">
                      <a href="mailto:connx@ntplc.co.th">
                        <p>connx@ntplc.co.th</p>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="row">
                    <!--   <div class="col-sm-1" style="text-align: right;">
                                <svg class="svg-inline--fa fa-mobile-alt fa-w-10" style="color: #FFD100; width:13px;" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="mobile-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" data-fa-i2svg=""><path fill="currentColor" d="M272 0H48C21.5 0 0 21.5 0 48v416c0 26.5 21.5 48 48 48h224c26.5 0 48-21.5 48-48V48c0-26.5-21.5-48-48-48zM160 480c-17.7 0-32-14.3-32-32s14.3-32 32-32 32 14.3 32 32-14.3 32-32 32zm112-108c0 6.6-5.4 12-12 12H60c-6.6 0-12-5.4-12-12V60c0-6.6 5.4-12 12-12h200c6.6 0 12 5.4 12 12v312z"></path></svg>
                            </div>
                            <div class="col-sm-11" style="color:#545859 ;font-weight: 300;font-size:21px;">
                                <p>0 2505 5203 (ในเวลาราชการ)</p>
                            </div> -->
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-5 d-flex align-items-stretch">
              <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d242.07585007392763!2d100.57588589741545!3d13.88617249550199!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x80ba2cbe12f1ba23!2sNT%20ConnX!5e0!3m2!1sth!2sth!4v1659674773160!5m2!1sth!2sth" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<a href="#" class="scrollup" style="display: none;">Scroll</a>

<section id="testimonial">
  <div class="sec-prefooter">
    <div class="container">
      <div class="box-prefooter t-bold">
        <div class="prefoot-contact-title">สอบถามข้อมูลเพิ่มเติมได้ที่</div>
        <div class="prefoot-contact-group">
          <div class="prefoot-contact-group-call">
            <div class="prefoot-contact-group-call-text">ศูนย์บริการ<br>ลูกค้าธุรกิจ</div>
            <a href="#" class="prefoot-contact-group-call-num txt-gdcolor">NT ConnX</a>
          </div>
          <div class="prefoot-contact-group-or t-medium"><span>หรือ</span></div>
          <div class="prefoot-contact-group-call"><a href="javascript:void(0)" onclick="window.location.href='form'" class="btn3 btn3-secondary"><span>สอบถามข้อมูลเพิ่มเติม</span></a></div>
        </div>
      </div>
    </div>
  </div>
</section>