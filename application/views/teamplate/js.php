<!-- end footer-main -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script>
  var script_url = "https://script.google.com/macros/s/AKfycbwO9Q3M44UjadS5k06M1WallIMAdJyB5RyFyYb3NUa3OkulOzUZU2s8NIg_981gV-sSjQ/exec";

  // Make an AJAX call to Google Script
  function insert_value() {

    var id1 = $("#id").val();
    var name = $("#name").val();
    var ph = $("#ph").val();
    var f1 = $("#f1").val();
    var f2 = $("#f2").val();


    var url = script_url + "?callback=ctrlq&name=" + name + "&id=" + id1 + "&ph=" + ph + "&f1=" + f1 + "&f2=" + f2 + "&action=insert";


    var request = jQuery.ajax({
      crossDomain: true,
      url: url,
      method: "GET",
      dataType: "jsonp"
    });

  }
</script>
<script>
  function openFormID() {
    document.getElementById("myFormID").style.display = "block";
  }

  function openForm1() {
    document.getElementById("myForm1").style.display = "block";
  }

  function closeFormID() {
    document.getElementById("myFormID").style.display = "none";
  }

  function closeForm1() {
    document.getElementById("myForm1").style.display = "none";
  }
</script>
<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-app.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
<script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-analytics.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-database.js"></script>
<script src="./assets/js/cookie.js"></script>
<!-- ALL JS FILES -->
<script src="./assets/js/all.js"></script>
<script src="./assets/js/bootstrap.min.js"></script>
<!-- ALL PLUGINS -->
<script src="./assets/js/custom.js"></script>
<script src="./assets/js/owl.carousel.min.js"></script>
</body>

</html>