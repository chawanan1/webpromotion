<div class="home-slider">
  <ul class="rslides" id="slider1">
    <li><img src="./assets/img/product/B1.png" alt="slide1"> </li>
    <li><img src="./assets/img/product/B2.png" alt="slide2"> </li>
    <li><img src="./assets/img/product/B3.png" alt="slide3"> </li>
  </ul>
</div>

<!-- =============================================================================================================================== -->
<section class="u-align-center u-clearfix u-gradient u-section-4" id="carousel_7130">
  <div class="u-clearfix u-sheet u-valign-middle u-sheet-1 pad-bottom-40">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <h2 class="block-title text-center">
        Product
      </h2>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="section-title">
          <h7>เลือกบริการที่ท่านสนใจ</h7>
        </div>
        <ul id="filters" class="clearfix">
          <li><span class="filter" data-filter=".Connec, .Platform, .Solutions">All</span></li>
          <li><span class="filter" data-filter=".Connec">Connectivity</span></li>
          <li><span class="filter" data-filter=".Platform">Platform</span></li>
          <li><span class="filter active" data-filter=".Solutions">Solutions</span></li>
          <!-- <li><span class="filter" data-filter=".logo">Logo</span></li>
                            <li><span class="filter" data-filter=".web">Web</span></li> -->
        </ul>


        <!--End of Form-->
        <!--    <div class="featured__controls">
                        <ul>
                            <li  class="active" data-filter=".All" >All</li>
                            <li data-filter=".Connectivity">Connectivity</li>
                            <li data-filter=".Platform">NT Platform</li>
                            <li data-filter=".Solutions">NT Solutions </li>
                        </ul>
                    </div> -->
      </div>
    </div>
    <div id="portfoliolist2">
      <!-- <div class="panel-pricing-in">
            <div class="featured__filter"> -->
      <div class="u-list u-list-1">
        <div class="u-repeater u-repeater-1">
          <?php
          foreach ($resultProduct as $item) {
            $html = '';
            for ($i = 0; $i < count($item); $i++) {
              if ($item[$i]->Service == "NT Connectivity") {
                $html .=
                  '<div class="u-align-center u-container-style u-list-item u-radius-20 u-repeater-item u-shape-round u-white u-list-item-1 portfolio Connec" data-cat="Connec">
                    <div class="u-container-layout u-similar-container u-valign-top u-container-layout-2">                
                      <img src="' . $item[$i]->Image . '" alt="" class="u-expanded-width u-image u-image-contain u-image-default u-image-2" data-image-width="626" data-image-height="625" data-toggle="modal" data-target="#myModal' . $item[$i]->id . '">
                      <h4 class="u-text u-text-default u-text-palette-1-base u-text-3">' . $item[$i]->Package . '</h4>
                      <p class="u-text u-text-palette-1-dark-2 u-text-4">' . $item[$i]->Feature . '</p></br></br>
                      <p>
                        <p></p>
                      </p>    
                      <a class="btn button-51" href="./form/Form_Read/' . $item[$i]->id . '" role="button">สนใจสินค้า</a>                 
                      <a class="btn1 btn-lg btn-block hvr-underline-from-center" data-toggle="modal" data-target="#myModal' . $item[$i]->id . '" href="#">รายละเอียดสินค้า</a>
                    </div>
                  </div>';
              } else {
                $html .=
                  '<div class="u-align-center u-container-style u-list-item u-radius-20 u-repeater-item u-shape-round u-white u-list-item-1 portfolio ' . $item[$i]->Service . '" data-cat="' . $item[$i]->Service . '">
                    <div class="u-container-layout u-similar-container u-valign-top u-container-layout-2">                
                      <img src="' . $item[$i]->Image . '" alt="" class="u-expanded-width u-image u-image-contain u-image-default u-image-2" data-image-width="626" data-image-height="625" data-toggle="modal" data-target="#myModal' . $item[$i]->id . '">
                      <h4 class="u-text u-text-default u-text-palette-1-base u-text-3">' . $item[$i]->Package . '</h4>
                      <p class="u-text u-text-palette-1-dark-2 u-text-4">' . $item[$i]->Feature . '</p></br></br>
                      <p>
                        <p></p>
                      </p>    
                      <a class="btn button-51" href="./form/Form_Read/' . $item[$i]->id . '" role="button">สนใจสินค้า</a>                 
                      <a class="btn1 btn-lg btn-block hvr-underline-from-center" data-toggle="modal" data-target="#myModal' . $item[$i]->id . '" href="#">รายละเอียดสินค้า</a>
                    </div>
                  </div>';
              }
            }
          }
          echo $html;
          ?>
        </div>
      </div>
      <!-- </div> -->
    </div>
  </div>
</section>
<!--  -->

<!--======================================== //    End HotPromotion // ========================================================-->

<!---------------------------------------------- myModal #rr5 --------------------------------- -->
<a href="#" class="scrollup" style="display: none;">Scroll</a>
<section id="testimonial">
  <div class="sec-prefooter">
    <div class="container">
      <div class="box-prefooter t-bold">
        <div class="prefoot-contact-title">สอบถามข้อมูลเพิ่มเติมได้ที่</div>
        <div class="prefoot-contact-group">
          <div class="prefoot-contact-group-call">
            <div class="prefoot-contact-group-call-text">ศูนย์บริการ<br>ลูกค้าธุรกิจ</div>
            <a href="#" class="prefoot-contact-group-call-num txt-gdcolor">NT ConnX</a>
          </div>
          <div class="prefoot-contact-group-or t-medium"><span>หรือ</span></div>
          <div class="prefoot-contact-group-call"><a href="javascript:void(0)" onclick="window.location.href='form'" class="btn3 btn3-secondary"><span>สอบถามข้อมูลเพิ่มเติม</span></a></div>
        </div>
      </div>
    </div>
  </div>
</section>