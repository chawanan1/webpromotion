<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('GET_Product');
        $this->load->model('GET_WorkmanShip');
    }

    public function index()
    {
        $data['resultProduct'] = $this->GET_Product->Product('$dataProduct');
        $data['resultWorkmanShip'] = $this->GET_WorkmanShip->WorkmanShip('$dataWorkmanShip');
        //$data['id'] = $this->uri->segment(3);

        $this->load->view('teamplate/css');
        $this->load->view('navbar');
        $this->load->view('Home', $data);
        $this->load->view('home_extension', $data);
        $this->load->view('footer');
        $this->load->view('teamplate/js');
    }
}
