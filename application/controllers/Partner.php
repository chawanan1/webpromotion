<?php
defined('BASEPATH') or exit('No direct script access allowed');
/* ปล. ชื่อ Class กับชื่อไฟล์ใน Controller ต้องตรงกัน เช่น Class Ntwelcome ชื่อไฟล์ใน Controller ก็ต้องเป็น Ntwelcome */

class Partner extends CI_Controller
{

    public function index()
    {

        $this->load->view('partner/head');
        $this->load->view('navbar');
        $this->load->view('partner/Partner');
        $this->load->view('footer');
        $this->load->view('partner/footer');

    }
    public function line_nontify()
    {
        $this->load->view("partner/line_nontify");

    }
}
