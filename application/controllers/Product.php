<?php
defined('BASEPATH') or exit('No direct script access allowed');
/* ปล. ชื่อ Class กับชื่อไฟล์ใน Controller ต้องตรงกัน เช่น Class Ntwelcome ชื่อไฟล์ใน Controller ก็ต้องเป็น Ntwelcome */

class Product extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('GET_Product');
    }

    public function index()
    {
        $data['resultProduct'] = $this->GET_Product->Product('$dataProduct');

        $this->load->view('Product/head');
        $this->load->view('navbar');
        $this->load->view('Product', $data);
        /* $this->load->view('home_contact'); */
        $this->load->view('home_extension');
        $this->load->view('footer');
        $this->load->view('Product/footer');
    }

    public function product_conn()
    {
        $data['resultProduct'] = $this->GET_Product->Product('$dataProduct');

        $this->load->view('Product/head');
        $this->load->view('navbar');
        $this->load->view('Product_conn', $data);
        $this->load->view('home_extension');
        $this->load->view('footer');
        $this->load->view('Product/footer');
    }
    public function Product_plath()
    {
        $data['resultProduct'] = $this->GET_Product->Product('$dataProduct');

        $this->load->view('Product/head');
        $this->load->view('navbar');
        $this->load->view('Product_plath', $data);
        $this->load->view('home_extension');
        $this->load->view('footer');
        $this->load->view('Product/footer');
    }

    public function product_solu()
    {
        $data['resultProduct'] = $this->GET_Product->Product('$dataProduct');

        $this->load->view('Product/head');
        $this->load->view('navbar');
        $this->load->view('Product_solu', $data);
        /* $this->load->view('home_contact'); */
        $this->load->view('home_extension');
        $this->load->view('footer');
        $this->load->view('Product/footer');
    }
}
