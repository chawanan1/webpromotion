<?php
defined('BASEPATH') or exit('No direct script access allowed');
/* ปล. ชื่อ Class กับชื่อไฟล์ใน Controller ต้องตรงกัน เช่น Class Ntwelcome ชื่อไฟล์ใน Controller ก็ต้องเป็น Ntwelcome */

class Form extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('GET_Product');
    }

    public function index()
    {
        $this->load->view('Form/head');
        $this->load->view('navbar');
        $this->load->view('Form');
        $this->load->view('footer');
        $this->load->view('Form/footer');
    }
    public function Form_Read($S_id)
    {
        $data['resultProduct'] = $this->GET_Product->Product('$dataProduct');
        $data['S_id'] = $this->uri->segment(3);

        $this->load->view('Form/head_read');
        $this->load->view('navbar_read');
        $this->load->view('Form_Read', $data);
        $this->load->view('footer_read');
        $this->load->view('Form/footer_read');
    }
    public function plath_view()
    {
        $this->load->view('Form/head');
        $this->load->view('navbar');
        $this->load->view("F_Creative");
        $this->load->view('footer');
        $this->load->view('Form/footer');
    }
    /* plath view 2-2 */
    public function plath_productive()
    {
        $this->load->view('Form/head');
        $this->load->view('navbar');
        $this->load->view("F_Productive");
        $this->load->view('footer');
        $this->load->view('Form/footer');
    }
    /* plath view 2-3 */
    public function plath_performance()
    {
        $this->load->view('Form/head');
        $this->load->view('navbar');
        $this->load->view("F_Performance");
        $this->load->view('footer');
        $this->load->view('Form/footer');
    }
    /* =====================[END Platform]============================ */
    public function connect_view()
    {
        $this->load->view('Form/head');
        $this->load->view('navbar');
        $this->load->view("F_Eval");
        $this->load->view('footer');
        $this->load->view('Form/footer');
    }

    /* Connect view 1-2 */
    public function connect_iconn1()
    {
        $this->load->view('Form/head');
        $this->load->view('navbar');
        $this->load->view("F_iConn1");
        $this->load->view('footer');
        $this->load->view('Form/footer');
    }
    /* Connect view 1-3 */
    public function connect_firmfix()
    {
        $this->load->view('Form/head');
        $this->load->view('navbar');
        $this->load->view("F_Firm");
        $this->load->view('footer');
        $this->load->view('Form/footer');
    }
    /* =====================[END Connectivity]============================ */

    public function solutions_view()
    {
        $this->load->view('Form/head');
        $this->load->view('navbar');
        $this->load->view("F_Hotspot");
        $this->load->view('footer');
        $this->load->view('Form/footer');
    }
    /* Connect view 1-3 */
    public function solutions_security()
    {
        $this->load->view('Form/head');
        $this->load->view('navbar');
        $this->load->view("F_Security");
        $this->load->view('footer');
        $this->load->view('Form/footer');
    }
    /* =====================[END Connectivity]============================ */
    public function line_noti()
    {
        $this->load->view("Form/line_noti");
    }
}
