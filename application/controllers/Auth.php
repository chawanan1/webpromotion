<?php
defined('BASEPATH') or exit('No direct script access allowed');
/* ปล. ชื่อ Class กับชื่อไฟล์ใน Controller ต้องตรงกัน เช่น Class Ntwelcome ชื่อไฟล์ใน Controller ก็ต้องเป็น Ntwelcome */

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('LoginAPI_model');
        $this->load->model('Insert_User_model');
        $this->load->model('Insert_NewsEvent_model');
        $this->load->model('Insert_DocumentFile_model');
        $this->load->model('Insert_WorkmanShip_model');
        $this->load->model('GET_NewEvent');
        $this->load->model('GET_Product');
        $this->load->model('GET_WorkmanShip');
        $this->load->model('GET_DocumentFile');
    }

    public function index()
    {
        $this->load->view('Login/head');
        $this->load->view('navbar');
        $this->load->view('Auth/login');
        $this->load->view('footer');
        $this->load->view('Login/footer');
    }
    public function add_user()
    {

        $this->load->view('Auth/Regiscss');
        $this->load->view('Auth/navbar');
        $this->load->view('Auth/add_user');
        $this->load->view('Auth/footer');
        $this->load->view('Auth/js');
    }
    public function Action_Adduser()
    {
        $dataUser['email'] = $this->input->post('email');
        $dataUser['employeeID'] = $this->input->post('employeeID');
        $dataUser['fname'] = $this->input->post('fname');
        $dataUser['lname'] = $this->input->post('lname');
        $dataUser['fname_en'] = $this->input->post('fname_en');
        $dataUser['lname_en'] = $this->input->post('lname_en');
        $dataUser['phone'] = $this->input->post('phone');
        $dataUser['mobile'] = $this->input->post('mobile');
        $dataUser['role'] = $this->input->post('role');
        $dataUser['full_segment'] = $this->input->post('full_segment');

        $insert_result = $this->Insert_User_model->Insert_User($dataUser);
        $success = json_decode($insert_result, TRUE);

        print_r($insert_result);
        if ($success['response'] = "successful data insert") {
            echo "<script>";
            echo "alert(\" Completed information \");";
            echo "window.location=document.referrer";
            echo "</script>";
        } else {
            echo 'false';
        }
    }
    public function Home_Sell()
    {
        $data['resultProduct'] = $this->GET_Product->Product('$dataProduct');
        $data['resultNewsEvent'] = $this->GET_NewEvent->NewEvent('$dataNewsEvent');
        $data['resultWorkmanShip'] = $this->GET_WorkmanShip->WorkmanShip('$dataWorkmanShip');

        $this->load->view('Auth/css');
        $this->load->view('Auth/navbar');
        $this->load->view('Auth/Home_Sell', $data);
        $this->load->view('Auth/home_extension');
        $this->load->view('Auth/footer');
        $this->load->view('Auth/js');
    }


    public function Product_Sell()
    {
        $data['resultProduct'] = $this->GET_Product->Product('$dataProduct');

        $this->load->view('Auth/Product/head');
        $this->load->view('Auth/navbar');
        $this->load->view('Auth/Product/Product_Sell', $data);
        $this->load->view('Auth/home_extension');
        $this->load->view('Auth/footer');
        $this->load->view('Auth/Product/footer');
    }
    public function Product_connSell()
    {
        $data['resultProduct'] = $this->GET_Product->Product('$dataProduct');

        $this->load->view('Auth/Product/head');
        $this->load->view('Auth/navbar');
        $this->load->view('Auth/Product/Product_connSell', $data);
        $this->load->view('Auth/home_extension');
        $this->load->view('Auth/footer');
        $this->load->view('Auth/Product/footer');
    }
    public function Product_PlathSell()
    {
        $data['resultProduct'] = $this->GET_Product->Product('$dataProduct');

        $this->load->view('Auth/Product/head');
        $this->load->view('Auth/navbar');
        $this->load->view('Auth/Product/Product_PlathSell', $data);
        $this->load->view('Auth/home_extension');
        $this->load->view('Auth/footer');
        $this->load->view('Auth/Product/footer');
    }
    public function Product_soluSell()
    {
        $data['resultProduct'] = $this->GET_Product->Product('$dataProduct');

        $this->load->view('Auth/Product/head');
        $this->load->view('Auth/navbar');
        $this->load->view('Auth/Product/Product_soluSell', $data);
        $this->load->view('Auth/home_extension');
        $this->load->view('Auth/footer');
        $this->load->view('Auth/Product/footer');
    }


    public function List()
    {
        $this->load->view('Auth/List/css');
        $this->load->view('Auth/navbar');
        $this->load->view('Auth/List');
        $this->load->view('Auth/footer');
        $this->load->view('Auth/List/js');
    }

    public function Document()
    {
        $data['resultDocumentFile'] = $this->GET_DocumentFile->DocumentFile('$dataDocumentFile');
        $this->load->view('Auth/css');
        $this->load->view('Auth/navbar');
        $this->load->view('Auth/Document', $data);
        $this->load->view('Auth/footer');
        $this->load->view('Auth/js');
    }
    public function Insert_DocumentFile()
    {
        $this->load->view('Auth/Regiscss');
        $this->load->view('Auth/navbar');
        $this->load->view('Auth/Insert_DocumentFile');
        $this->load->view('Auth/footer');
        $this->load->view('Auth/js');
    }
    public function Action_Insert_DocumentFile()
    {
        $dataDoc['doc_file'] = $this->input->post('doc_name');
        $dataDoc['doc_name'] = $this->input->post('doc_file');

        // insert data array to model			
        $insert_result = $this->Insert_DocumentFile_model->insert($dataDoc);
        $success = json_decode($insert_result, TRUE);

        if ($success['response'] = "successful data insert") {
            echo "<script>";
            echo "alert(\" Completed information \");";
            echo "window.location=document.referrer";
            echo "</script>";
        } else {
            echo 'false';
        }
    }
    public function dashboard()
    {
        $this->load->view('Auth/css');
        $this->load->view('Auth/navbar');
        $this->load->view('Auth/dashboard');
        $this->load->view('Auth/footer');
        $this->load->view('Auth/js');
    }
    /////////////////////////////////////////////* [CONFIG] */////////////////////////////////////////////////////
    public function firebaseRDB()
    {
        $this->load->view('Auth/firebaseRDB');
    }


    public function config()
    {
        $this->load->view('Auth/config');
    }

    /* //===========================================[Form Sell]===========================================// */

    public function Form()
    {
        $this->load->view('Auth/Form/head');
        $this->load->view('Auth/navbar');
        $this->load->view('Auth/Form/Form');
        $this->load->view('Auth/footer');
        $this->load->view('Auth/Form/footer');
    }
    public function Form_Read($S_id)
    {
        $data['resultProduct'] = $this->GET_Product->Product('$dataProduct');
        $data['S_id'] = $this->uri->segment(3);

        $this->load->view('Auth/Form/head_read');
        $this->load->view('Auth/navbar_read');
        $this->load->view('Auth/Form/Form_Read', $data);
        $this->load->view('Auth/footer_read');
        $this->load->view('Auth/Form/footer');
        $this->load->view('Auth/js_read');
    }
    public function plath_view()
    {
        $this->load->view('Auth/Form/head');
        $this->load->view('Auth/navbar');
        $this->load->view("Auth/Form/F_Creative");
        $this->load->view('Auth/footer');
        $this->load->view('Auth/Form/footer');
    }
    /* plath view 2-2 */
    public function plath_productive()
    {
        $this->load->view('Auth/Form/head');
        $this->load->view('Auth/navbar');
        $this->load->view("Auth/Form/F_Productive");
        $this->load->view('Auth/footer');
        $this->load->view('Auth/Form/footer');
    }
    /* plath view 2-3 */
    public function plath_performance()
    {
        $this->load->view('Auth/Form/head');
        $this->load->view('Auth/navbar');
        $this->load->view("Auth/Form/F_Performance");
        $this->load->view('Auth/footer');
        $this->load->view('Auth/Form/footer');
    }
    /* =====================[END Platform]============================ */
    public function connect_view()
    {
        $this->load->view('Auth/Form/head');
        $this->load->view('Auth/navbar');
        $this->load->view("Auth/Form/F_Eval");
        $this->load->view('Auth/footer');
        $this->load->view('Auth/Form/footer');
    }

    /* Connect view 1-2 */
    public function connect_iconn1()
    {
        $this->load->view('Auth/Form/head');
        $this->load->view('Auth/navbar');
        $this->load->view("Auth/Form/F_iConn1");
        $this->load->view('Auth/footer');
        $this->load->view('Auth/Form/footer');
    }
    /* Connect view 1-3 */
    public function connect_firmfix()
    {
        $this->load->view('Auth/Form/head');
        $this->load->view('Auth/navbar');
        $this->load->view("Auth/Form/F_Firm");
        $this->load->view('Auth/footer');
        $this->load->view('Auth/Form/footer');
    }
    /* =====================[END Connectivity]============================ */

    public function solutions_view()
    {
        $this->load->view('Auth/Form/head');
        $this->load->view('Auth/navbar');
        $this->load->view("Auth/Form/F_Hotspot");
        $this->load->view('Auth/footer');
        $this->load->view('Auth/Form/footer');
    }
    /* Connect view 1-3 */
    public function solutions_security()
    {
        $this->load->view('Auth/Form/head');
        $this->load->view('Auth/navbar');
        $this->load->view("Auth/Form/F_Security");
        $this->load->view('Auth/footer');
        $this->load->view('Auth/Form/footer');
    }
    /* =====================[END Connectivity]============================ */
    public function line_noti()
    {
        $this->load->view("Auth/Form/line_noti");
    }
    public function line_noti_read()
    {
        $this->load->view("Auth/Form/line_noti_read");
    }

    /* =====================[New & Event Pages]============================ */
    public function New_Event()
    {
        $data['resultNewsEvent'] = $this->GET_NewEvent->NewEvent('$dataNewsEvent');

        $this->load->view('Auth/css');
        $this->load->view('Auth/navbar');
        $this->load->view("Auth/New_Event", $data);
        $this->load->view('Auth/footer');
        $this->load->view('Auth/js');
    }
    public function Insert_NewsEvent()
    {
        $this->load->view('Auth/Regiscss');
        $this->load->view('Auth/navbar');
        $this->load->view('Auth/Insert_NewsEvent');
        $this->load->view('Auth/footer');
        $this->load->view('Auth/js');
    }
    public function Action_Insert_NewsEvent()
    {

        $dataNewsEvent['header'] = $this->input->post('news_header');
        $dataNewsEvent['content'] = $this->input->post('news_content');

        $insert_result = $this->Insert_NewsEvent_model->Insert_NewsEvent($dataNewsEvent);
        $success = json_decode($insert_result, TRUE);

        if ($success['response'] = "successful data insert") {
            echo "<script>";
            echo "alert(\" Completed information \");";
            echo "window.location=document.referrer";
            echo "</script>";
        } else {
            echo 'false';
        }
    }
    public function Login()
    {

        $email = trim($this->input->post('email'));
        $employeeID = trim($this->input->post('employeeID'));

        $Userdata['email'] = $email;
        $Userdata['employeeID'] = $employeeID;

        $User = $this->LoginAPI_model->login($Userdata);

        $User_A = json_decode($User, TRUE);

        if (in_array("Data Not found", $User_A)) {

            echo "<script>";
            echo "alert(\" Email หรือ รหัสพนักงาน ไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง\");";
            echo "window.history.back()";
            echo "</script>";
        } else {

            $email_API = $User_A['email'];
            $employeeID_API = $User_A['employeeID'];

            if ($email == $email_API && $employeeID == $employeeID_API) {

                $this->session->set_userdata($User_A);
                header("location:./Home_Sell");
            }
        }
    }

    public function logout()
    {
        $this->load->view('Auth/logout');
    }

    public function Insert_WorkmanShip()
    {
        $this->load->view('Auth/Regiscss');
        $this->load->view('Auth/navbar');
        $this->load->view('Auth/Insert_WorkmanShip');
        $this->load->view('Auth/footer');
        $this->load->view('Auth/js');
    }

    public function Action_Insert_WorkmanShip()
    {
        $dataWorkmanShip['image'] = $this->input->post('Image');
        $dataWorkmanShip['header'] = $this->input->post('work_header');
        $dataWorkmanShip['content'] = $this->input->post('work_content');

        $insert_result = $this->Insert_WorkmanShip_model->Insert_WorkmanShip($dataWorkmanShip);
        $success = json_decode($insert_result, TRUE);

        if ($success['response'] = "successful data insert") {
            echo "<script>";
            echo "alert(\" Completed information \");";
            echo "window.location=document.referrer";
            echo "</script>";
        } else {
            echo 'false';
        }
    }

    public function about_read($A_id)
    {
        $data['resultWorkmanShip'] = $this->GET_WorkmanShip->WorkmanShip('$dataWorkmanShip');
        $data['A_id'] = $this->uri->segment(3);

        //$this->load->view('Auth/Form/head_read');
        $this->load->view('Auth/About/head');
        //$this->load->view('Auth/navbar_read');
        $this->load->view('Auth/About/about_read', $data);
        $this->load->view('Auth/footer_read');
        //$this->load->view('Auth/js_read');
        $this->load->view('Auth/About/footer');
    }
    public function auth_policy()
    {
        //$this->load->helper(array('form', 'url'));

        // $this->load->library('form_validation');

        // $this->load->helper('html');
        $this->load->view('Auth/policy/css');
        $this->load->view('Auth/navbar');
        $this->load->view('Auth/policy/Privacy_policy');
        $this->load->view('Auth/footer');
        $this->load->view('Auth/js');
    }
}
