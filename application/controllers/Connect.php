<?php
defined('BASEPATH') or exit('No direct script access allowed');
/* ปล. ชื่อ Class กับชื่อไฟล์ใน Controller ต้องตรงกัน เช่น Class Ntwelcome ชื่อไฟล์ใน Controller ก็ต้องเป็น Ntwelcome */

class Connect extends CI_Controller
{

    public function index()
    {
        //$this->load->helper(array('form', 'url'));

        // $this->load->library('form_validation');

        // $this->load->helper('html');
        /* $this->load->view('teamplate/head'); */
        $this->load->view('Config/Connect');
        /* $this->load->view('teamplate/footer'); */

    }

    public function config_Auth()
    {
        //$this->load->helper(array('form', 'url'));

        // $this->load->library('form_validation');

        // $this->load->helper('html');
        /* $this->load->view('teamplate/head'); */
        $this->load->view('Auth/Config/Connect');
        /* $this->load->view('teamplate/footer'); */

    }

}
