<?php
defined('BASEPATH') or exit('No direct script access allowed');
/* ปล. ชื่อ Class กับชื่อไฟล์ใน Controller ต้องตรงกัน เช่น Class Ntwelcome ชื่อไฟล์ใน Controller ก็ต้องเป็น Ntwelcome */

class Crud extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Insert_Product_model');
        $this->load->library('form_validation');
    }
    public function index()
    {

        $this->load->view('Auth/List/css');
        $this->load->view('Auth/navbar');
        $this->load->view('Auth/Product_Mgr/list_product');
        $this->load->view('Auth/footer');
        $this->load->view('Auth/List/js');
    }
    public function add_product()
    {

        /* $this->load->view('Auth/List/css'); */
        $this->load->view('Auth/Regiscss');
        $this->load->view('Auth/navbar');
        $this->load->view('Auth/Product_Mgr/add_product');
        $this->load->view('Auth/footer');
        $this->load->view('Auth/js');
    }


    public function insert_product()
    {
        $dataProduct['Image'] = $this->input->post('Image');
        $dataProduct['Package'] = $this->input->post('Package');
        $dataProduct['Service'] = $this->input->post('Service');
        $dataProduct['Detail1'] = $this->input->post('Detail1');
        $dataProduct['Detail2'] = $this->input->post('Detail2');
        $dataProduct['Detail3'] = $this->input->post('Detail3');
        $dataProduct['Detail4'] = $this->input->post('Detail4');
        $dataProduct['Feature'] = $this->input->post('Feature');

        $insert_result = $this->Insert_Product_model->Insert_Product($dataProduct);
        $success = json_decode($insert_result, TRUE);

        if ($success['response'] = "successful data insert") {
            echo "<script>";
            echo "alert(\" Completed information \");";
            echo "window.location=document.referrer";
            echo "</script>";
        } else {
            echo 'false';
        }
    }
    public function edit_product()
    {
        $this->load->view('Auth/List/css');
        $this->load->view('Auth/navbar');
        $this->load->view('Auth/Product_Mgr/edit_product');
        $this->load->view('Auth/footer');
        $this->load->view('Auth/List/js');
    }
    public function action_edit()
    {
        $this->load->view('Auth/Product_Mgr/action_edit');
    }
    public function delete_product()
    {
        $this->load->view('Auth/Product_Mgr/insert_product');
    }
}
