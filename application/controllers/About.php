<?php
defined('BASEPATH') or exit('No direct script access allowed');
/* ปล. ชื่อ Class กับชื่อไฟล์ใน Controller ต้องตรงกัน เช่น Class Ntwelcome ชื่อไฟล์ใน Controller ก็ต้องเป็น Ntwelcome */

class About extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('GET_WorkmanShip');
    }
    public function about_read($A_id)
    {
        $data['resultWorkmanShip'] = $this->GET_WorkmanShip->WorkmanShip('$dataWorkmanShip');
        $data['A_id'] = $this->uri->segment(3);

        $this->load->view('About/head');
        //$this->load->view('navbar_read');
        $this->load->view('About/about_read', $data);
        $this->load->view('footer_read');
        $this->load->view('About/footer');
    }
}
