<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['Product'] = 'Product';
// ==============[Partner]=================== //
$route['Partner'] = 'Partner/index';
$route['Partner_linenonti'] = 'Partner/line_nontify';

// ==============[Platform]=================== //
$route['Platform'] = 'Platform/index';
//$route['Platform'] = 'Partner/line_nontify';


// ==============[Product]=================== //
$route['Product'] = 'Product/index';
$route['Product_conn'] = 'Product/product_conn';
$route['Product_solu'] = 'Product/product_solu';
$route['Product_plath'] = 'Product/product_plath';

// ==============[Solution]=================== //
$route['Solution'] = 'Solution/index';


// ==============[Form]=================== //
$route['Form'] = 'Form/index';
$route['Form_Read'] = 'Form/Form_Read';
$route['plath_view'] = 'Form/plath_view';
$route['plath_productive'] = 'Form/plath_productive';
$route['plath_performance'] = 'Form/plath_performance';
$route['connect_view'] = 'Form/connect_view';
$route['connect_iconn1'] = 'Form/connect_iconn1';
$route['connect_firmfix'] = 'Form/connect_firmfix';
$route['solutions_view'] = 'Form/solutions_view';
$route['solutions_security'] = 'Form/solutions_security';
$route['line_noti'] = 'Form/line_noti';

// ==============[Contact]=================== //
//$route['Contact'] = 'XBlog/index';

// ==============[Cookie_policy]=================== //
$route['Cookie_policy'] = 'Cookie_policy/index';

// ==============[Connectivity]=================== //
/* $route['Connectivity'] = 'XBlog/index'; */


// ==============[Auth LOGIN SELL]=================== //
$route['Auth'] = 'Auth/index';
$route['add_user'] = 'Auth/add_user';
$route['Action_Adduser'] = 'Auth/Action_Adduser';
$route['Home_Sell'] = 'Auth/Home_Sell';
$route['Product_Sell'] = 'Auth/Product_Sell';
$route['Product_connSell'] = 'Auth/Product_connSell';
$route['Product_PlathSell'] = 'Auth/Product_PlathSell';
$route['Product_soluSell'] = 'Auth/Product_soluSell';
$route['List'] = 'Auth/List';
$route['Document'] = 'Auth/Document';
$route['dashboard'] = 'Auth/dashboard';
$route['firebaseRDB'] = 'Auth/firebaseRDB';
$route['config'] = 'Auth/config';
$route['logout'] = 'Auth/logout';
$route['Form'] = 'Auth/Form';
$route['Form_Read'] = 'Auth/Form_Read';
$route['plath_view'] = 'Auth/plath_view';
$route['plath_productive'] = 'Auth/plath_productive';
$route['plath_performance'] = 'Auth/plath_performance';
$route['connect_view'] = 'Auth/connect_view';
$route['connect_iconn1'] = 'Auth/connect_iconn1';
$route['connect_firmfix'] = 'Auth/connect_firmfix';
$route['solutions_view'] = 'Auth/solutions_view';
$route['solutions_security'] = 'Auth/solutions_security';
$route['line_noti'] = 'Auth/line_noti';
$route['line_noti_read'] = 'Auth/line_noti_read';
$route['Insert_NewsEvent'] = 'Auth/Insert_NewsEvent';
$route['Action_Insert_NewsEvent'] = 'Auth/Action_Insert_NewsEvent';
$route['Insert_DocumentFile'] = 'Auth/Insert_DocumentFile';
$route['Action_Insert_DocumentFile'] = 'Auth/Action_Insert_DocumentFile';
$route['Insert_WorkmanShip'] = 'Auth/Insert_WorkmanShip';
$route['Action_Insert_WorkmanShip'] = 'Auth/Action_Insert_WorkmanShip';
$route['about_read'] = 'Auth/about_read';
$route['auth_policy'] = 'Auth/auth_policy';

// ==============[Crud-Product]=================== //

$route['add_product'] = 'Crud/add_product';
$route['insert_product'] = 'Crud/insert_product';
$route['edit_product'] = 'Crud/edit_product';
$route['action_edit'] = 'Crud/action_edit';
$route['list_product'] = 'Crud/index';

// ==============[About]=================== //
//$route['About'] = 'About/index';
$route['about_read'] = 'About/about_read';
//$route['factory'] = 'About/factory';
//$route['baanchang'] = 'About/baanchang';
//$route['hotspot'] = 'About/hotspot';
//$route['security'] = 'About/security';

// ==============[Action_Login]=================== //
$route['Login'] = 'Login_API/login';

// ==============[Action_signup]=================== //
$route['Action_signup'] = 'Action_signup/index';




$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
