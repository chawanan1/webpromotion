<?php

class GET_Product extends CI_Model
{


    public function Product()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.totwbs.com/connx/webprom/getproduct',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'x-api-key: XnnOcf5b490f7N57cTc4Ceo10f8Nn2Xdx3',
                'Authorization: Basic Y29ubnh3ZWJwcm9tOkNuWEBwcm9tb3Rpb253ZWJTYWxlOTQzMA==',
                'Cookie: ci_session=pvgm86pmtmofdftsm7ofc0fp1rpq8bmv'
            ),
        ));

        $response = curl_exec($curl);
        $dataProduct = json_decode($response);

        curl_close($curl);

        return $dataProduct;
        //echo $response;

    }
}
