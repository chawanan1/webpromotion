<?php

class GET_NewEvent extends CI_Model
{


    public function NewEvent()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.totwbs.com/connx/webprom/getnew',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'x-api-key: XnnOcf5b490f7N57cTc4Ceo10f8Nn2Xdx3',
                'Authorization: Basic Y29ubnh3ZWJwcm9tOkNuWEBwcm9tb3Rpb253ZWJTYWxlOTQzMA==',
                'Cookie: ci_session=nfiupnked2vgorfle4ct2gtqm1do1jas'
            ),
        ));

        $response = curl_exec($curl);
        $dataNewsEvent = json_decode($response);

        curl_close($curl);

        return $dataNewsEvent;
        //echo $response;
    }
}
