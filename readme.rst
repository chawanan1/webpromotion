

<!-- -------------------------------------------------------------------------------------------------------------------- -->

Docker Install Web NT ConnX
==============
Item            | Info
--------------- | ---------------
Last update     | 7/10/2565
Project version | 2.3.4

<a name="readme-top"></a>

<!-- [![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url] -->
[![Contributors][contributors-shield]]
[![Forks][forks-shield]]
[![Stargazers][stars-shield]]
[![Issues][issues-shield]]
[![MIT License][license-shield]]
[![LinkedIn][linkedin-shield]]


<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="#">
    <img src="assets/images/LGConnX2.png" alt="Logo" width="230" height="100">
  </a>

  <h3 align="center">NT ConnX Website</h3>

  <p align="center">
    Project NT ConnX Path 2
    <br />
    <a href="https://ntconnx.com"><strong>Peview »</strong></a>
    <br />
    <br />
    <a href="#">Report Demo</a>
    ·
    <a href="#">Report Bug</a>
    ·
    <a href="#">Request Feature</a>
  </p>
</div>





<!-- ABOUT THE PROJECT -->
## About The Project
<div align="center">
<!-- [![Product Name Screen Shot][product-screenshot]](https://example.com) -->
<p align="center">
<strong>NT ConnX</strong>

<strong>A Wireless Connectivity of things</strong>

<strong>Driving a Digital Business Solotion</strong>
</p>
</div>

### Built With

This section should list any major frameworks/libraries used to bootstrap your project. Leave any add-ons/plugins for the acknowledgements section. Here are a few examples.


* [![Angular][Angular.io]][Angular-url]
* [![Bootstrap][Bootstrap.com]][Bootstrap-url]
* [![JQuery][JQuery.com]][JQuery-url]
* [![Php][Php.net]][Php-url]
* [![Docker][Docker.com]][Docker-url]
* [![Codeigniter][Codeigniter.com]][Codeigniter-url]
* [![Apache][Apache.org]][Apache-url]
* [![reCAPTCHA][reCAPTCHA.com]][reCAPTCHA-url]
* [![Firebase][Firebase.com]][Firebase-url]
* [![Analytics][Analytics.com]][Analytics-url]


<!-- GETTING STARTED -->
## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
* docker
  ```sh
  docker login
  ```
* gitlab
  ```sh
  git login
  ```
### Installation

_Below is an example of how you can instruct your audience on installing and setting up your app. This template doesn't rely on any external dependencies or services._

1. Get a free API Key at [https://example.com](https://example.com)
2. Clone the repo
   ```sh
   git clone https://gitlab.com/connx/connx-webpromotion.git
   ```
3. Docker Build Contrainer
   ```sh
   docker build --build-arg BUILD_MODE=production . -t registry.gitlab.com/connx/connx-webpromotion:v2.2.4
   ```
4. Docker Run Contrainer
   ```sh
   docker run -p 80:80 --name connx-webpromotion registry.gitlab.com/connx/connx-webpromotion:v2.2.4
   ```
5. Docker Push Contrainer Registry
   ```sh
   docker push registry.gitlab.com/connx/connx-webpromotion:v2.2.4
   ```
   
<!-- 4. Enter your API in `config.js`
   ```js
   const API_KEY = 'ENTER YOUR API';
   ``` -->

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Settings & Config [IMG]

To config the image  `application/views/Config/`, Flie `Connect.php` In folder:

1. แก้ไขแค่ ชื่อรูปภาพ และ นามสกุลไฟล์เท่านั้น
```
$_CONFIG['PROIMG']['HOT1'] = '1.png';
```
หากต้องการให้เพิ่มรูปภาพใหม่เอารูปภาพไปไว้ที่  `application/assets/img/` In folder:

1. ไปที่หน้าที่เราต้องการเพิ่มรูปภาพ
2. ก็อปโค้ดจากบรรทัดบนในหน้านั้น แล้วทำการเปลี่ยนชื่อ

### ตัวอย่าง
```
<?php 
include 'Config/Connect.php';
echo "<img class='modalimg' src='/assets/img/nt_connectivity/" . $_CONFIG['CONN']['CONN1'] . "' width='40%' > ";
?>
```
### เพิ่มรูปใหม่

```
<?php 
include 'Config/Connect.php';
echo "<img class='modalimg' src='/assets/img/โฟเดอร์ที่นำรูปภาพไปไว้/" . $_CONFIG['แก้ไขแพต1']['แก้ไขแพต2'] . "' width='40%' > ";
?>
```
ต่อมาให้ไปที่  `application/views/Config/` แล้วทำการเพิ่มโค้ดส่วนนี้ไป:
```
$_CONFIG['แพต1']['แพต2'] = 'ชื่อรูป.png';
```

เส้นทาง Config จุดที่จะต้องเพิ่ม Code ถ้าเราเพิ่มรูปใหม่  :
- `application/views/Config/Connect.php`
- `application/views/Auth/Config/Connect.php`
- `application/views/Auth/Product/Connect.php`

เส้นทางการแก้ไขในแทบ Navbar   :
- `application/views/navbar.php`
- `application/views/footer.php`
- `application/views/Auth/navbar.php`
- `application/views/Auth/footer.php`




<!-- ROADMAP -->
## Roadmap

- [x] Page Sell Show Price
- [x] Firebase Login Auth Role = Admin & Sell
- [x] Edit & Add User / Add Role
- [ ] UPDATE Last 07/10/2565
- [ ] ...UPDATE 
    - [ ] ...UPDATE 
    - [ ] ...UPDATE 





<!-- LICENSE -->
## License

Distributed under the NT ConnX License. See `LICENSE.txt` for more information.




<!-- CONTACT -->
## Contact

NT ConNX - [EMAIL](tel:connx@ntplc.co.th) - connx@ntplc.co.th

NT ConnX Website: [http://ntconnx.com/](http://ntconnx.com/)

###
Copyright © 2022 NT ConnX 2022. All rights reserved
###

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=for-the-badge
[contributors-url]: https://github.com/othneildrew/Best-README-Template/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge
[forks-url]: https://github.com/othneildrew/Best-README-Template/network/members
[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=for-the-badge
[stars-url]: https://github.com/othneildrew/Best-README-Template/stargazers
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge
[issues-url]: https://github.com/othneildrew/Best-README-Template/issues
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/othneildrew
[product-screenshot]: images/screenshot.png
[Next.js]: https://img.shields.io/badge/next.js-000000?style=for-the-badge&logo=nextdotjs&logoColor=white
[Next-url]: https://nextjs.org/
[React.js]: https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB
[React-url]: https://reactjs.org/
[Vue.js]: https://img.shields.io/badge/Vue.js-35495E?style=for-the-badge&logo=vuedotjs&logoColor=4FC08D
[Vue-url]: https://vuejs.org/
[Angular.io]: https://img.shields.io/badge/Angular-DD0031?style=for-the-badge&logo=angular&logoColor=white
[Angular-url]: https://angular.io/
[Svelte.dev]: https://img.shields.io/badge/Svelte-4A4A55?style=for-the-badge&logo=svelte&logoColor=FF3E00
[Svelte-url]: https://svelte.dev/
[Laravel.com]: https://img.shields.io/badge/Laravel-FF2D20?style=for-the-badge&logo=laravel&logoColor=white
[Laravel-url]: https://laravel.com
[Bootstrap.com]: https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white
[Bootstrap-url]: https://getbootstrap.com
[JQuery.com]: https://img.shields.io/badge/jQuery-0769AD?style=for-the-badge&logo=jquery&logoColor=white
[JQuery-url]: https://jquery.com 

[Php.net]: https://img.shields.io/badge/php-2b5aff?style=for-the-badge&logo=php&logoColor=white
[Php-url]: https://php.net/
[apache.org]: https://img.shields.io/badge/apache-eff3ff?style=for-the-badge&logo=apache&logoColor=red
[apache-url]: https://apache.org/
[Docker.com]: https://img.shields.io/badge/docker-043bff?style=for-the-badge&logo=docker&logoColor=white
[Docker-url]: https://Docker.com/
[Firebase.com]: https://img.shields.io/badge/Firebase-fffcef?style=for-the-badge&logo=Firebase&logoColor=ffc804
[Firebase-url]: https://Firebase.google.com/
[Analytics.com]: https://img.shields.io/badge/Analytics-fffcef?style=for-the-badge&logo=googleAnalytics&logoColor=ffc804
[Analytics-url]: https://ads.Analytics.com/
[reCAPTCHA.com]: https://img.shields.io/badge/reCAPTCHA-eff3ff?style=for-the-badge&logo=reCaptcha&logoColor=ffc804
[reCAPTCHA-url]: https://developers.google.com/recaptcha/docs/display.com/
[Codeigniter.com]: https://img.shields.io/badge/codeigniter-ffx?style=for-the-badge&logo=codeigniter&logoColor=red
[Codeigniter-url]: https://codeigniter.com/






